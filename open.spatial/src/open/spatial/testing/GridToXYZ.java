package open.spatial.testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import open.spatial.Text;

public class GridToXYZ {
	
	
	
	public static void main(String[] args) {
		File f1 = new File("C:\\Users\\Andrew\\Documents\\magnetics.txt");
		File f2 = new File("C:\\Users\\Andrew\\Documents\\conductivity.txt");
		try {
			run(f1,f2,0.3,0.3);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void run(File f1, File f2,double dx, double dy) throws Exception{
		BufferedReader in1 = new BufferedReader(new FileReader(f1));
		BufferedReader in2 = new BufferedReader(new FileReader(f2));
		String line1 = "";
		String line2 = "";
		
		double ox = 0;
		double oy = 0;
		double y = oy;
		double x = ox;
		while((line1 = in1.readLine())!= null) {
			line2 = in2.readLine();
			ArrayList<Double> vals1 = Text.split(line1);
			ArrayList<Double> vals2 = Text.split(line2);
			x = ox;
			for(int i = 0 ; i < vals1.size() ;i++) {
				double v1 = vals1.get(i);
				double v2 = vals2.get(i);
				System.out.println(x + "\t" + y + "\t" + 0 + "\t" + v1 + "\t" + v2);
				x += dx;
			}
			
			y += dy;
		}
		in2.close();
	}
}