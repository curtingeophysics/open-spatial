package open.spatial.testing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import open.spatial.Text;
import open.spatial.formats.KML;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class ExtractPoints {
	public final static int MULT = 10;
	public static void main(String[] args) throws Exception {
		File dir = new File("U:\\EGP_Data\\Projects\\Research\\WATER Projects\\DoW_Hydrogeophysics_2015_2018\\WP2 - Reprocessing\\WP2-RA2 re-processing Airborne TEM\\DTMS");
		File dirOut = new File("U:\\EGP_Data\\Projects\\Research\\WATER Projects\\DoW_Hydrogeophysics_2015_2018\\WP2 - Reprocessing\\WP2-RA2 re-processing Airborne TEM\\DTMS\\Cut\\");
		dirOut.mkdirs();
		for(File f : dir.listFiles()) {
			if(f.isFile()) {
				BufferedReader in = new BufferedReader(new FileReader(f));
				BufferedWriter out = new BufferedWriter(new FileWriter(dirOut.getAbsolutePath() + "\\" + f.getName()));
				out.write("line\tlat\tlon");
				out.newLine();
				String line = "";
				int count = 1;
				
				LinkedHashMap<String, ArrayList<Point>> points  = new LinkedHashMap<String, ArrayList<Point>>();
				while((line = in.readLine()) != null) {
					if(count % MULT == 0) {
						ArrayList<Double> vals = Text.split(line);
						int lineNumber = (int) vals.get(0).intValue();
						String name = "" + lineNumber;
						double e = vals.get(1);
						double n = vals.get(2);
						int zone = (f.getAbsolutePath().toLowerCase().contains("pat") || f.getAbsolutePath().toLowerCase().contains("esp")) ? 51 : 50;
						UTMRef r = new UTMRef(zone, 'J', e, n);
						LatLng ll = r.toLatLng();
						double lat = ll.getLatitude();
						double lon = ll.getLongitude();
						out.write(lineNumber + "\t" + lat + "\t" + lon);
						out.newLine();
						ArrayList<Point> pts = points.get(name);
						if(pts == null) {
							pts = new ArrayList<Point>();
							points.put(name, pts);
							
						}
						pts.add(new Point(lat, lon));
					}
					count++;
				}
				out.close();
				in.close();
			
				File outfile = new File(dirOut.getAbsolutePath() + "\\" + f.getName() + ".kml");
				KML.exportWaypoints(points, outfile, false);
			}
			
		
		}
	}
}
