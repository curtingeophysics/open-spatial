package open.spatial.formats;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.Text;
import open.spatial.crs.CRSConverter;
import open.spatial.crs.Projections;
import open.spatial.crs.WTKInstance;
import open.spatial.points.Point;
import open.spatial.text.DataTable;

public class KML extends OpenSpatialFormat {

	public final static WTKInstance WTK_GOOGLE = new WTKInstance("WGS84 - GOOGLE WTK", "WGS84 - GOOGLE WTK", CRSConverter.WGS84);

	static final Logger logger = LogManager.getLogger(KML.class.getName());

	public final static int MODE_POINT = 10;
	public final static int MODE_TRACK = 20;

	private final int currentMode;

	public KML(int mode) {
		this.currentMode = mode;
	}


	@Override
	public String getName() {
		return "KML Google Earth Format " + (currentMode == MODE_POINT ? "(Points)" : "(Tracks)");
	}

	@Override
	public List<String> getExtensions() {
		return Arrays.asList(".kml");
	}

	@Override
	public DataTable read(File fin) {
		try {
			ArrayList<Point> pts = loadKMLPoints(fin.getAbsolutePath(), false);
			DataTable dt = new DataTable(pts);
			dt.setInputCRS(WTK_GOOGLE);
			return dt;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	@Override
	public void write(DataTable table, File fout) {
		try {
		switch (currentMode) {
			case MODE_POINT: exportWaypoints(table.getLinesPointsHash(), fout, true);
			case MODE_TRACK: exportWaypoints(table.getLinesPointsHash(), fout, false);
				break;

			default:
				break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	public static DataTable loadTable(String path) throws Exception{
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

		logger.info("Loading KML Points from Line");
		ArrayList<Point> points = new ArrayList<Point>();

		BufferedReader in = new BufferedReader(new FileReader(path));
		String line = "";

		while(line != null) {
			while((line = in.readLine()) != null && !line.toLowerCase().contains("coordinates")) {

			}
			String s = line;
			while((line) != null && !line.toLowerCase().contains("/coordinates")) {
				s += line;
				line = in.readLine();
			}
			if(s != null) {
				s = s.toUpperCase().replaceAll("<COORDINATES>", "");
				s = s.toUpperCase().replaceAll("</COORDINATES>", "");
				s = s.split("<")[0];
				System.out.println(s);
				ArrayList<Double> vals = Text.split(s);
				while(vals.size() > 0) {
					double lon = vals.remove(0);
					double lat = vals.remove(0);
					double elev = vals.remove(0);
					Point p = new Point(lat, lon);
					p.z = elev;
					points.add(p);
					ArrayList<String> v = new ArrayList<>(Arrays.asList("" + lon,"" + lat, "" + elev));
					values.add(v);

				}
			}
			line = in.readLine();
		}
		in.close();
		logger.debug("...Loaded " + points.size() + " points.");
		DataTable dt = new DataTable(new ArrayList<>(), values);
		dt.xcol.set(0);
		dt.ycol.set(1);
		dt.zcol.set(2);
		dt.setInputCRS(Projections.idMap.get("EPSG:4326"));
		return dt;
	}

	public static ArrayList<Point> loadKMLPoints(String path, boolean preview) throws Exception{
		logger.info("Loading KML Points from Line");
		ArrayList<Point> points = new ArrayList<Point>();

		BufferedReader in = new BufferedReader(new FileReader(path));
		String line = "";
		while(!(line = in.readLine()).contains("coordinates")) {

		}
		String s = line;
		while(!(line = in.readLine()).contains("coordinates")) {
			s += line;
		}
		s = s.toUpperCase().replaceAll("<COORDINATES>", "");
		s = s.toUpperCase().replaceAll("</COORDINATES>", "");
		ArrayList<Double> vals = Text.split(s);
		while(vals.size() > 0) {
			double lon = vals.remove(0);
			double lat = vals.remove(0);
			double elev = vals.remove(0);
			Point p = new Point(lat, lon);
			p.z = elev;
			points.add(p);
			if(preview) {
				in.close();
				return points;
			}
		}

		in.close();
		logger.debug("...Loaded " + points.size() + " points.");
		return points;
	}



//	public final static File header = new File(KML.class.getResource("header.txt").toExternalForm());
//	public final static File footer = new File(KML.class.getResource("footer.txt").toExternalForm());
	public final static NumberFormat nf = new DecimalFormat("000");

//	public final double [] poly = {116.3080792298351,-31.91363606586881,0,116.3098346162357,-31.76283904967486,0,116.313684044386,-31.35556845261878,0,116.2982667236979,-30.91751273753607,0,116.4120969342307,-30.69042031167455,0,116.3862508037662,-30.41856452862757,0,116.4325911260027,-30.17264342471115,0,116.4767490498344,-29.41644439017126,0,116.5167756933897,-28.65846280047385,0,116.518618982665,-25.61638470408076,0,112.6985762559097,-24.43292162039675,0,112.4694281699921,-25.21002459515444,0,114.227651794659,-28.44317620899495,0,114.3927963261455,-28.89927039718721,0,114.5557640519672,-29.45982640997962,0,114.5138230834433,-29.99488416236612,0,114.7471318424652,-30.50604701301918,0,114.9062169290286,-30.95172769355218,0,115.1933882943293,-31.42880734155646,0,115.0915244620111,-31.83624176708203,0,115.0592684019377,-32.33608905650336,0,114.9860833973549,-32.85087056431857,0,114.9844529492084,-32.89640091075992,0,114.512143414631,-33.30003683696494,0,114.4353883935381,-33.69551823346131,0,114.5492431802528,-34.09428595903575,0,114.8103468078764,-34.61722680663979,0,115.0792606321913,-35.00223277371818,0,115.7471434794653,-35.1414666635305,0,116.4679847518181,-35.29269275147088,0,117.0001586203622,-35.2892704332579,0,117.6425351530517,-35.25258958177685,0,118.485574048258,-35.14990318505197,0,118.7831856397369,-35.09943979917586,0,118.9866741371289,-34.91388672882421,0,119.1518850117963,-34.74437730662012,0,119.408952902037,-34.69407106709319,0,119.8660917962455,-34.60885737813447,0,120.0650235896543,-34.49871158969685,0,120.2438509323556,-34.34380445924622,0,120.4252003530484,-34.32422806655595,0,121.1136651024079,-34.29020147600981,0,121.6200328020351,-34.27440996433245,0,122.1873396816357,-34.40525549097072,0,123.362037863156,-34.35867645772605,0,123.3881226489226,-33.93665934208972,0,123.2430767249299,-33.64299608430051,0,122.5036839626076,-33.5681875170345,0,121.6634335314675,-33.52578491412339,0,121.3278531087134,-33.40726928093113,0,120.51735115596,-33.33750605650554,0,119.9439400252871,-33.44151750555437,0,119.3385626784743,-33.53797518705569,0,118.9513982280594,-33.65748800247057,0,118.472714369542,-33.9691245170593,0,118.0892159102008,-34.12530860017757,0,117.7105151642489,-34.19818412712689,0,117.4043173285694,-34.18758191839015,0,117.2246600929789,-34.14897100164675,0,116.9447939744652,-34.11549974238648,0,116.712023136243,-34.03756055646433,0,116.4602880815415,-33.89290361562942,0,116.2097826705482,-33.6925577413497,0,116.1241803209863,-33.26706755551863,0,116.1710404554342,-32.8264668522167,0,116.2148476615723,-32.50701627022163,0,116.2380287378816,-32.18138241526973,0,116.3080792298351,-31.91363606586881,0};
	public final static void exportWaypoints(LinkedHashMap<String, ArrayList<Point>> points, File outfile, boolean isWaypoints) throws Exception{
		logger.info("Exporting Waypoints to KML " + outfile.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
		logger.debug("...Creating header");

		String [] header = {"<?xml version=\"1.0\" encoding=\"UTF-8\"?> ",
		"<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">",
		"<Document>                                                                                                                                                                   "        ,
		"	<name>"+outfile.getName()+"</name>                                                                                                                                                     "        ,
		"	<Style id=\"sh_placemark_circle_highlight\">                                                                                                                                "      ,
		"		<IconStyle>                                                                                                                                                           "        ,
		"			<scale>0.6</scale>                                                                                                                                                "        ,
		"			<Icon>                                                                                                                                                            "        ,
		"				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href>                                                                        "        ,
		"			</Icon>                                                                                                                                                           "        ,
		"		</IconStyle>                                                                                                                                                          "        ,
		"				<LabelStyle>                                                                                                                                                  "        ,
		"			<scale>0.5</scale>                                                                                                                                                "        ,
		"		</LabelStyle>                                                                                                                                                         "        ,
		"		<ListStyle>                                                                                                                                                           "        ,
		"		                                                                                                                                                                      "        ,
		"		</ListStyle>                                                                                                                                                          "        ,
		"	</Style>                                                                                                                                                                  "        ,
		"	<Style id=\"sn_placemark_circle\">                                                                                                                                          "      ,
		"		<IconStyle>                                                                                                                                                           "        ,
		"			<scale>0.6</scale>                                                                                                                                                "        ,
		"			<Icon>                                                                                                                                                            "        ,
		"				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>                                                                                  "        ,
		"			</Icon>                                                                                                                                                           "        ,
		"		</IconStyle>                                                                                                                                                          "        ,
		"				<LabelStyle>                                                                                                                                                  "        ,
		"			<scale>0.5</scale>                                                                                                                                                "        ,
		"		</LabelStyle>                                                                                                                                                         "        ,
		"		<ListStyle>                                                                                                                                                           "        ,
		"		</ListStyle>                                                                                                                                                          "        ,
		"	</Style>                                                                                                                                                                  "        ,
		"	<StyleMap id=\"msn_placemark_circle\">                                                                                                                                      "      ,
		"		<Pair>                                                                                                                                                                "        ,
		"			<key>normal</key>                                                                                                                                                 "        ,
		"			<styleUrl>#sn_placemark_circle</styleUrl>                                                                                                                         "        ,
		"		</Pair>                                                                                                                                                               "        ,
		"		<Pair>                                                                                                                                                                "        ,
		"			<key>highlight</key>                                                                                                                                              "        ,
		"			<styleUrl>#sh_placemark_circle_highlight</styleUrl>                                                                                                               "        ,
		"		</Pair>                                                                                                                                                               "        ,
		"	</StyleMap>                                                                                                                                                               "        ,
		"	<Folder>                                                                                                                                                                  "        ,
		"		<name>Receivers</name>                                                                                                                                                "        ,
		"		<open>1</open>                                                                                                                                                        "};

		for(String l : header) {
			out.write(l);
			out.newLine();
		}

		String [] footer = {"</Folder>",
		"</Document>",
		"</kml>"};



		for(String name : points.keySet()) {
			ArrayList<Point> pts = points.get(name);
			if(isWaypoints) {
				logger.debug("...Creating Waypoints");
				writeWaypointsStrings(out,pts, true);
			} else {
				logger.debug("...Creating Tracks");
				writeTrackStrings(out,pts,name, true);
			}
		}
//		out.write(getTextFromFile(header,outfile.getName()));

		logger.debug("...Creating Footer");
//		out.write(getTextFromFile(footer,outfile.getName()));
		for(String l : footer) {
			out.write(l);
			out.newLine();
		}
		out.close();
		logger.debug("...Exported " + points.size() + " points.");
	}
	public final static void exportZScaledWaypoints(ArrayList<Point> points, File outfile, boolean isWaypoints) throws Exception{
		logger.info("Exporting Waypoints to KML " + outfile.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
		logger.debug("...Creating header");
		int nlevels = 20;
		ArrayList<Double> scales = new ArrayList<Double>();
		for(double s = 0.3 ; s <= 1.5 ; s+= (1.5-0.3)/20) {
			scales.add(s);
		}

		ArrayList<String> header = new ArrayList<String>();
		header.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?> ");
		header.add("<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">");
		header.add("<Document>                                                                                                                                                                   "        );
		header.add("	<name>"+outfile.getName()+"</name>                                                                                                                                                     "        );
		int ith = 0;
		for (double s : scales) {
			header.add("<Style id=\"sn_placemark_circle"+ith+"\">");
			header.add("<IconStyle>");
			header.add("	<scale>"+s+"</scale>");
			header.add("	<Icon>");
			header.add("		<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>");
			header.add("	</Icon>");
			header.add("</IconStyle>");
			header.add("<LabelStyle>");
			header.add("<scale>0.5</scale>");
			header.add("</LabelStyle>  ");
			header.add("<BalloonStyle>");
			header.add("</BalloonStyle>");
			header.add("<ListStyle>");
			header.add("</ListStyle>");
			header.add("</Style>");

			header.add("<StyleMap id=\"msn_placemark_circle"+ith+"\">                  ");
			header.add("<Pair>                                                ");
			header.add("	<key>normal</key>                                  ");
			header.add("	<styleUrl>#sn_placemark_circle"+ith+"</styleUrl>          ");
			header.add("</Pair>                                               ");
			header.add("<Pair>                                                ");
			header.add("	<key>highlight</key>                               ");
			header.add("	<styleUrl>#sh_placemark_circle_highlight"+ith+"</styleUrl>");
			header.add("</Pair>                                               ");
			header.add("</StyleMap>                                           ");

		header.add("	<Style id=\"sh_placemark_circle_highlight"+ith+"\">");
		header.add("	<IconStyle>");
		header.add("		<scale>s</scale>");
		header.add("		<Icon>");
		header.add("			<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href>");
		header.add("		</Icon>");
		header.add("	</IconStyle>");
		header.add("	<BalloonStyle>");
		header.add("	</BalloonStyle>");
		header.add("	<ListStyle>");
		header.add("	</ListStyle>");
		header.add("</Style>");

			ith++;
		}
		header.add("	<Folder>                                                                                                                                                                  "        );
		header.add("		<name>Receivers</name>                                                                                                                                                "        );
		header.add("		<open>1</open>                                                                                                                                                        ");

		for(String l : header) {
			out.write(l);
			out.newLine();
		}

		String [] footer = {"</Folder>",
		"</Document>",
		"</kml>"};

//		out.write(getTextFromFile(header,outfile.getName()));
		logger.debug("...Creating Waypoints");
		writeWaypointsScaledStrings(out,points, true,scales);
		logger.debug("...Creating Footer");
//		out.write(getTextFromFile(footer,outfile.getName()));
		for(String l : footer) {
			out.write(l);
			out.newLine();
		}
		out.close();
		logger.debug("...Exported " + points.size() + " points.");
	}
	public final static void exportWaypoints(ArrayList<Point> points, File outfile, boolean isWaypoints) throws Exception{
		logger.info("Exporting Waypoints to KML " + outfile.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
		logger.debug("...Creating header");

		String [] header = {"<?xml version=\"1.0\" encoding=\"UTF-8\"?> ",
		"<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">",
		"<Document>                                                                                                                                                                   "        ,
		"	<name>"+outfile.getName()+"</name>                                                                                                                                                     "        ,
		"	<Style id=\"sh_placemark_circle_highlight\">                                                                                                                                "      ,
		"		<IconStyle>                                                                                                                                                           "        ,
		"			<scale>0.6</scale>                                                                                                                                                "        ,
		"			<Icon>                                                                                                                                                            "        ,
		"				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href>                                                                        "        ,
		"			</Icon>                                                                                                                                                           "        ,
		"		</IconStyle>                                                                                                                                                          "        ,
		"				<LabelStyle>                                                                                                                                                  "        ,
		"			<scale>0.5</scale>                                                                                                                                                "        ,
		"		</LabelStyle>                                                                                                                                                         "        ,
		"		<ListStyle>                                                                                                                                                           "        ,
		"		                                                                                                                                                                      "        ,
		"		</ListStyle>                                                                                                                                                          "        ,
		"	</Style>                                                                                                                                                                  "        ,
		"	<Style id=\"sn_placemark_circle\">                                                                                                                                          "      ,
		"		<IconStyle>                                                                                                                                                           "        ,
		"			<scale>0.6</scale>                                                                                                                                                "        ,
		"			<Icon>                                                                                                                                                            "        ,
		"				<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>                                                                                  "        ,
		"			</Icon>                                                                                                                                                           "        ,
		"		</IconStyle>                                                                                                                                                          "        ,
		"				<LabelStyle>                                                                                                                                                  "        ,
		"			<scale>0.5</scale>                                                                                                                                                "        ,
		"		</LabelStyle>                                                                                                                                                         "        ,
		"		<ListStyle>                                                                                                                                                           "        ,
		"		</ListStyle>                                                                                                                                                          "        ,
		"	</Style>                                                                                                                                                                  "        ,
		"	<StyleMap id=\"msn_placemark_circle\">                                                                                                                                      "      ,
		"		<Pair>                                                                                                                                                                "        ,
		"			<key>normal</key>                                                                                                                                                 "        ,
		"			<styleUrl>#sn_placemark_circle</styleUrl>                                                                                                                         "        ,
		"		</Pair>                                                                                                                                                               "        ,
		"		<Pair>                                                                                                                                                                "        ,
		"			<key>highlight</key>                                                                                                                                              "        ,
		"			<styleUrl>#sh_placemark_circle_highlight</styleUrl>                                                                                                               "        ,
		"		</Pair>                                                                                                                                                               "        ,
		"	</StyleMap>                                                                                                                                                               "        ,
		"	<Folder>                                                                                                                                                                  "        ,
		"		<name>Receivers</name>                                                                                                                                                "        ,
		"		<open>1</open>                                                                                                                                                        "};

		for(String l : header) {
			out.write(l);
			out.newLine();
		}

		String [] footer = {"</Folder>",
		"</Document>",
		"</kml>"};

//		out.write(getTextFromFile(header,outfile.getName()));
		if(isWaypoints) {
			logger.debug("...Creating Waypoints");
			writeWaypointsStrings(out,points, false);
		} else {
			logger.debug("...Creating Tracks");
//			writeTrackStrings(out,points,"track", true);
		}
		logger.debug("...Creating Footer");
//		out.write(getTextFromFile(footer,outfile.getName()));
		for(String l : footer) {
			out.write(l);
			out.newLine();
		}
		out.close();
		logger.debug("...Exported " + points.size() + " points.");
	}


	public static void writeTrackStrings(BufferedWriter out, ArrayList<Point> points, String name, boolean cutdown) throws Exception {


		String s = "";

		logger.debug("...N Points = " + points.size());
		s += "<Placemark>                           \n";
		s += "	<name>"+name+"</name>                  \n";
		s += "	<styleUrl>#m_ylw-pushpin</styleUrl> \n";
		s += "	<LineString>                        \n";
		s += "		<tessellate>1</tessellate>      \n";
		s += "		<coordinates>                   \n";
		s += "		   ";
		out.write(s);
		Iterator<Point> it = points.iterator();
		while(it.hasNext()) {
			Point p = it.next();
			out.write(p.lon + "," + p.lat + "," + p.z + " ");
		}

		s = "\n";
		s += "										\n";
		s += "		</coordinates>                  \n";
		s += "	</LineString>                       \n";
		s += "</Placemark>                           \n";


		out.write(s);
//		return s;

	}

	public static void writeWaypointsScaledStrings(BufferedWriter out, ArrayList<Point> points, boolean cutdown,ArrayList<Double> scales) throws Exception {


		String s = "";

		logger.debug("...N Points = " + points.size());
		ArrayList<Double> allZs = new ArrayList<Double>();
		for(Point p : points) {
			allZs.add(p.z);
		}
		Collections.sort(allZs);
		for(Point p : points) {
//			if(p.lon < 128.7422) {
				s = "";

				String name = p.name;
				String latitude = ""  + p.lat;
				String longitude = ""  + p.lon;
				String z = ""  + p.z;

				String fullname = name; // "Rx" + nf.format(Integer.valueOf(name));

//				String latitude = convertCoordinate(lat);
//				String longitude = convertCoordinate(lon);

	//
				s += "<Placemark>" + "\n";
				s += "<name>"+fullname+"</name>" + "\n";
				if(!cutdown) {
//					s += "<name>"+fullname+"</name>" + "\n";
					s += "<LookAt>" + "\n";
					s += "<longitude>"+longitude+"</longitude>" + "\n";
					s += "<latitude>"+latitude+"</latitude>" + "\n";
					s += "	<altitude>"+z+"</altitude>" + "\n";
					s += "	<gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>" + "\n";
					s += "</LookAt>" + "\n";
				}

				int index = Collections.binarySearch(allZs, p.z);
				int scaleClass = (int) (((double) index / (double) allZs.size())*(double) scales.size());
//				System.out.println(scaleClass);
				s += "<styleUrl>#msn_placemark_circle"+scaleClass+"</styleUrl>" + "\n";
				s += "<Point>" + "\n";
				s += "	<gx:drawOrder>1</gx:drawOrder>" + "\n";
				s += "	<coordinates>"+longitude+","+latitude+",0</coordinates>" + "\n";
				s += "</Point>" + "\n";
				s += "</Placemark>	" + "\n";
				out.write(s);
//			}

		}


//		return s;

	}


	public static void writeWaypointsStrings(BufferedWriter out, ArrayList<Point> points, boolean cutdown) throws Exception {


		String s = "";

		logger.debug("...N Points = " + points.size());
		for(Point p : points) {
//			if(p.lon < 128.7422) {
				s = "";

				String name = p.name;
				String latitude = ""  + p.lat;
				String longitude = ""  + p.lon;
				String z = ""  + p.z;

				String fullname = name; // "Rx" + nf.format(Integer.valueOf(name));

//				String latitude = convertCoordinate(lat);
//				String longitude = convertCoordinate(lon);

	//
				s += "<Placemark>" + "\n";
				s += "<name>"+fullname+"</name>" + "\n";
				if(!cutdown) {
//					s += "<name>"+fullname+"</name>" + "\n";
					s += "<LookAt>" + "\n";
					s += "<longitude>"+longitude+"</longitude>" + "\n";
					s += "<latitude>"+latitude+"</latitude>" + "\n";
					s += "	<altitude>"+z+"</altitude>" + "\n";
					s += "	<gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>" + "\n";
					s += "</LookAt>" + "\n";
				}
				s += "<styleUrl>#msn_placemark_circle</styleUrl>" + "\n";
				s += "<Point>" + "\n";
				s += "	<gx:drawOrder>1</gx:drawOrder>" + "\n";
				s += "	<coordinates>"+longitude+","+latitude+",0</coordinates>" + "\n";
				s += "</Point>" + "\n";
				s += "</Placemark>	" + "\n";
				out.write(s);
//			}

		}


//		return s;

	}

	public static String getTextFromFile(File f, String name) throws Exception{
		String s ="";
		String line ="";

		BufferedReader in = new BufferedReader(new FileReader(f));

		while((line = in.readLine())!= null) {
			if(line.contains("Test.kml")) {
				s += line.replaceAll("Test.kml", name) + "\n";
			} else {
				s += line + "\n";
			}


		}
		in.close();

		return s;

	}

	public final static String OUTPUT_ID_POINT = "okml";
	public final static String OUTPUT_ID_TRACK = "okmlt";


	@Override
	public String getOutputID() {
		return OUTPUT_ID_POINT;
	}
}

