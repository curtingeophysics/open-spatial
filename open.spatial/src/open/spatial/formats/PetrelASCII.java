package open.spatial.formats;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.Text;
import open.spatial.points.Point;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class PetrelASCII extends OpenSpatialFormat{
	static final Logger logger = LogManager.getLogger(PetrelASCII.class.getName());
	public static void exportFiles(File fout, ArrayList<Point> points, boolean isUTM) throws Exception{
		logger.info("Writing Petrel ASCII File");
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
		writePetrelHeader(out);
		String line = "";
		int i = 0;
		if(isUTM) {
			for(Point p : points) {
				i++;
				out.write(p.getUTM().getEasting() + "\t" + p.getUTM().getNorthing() + "\t" + p.z + "\t0.0");
				out.newLine();
				if(i%10000 == 0) logger.info("..." + i + ": " + p.getUTM().getEasting() + "\t" + p.getUTM().getNorthing() + "\t" + p.z);

			}
		} else {
			for(Point p : points) {
				i++;
				out.write(p.lon + "\t" + p.lat + "\t" + p.z + "\t0.0");
				out.newLine();
				if(i%10000 == 0) logger.info("..." + i +p.getUTM().getEasting() + "\t" + p.getUTM().getNorthing() + "\t" + p.z );
			}
		}
		System.out.println("..." + i + " points found.");
		out.close();
	}

	public static ArrayList<Point> read(File f, int utm, char utmz) throws Exception{
		ArrayList<Point> pts = new ArrayList<>();

		BufferedReader in = new BufferedReader(new FileReader(f));
		String line = "";
		int colX = 0;
		int colY = 1;
		int colZ = 2;
		while(!(line = in.readLine()).toUpperCase().contains("BEGIN HEADER")) {
			//
		}
		int i = 0;
		while(!(line = in.readLine()).toUpperCase().contains("END HEADER")) {
			if(line.toUpperCase().contains("X")) colX = new Integer(i);
			else if(line.toUpperCase().contains("Y")) colY = new Integer(i);
			else if(line.toUpperCase().contains("Z")) colZ = new Integer(i);
			i++;
		}

		while((line = in.readLine()) != null) {
			ArrayList<Double> vals = Text.split(line);
			double e = vals.get(colX);
			double n = vals.get(colY);
			double z = vals.get(colZ);
			UTMRef r = new UTMRef(utm, utmz, e, n);
			LatLng l = r.toLatLng();
			Point p = new Point(l.getLatitude(), l.getLongitude());
			p.z = z;
			pts.add(p);

		}

		in.close();

		return pts;
	}

	private static void writePetrelHeader(BufferedWriter out) throws Exception{
		out.write("# Petrel Points with Attributes                              "); out.newLine();
		out.write("# Lines starting with # are comments                         "); out.newLine();
		out.write("VERSION 1                                                    "); out.newLine();
		out.write("BEGIN HEADER                                                 "); out.newLine();
		out.write("X                                                            "); out.newLine();
		out.write("Y                                                            "); out.newLine();
		out.write("Z                                                            "); out.newLine();
		out.write("END HEADER                                                   "); out.newLine();

	}

	public static BufferedWriter createWriter(File fin) throws Exception{
		BufferedWriter out = new BufferedWriter(new FileWriter(fin));
		writePetrelHeader(out);
		return out;
	}

	@Override
	public String getName() {
		return "Petrel ASCII Points";
	}

	@Override
	public List<String> getExtensions() {
		return new ArrayList<String>(Arrays.asList(".txt","ascii"));
	}

	@Override
	public DataTable read(File fin) {
		logger.error("STILL NEED TO IMPLEMENT");
		return null;
	}

	@Override
	public void write(DataTable table, File fout) {

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(fout));
			writePetrelHeader(out);
			for(double [] p : table.getProjectedXYZ()) {
				out.write(p[0] + "\t" + p[1] + "\t" + p[2] + "\n");
			}
			out.close();
		} catch(Exception e) {
			logger.error(e);
		}
	}

	public final static String OUTPUT_ID = "opet";


	@Override
	public String getOutputID() {
		return OUTPUT_ID;
	}
}
