package open.spatial.formats;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class OpenSpatialExportFormats {
	
	public final static int COR = 2000;
	public final static int GPX = 2100;
	public final static int KML = 2200;
	public final static int KML_TRACK = 2210;
	public final static int PETREL_ASCII = 2300;
	
	
	public static LinkedHashMap<Integer, OpenSpatialFormat> formats = getFormats();

	private static LinkedHashMap<Integer,OpenSpatialFormat> getFormats() {
		LinkedHashMap<Integer,OpenSpatialFormat> formats = new LinkedHashMap<Integer,OpenSpatialFormat>();
		
		formats.put(COR,new CORGPR());
		formats.put(GPX,new GPXFormat());
		formats.put(KML,new KML(open.spatial.formats.KML.MODE_POINT));
		formats.put(KML_TRACK,new KML(open.spatial.formats.KML.MODE_TRACK));
		formats.put(PETREL_ASCII, new PetrelASCII());
//		formats.add(new UTMASCII());
		
		
		return formats;
	}
	
	public static String getID(int id) {
		switch (id) {
			case COR: return "ocor";
			case GPX: return "ogpx";
			case KML: return "okml";
			case KML_TRACK: return "okmlt";
			case PETREL_ASCII: return "opet";		
		}
		return "Unknown";
	}
	
	
	
}
