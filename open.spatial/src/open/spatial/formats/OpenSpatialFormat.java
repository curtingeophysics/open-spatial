package open.spatial.formats;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import open.spatial.OpenSpatial;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;

public abstract class OpenSpatialFormat {


//	public abstract

	public abstract String getOutputID();
	public abstract String getName();
	public abstract List<String> getExtensions();
	public abstract DataTable read(File fin);
	public abstract void write(DataTable table, File fout);

	public int getNParameters() {
		return 1; // override this if required
	}


	public OpenSpatialProcess getExportProcess() {
		OpenSpatialProcess outProcess = new OpenSpatialProcess() {

			String path = OpenSpatial.properties.get("export.path.previous." + getOutputID());
			TextField filePath = new TextField(path);

			@Override
			public DataTable run(DataTable t) throws Exception {

				OpenSpatial.properties.store("export.path.previous." + getOutputID(),filePath.getText());
				write(t, new File(filePath.getText()));



				 try {
					 	if(super.openOnComplete.get())
					 		Desktop.getDesktop().browse(new File(filePath.getText()).toURI());
					} catch (IOException e) {
						e.printStackTrace();
					}


				return t;
			}

			@Override
			public int nParameters() {
				return 1;
			}

			@Override
			public boolean isExport() {
				return true;
			}

			@Override
			public String getName() {
				return OpenSpatialFormat.this.getName();
			}

			@Override
			public String getID() {
				return getOutputID();
			}

			@Override
			public String getFunctionName() {
				return "Export: " + getName() + " (" + getID() + ")";
			}

			@Override
			public Node createNode() {

				Button select = new Button("...",FontAwesome.FOLDER());

				select.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						FileChooser fc = new FileChooser();
						File f = fc.showSaveDialog(null);
						filePath.setText(f.getAbsolutePath());
					}
				});


				HBox b = new HBox(new Label("Path"),filePath,select);

				return b;
			}

			@Override
			public int createControlReturnRow(GridPane gp, int row) {
				return 0;
			}
		};
		return outProcess;
	}

	public static String renamePathWithLineNumber(String name, String lineid) {

		int index = name.lastIndexOf('.');
		if(index > 1) {
			String start = name.substring(0, index);
			String end = name.substring(index+1,name.length());
			name = start  + "_" + lineid + "." + end;
		} else {
			name = name + "_" + lineid;
		}
		return name;
	}
}
