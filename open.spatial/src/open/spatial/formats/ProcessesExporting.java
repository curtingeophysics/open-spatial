package open.spatial.formats;

import java.util.ArrayList;

import javafx.scene.Node;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.OpenSpatialProcessSet;

public class ProcessesExporting extends OpenSpatialProcessSet{

	@Override
	public String getName() {
		return "Export";
	}

	@Override
	public ArrayList<OpenSpatialProcess> getProcesses() {
		ArrayList<OpenSpatialProcess> processes = new ArrayList<OpenSpatialProcess>();
		processes.add(new CORGPR().getExportProcess());
		processes.add(new GPXFormat().getExportProcess());
		processes.add(new KML(KML.MODE_POINT).getExportProcess());
		processes.add(new KML(KML.MODE_TRACK).getExportProcess());
//		processes.add(new LatLonASCII().getExportProcess());
		processes.add(new PetrelASCII().getExportProcess());
		processes.add(new UTMASCII().getExportProcess());
		return processes;

	}

	@Override
	public Node getIcon(float size) {
		return FontAwesome.DOWNLOAD(size);
	}

	@Override
	protected OpenSpatialProcess create(String id) {
		switch (id) {
		case CORGPR.OUTPUT_ID: return new CORGPR().getExportProcess();
//		case GPXFormat.OUTPUT_ID: return new GPXFormat().getExportProcess();
		case KML.OUTPUT_ID_POINT: return new KML(KML.MODE_POINT).getExportProcess();
		case KML.OUTPUT_ID_TRACK: return new KML(KML.MODE_TRACK).getExportProcess();
		case UTMASCII.OUTPUT_ID: return new UTMASCII().getExportProcess();
		default:  return new UTMASCII().getExportProcess();
		}
	}

}
