package open.spatial.formats;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.OpenSpatial;
//import open.spatial.crs.WTKInstances;
import open.spatial.text.DataTable;

public class AEMCombinedASCII {
	static final Logger logger = LogManager.getLogger(AEMCombinedASCII.class.getName());

	public String getName() {
		return "AEM Sanitizer Combined ASCII";
	}

	
	public List<String> getExtensions() {
		return new ArrayList<String>(Arrays.asList(".txt"));
	}

	
	public DataTable read(File fin, boolean preview ) throws Exception {
		DataTable dt = DataTable.importDataTable(fin, new ArrayList<>(Arrays.asList("\t")), true, preview ? 100 : Integer.MAX_VALUE, 1);
//		dt.setXHeader("te");
//		dt.setYHeader("tn");
//		dt.setZHeader("dtm");
////		dt.setIDHeader("-");
//		dt.setLineHeader("line");
		return dt;
//		CSVReader r = new CSVReader();
//		ArrayList<Point> points = new ArrayList<Point>();
//		ArrayList<ArrayList<String>> headerList = new ArrayList<ArrayList<String>>();
//		ArrayList<ArrayList<String>> allvalues = new ArrayList<ArrayList<String>>();
//		int xi = -1;
//		int yi = -1;
//		int zi = -1;
//		int lineNumber = -1;
//		
//		try {
//			BufferedReader in = new BufferedReader(new FileReader(fin));
//			String header = in.readLine();
//			logger.info("Parsing " + getName());
//			
//			LinkedHashMap<String, Integer> headerValues = r.parseHeader(header, '\t');
//			
//			headerList.add(new ArrayList(headerValues.keySet()));
//			
//			logger.info("...Header (" + headerValues.size() + ")");
//			String line = "";
//			for(String k : headerValues.keySet()) {
//				logger.info(k + ": " + headerValues.get(k));
//			}
//			
//			 xi = headerValues.get("te");
//			 yi = headerValues.get("te");
//			 zi = headerValues.get("dtm");
//			 lineNumber = headerValues.get("line");
//			
//			while((line = in.readLine()) != null) {
//				List<String> values = r.parseLine(line, '\t');
//				allvalues.add(new ArrayList<>(values));
//				
//				double x = Double.valueOf(values.get(xi));
//				double y = Double.valueOf(values.get(yi));
//				double z = Double.valueOf(values.get(zi));
//				UTMRef ref = new UTMRef(latZone, lngZone, x, y);
//				Point p = new Point(ref, z);
//				p.line = values.get(lineNumber);
//				points.add(p);
//			}
//			logger.info("...Values (" + headerValues.size() + ")");
//			
//		} catch(Exception e) {
//			logger.error(e);
//		}
//		DataTable dt = new DataTable(headerList,allvalues);
//		dt.linecol.set(lineNumber);
//		dt.xcol.set(xi);
//		dt.ycol.set(yi);
//		dt.zcol.set(zi);
	}


	public void write(DataTable table, File fout) {
//		CSVReader r = new CSVReader();
//		try {
//			BufferedReader in = new BufferedReader(new FileReader(fout));
//			String header = in.readLine();
//			LinkedHashMap<String, Integer> headerValues = r.parseHeader(header, '\t');
//			String line = "";
//			while((line = in.readLine()) != null) {
//				List<String> values = r.parseLine(line, '\t');
//			}
//			
//			
//		} catch(Exception e) {
//			logger.error(e);
//		}
	}

	
	public static void main(String[] args) {
//		OpenSpatial.initLogger();
//		File f = new File("D:\\20_Work\\2018_AEM_Australia\\AEM_AusAEM_Goldfields\\AEM_Tempest_25Hz_Eastern_Goldfields\\combined.txt");
//		File fout = new File("D:\\20_Work\\2018_AEM_Australia\\AEM_AusAEM_Goldfields\\AEM_Tempest_25Hz_Eastern_Goldfields\\goldfields.kml");
////		BufferedReader in = new BufferedReader(new FileReader(f));
//		AEMCombinedASCII a = new AEMCombinedASCII();
//		try {
//			DataTable dt = a.read(f,false);
//			dt.setInputCRS(WTKInstances.WTK_GDA94_Z52S);
//			KML k = new KML(KML.MODE_TRACK);
//			k.write(dt, fout);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
	}
	
}
