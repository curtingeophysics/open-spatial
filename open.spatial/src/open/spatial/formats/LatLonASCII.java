package open.spatial.formats;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.Text;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;


public class LatLonASCII {
	static final Logger logger = LogManager.getLogger(LatLonASCII.class.getName());
	public static ArrayList<Point> loadLatLonPoints(String absolutePath,int colid,int colx,int coly, int colz, int nHeaderLines, boolean preview) {

		File f = new File(absolutePath);
		ArrayList<Point> points = new ArrayList<>();
		try {
			logger.info("Loading Lat Lon Point File..." + absolutePath);
			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = "";
			for(int i = 0 ; i < nHeaderLines ; i++) {
				logger.info("Skipping Header Line "+(i+1)+": " + in.readLine()); //skip header
			}
			while((line = in.readLine()) !=null) {
				
				String [] split = Text.sanitizeString(line);				
				String id = (colid == -1) ? "Unknown" : split[colid];
//				for(int i =0 ; i < split.length ; i++) {
//					System.out.println((i+1) + "\t" + split[i]);
//				}
				try {
				if(split.length >= 2) {
//					int i = 0;
//					for(String s : split) {
//						
//						System.out.println(i++ + ": " + s);
//					}
					double lat = Double.valueOf(split[coly]);
					double lon = Double.valueOf(split[colx]);
	
					double z = (colz == -1 || colz >= split.length) ? 0.0 : Double.valueOf(split[colz]);
					Point p = new Point(lat, lon);
	
					p.z = z;
					p.name = id;
					points.add(p);
					if(preview) {
						in.close();
						return points;
					}
				}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}

			logger.info("Loaded " + points.size() + " points.");

			in.close();
		} catch(Exception e ) {
			e.printStackTrace();
		}

		return points;
	}
	public static ArrayList<Point> clipLatLon(ArrayList<Point> pts, double valLatMin, double valLatMax,double valLonMin, double valLonMax) {
		logger.info("Clipping Points" );
		ArrayList<Point> points = new ArrayList<Point>();
		for(Point p : pts) {
			if(p.lat >= valLatMin && p.lat <= valLatMax) {
				if(p.lon >= valLonMin && p.lon <= valLonMax) {
					points.add(p);
				}
			}
		}
		return points;
	}
	public static void printPointsLatLon(ArrayList<Point> pts, File fout) throws Exception {
			logger.info("Writing Lat-Lon ASCII File: " + fout.getAbsolutePath());
			BufferedWriter out = new BufferedWriter(new FileWriter(fout));
			out.write(("P,X,Y,Z"));
			out.newLine();
			for(Point p : pts) {
				LatLng pl = new LatLng(p.lat, p.lon);
				out.write(p.name +"," + pl.getLatitude() + "," + pl.getLongitude() +"," + p.z);
				out.newLine();
			}
			out.close();

	}
	public static void createGrid(ArrayList<Point> modifiedPoints, File outputFile, double dlat, double dlon) throws Exception{
		logger.info("Writing Lat-Lon ASCII File: " + outputFile.getAbsolutePath());
		logger.info("...Creating Grid...");
		double minLat = modifiedPoints.get(0).lat;
		double minLon = modifiedPoints.get(0).lon;
		double maxLat = modifiedPoints.get(0).lat;
		double maxLon = modifiedPoints.get(0).lon;
		for(Point p : modifiedPoints) {
			minLat = Math.min(minLat, p.lat);
			minLon = Math.min(minLon, p.lon);
			maxLat = Math.max(maxLat, p.lat);
			maxLon = Math.max(maxLon, p.lon);
		}
		ArrayList<Double> lats = new ArrayList<Double>();
		ArrayList<Double> lons = new ArrayList<Double>();
		for(double l = minLat ; l <= maxLat - dlat ; l += dlat) {
			lats.add(new Double(l));
		}
		for(double l = minLon ; l <= maxLon - dlon ; l += dlon) {
			lons.add(new Double(l));
		}
		int [][] counts = new int [lats.size()][lons.size()];
		for(Point p : modifiedPoints) {
			int nearestLat = Collections.binarySearch(lats, p.lat);
			int nearestLon = Collections.binarySearch(lons, p.lon);

			nearestLat = nearestLat >= 0 ? nearestLat : Math.max(0,-nearestLat-2);
			nearestLon = nearestLon >= 0 ? nearestLon : Math.max(0,-nearestLon-2);
			counts[nearestLat][nearestLon]++;
		}
		BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
		out.write("Lat\tLon\tCount");
		out.newLine();

		for(int i = 0 ; i < lats.size() ; i++) {
			for(int j = 0 ; j < lons.size() ; j++) {
				if(counts[i][j] != 0) {
					out.write(lats.get(i) + "\t" + lons.get(j) + "\t" + counts[i][j]);
					out.newLine();
				}

			}
		}

		out.close();
	}


}
