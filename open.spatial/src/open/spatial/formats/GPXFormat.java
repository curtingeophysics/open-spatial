package open.spatial.formats;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;

//import javax.xml.bind.JAXBElement;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.vesalainen.gpx.GPX;
import org.vesalainen.jaxb.gpx.ExtensionsType;
import org.vesalainen.jaxb.gpx.GpxType;
import org.vesalainen.jaxb.gpx.TrkType;
import org.vesalainen.jaxb.gpx.TrksegType;
import org.vesalainen.jaxb.gpx.WptType;

import open.spatial.points.Point;
import open.spatial.text.DataTable;

public class GPXFormat extends OpenSpatialFormat {
	static final Logger logger = LogManager.getLogger(GPXFormat.class.getName());
	public static ArrayList<Point> read(String path, boolean preview) throws Exception{
		logger.info("Reading GPX File: " + path);
		ArrayList<Point> points = new ArrayList<Point>();

		InputStream is = new FileInputStream(new File(path));
		GPX g = new GPX(is);
		JAXBElement<GpxType> gpx1 = g.getGpx();
        GpxType gpxType = gpx1.getValue();
        List<TrkType> trkList = gpxType.getTrk();
        for (TrkType trk : trkList)      {
            String name = trk.getName();

            ExtensionsType extensions = trk.getExtensions();
                List<TrksegType> trksegList = trk.getTrkseg();
                for (TrksegType tt : trksegList) {
                	List<WptType> trkptList = tt.getTrkpt();
                    for (WptType wt : trkptList)
                    {
                    	Point pt = new Point(wt.getLat().doubleValue(), wt.getLon().doubleValue());
                    	pt.name = wt.getName();
                    	pt.line = name;
//                    	System.out.println(name + "\t" + pt);
                    	points.add(pt);
                    	if(preview) {
            				is.close();
            				return points;
            			}
//                    	System.out.println(name + ": " + wt.getName() + "\t" + wt.getLat() + "\t" + wt.getLon());
                    }
                }


        }
        for(WptType wt : gpxType.getWpt()) {
        	Point pt = new Point(wt.getLat().doubleValue(), wt.getLon().doubleValue());
        	pt.name = wt.getName();
        	points.add(pt);
        	if(preview) {
				is.close();
				return points;
			}
        }
    	is.close();
        return points;
	}

	public static void writeTrack(ArrayList<Point> pts, String path) throws Exception {
		logger.info("Writing GPX Track File: " + path);
		GPX g = new GPX();
		FileWriter fout= new FileWriter(new File(path));
		JAXBElement<GpxType> gpx1 = g.getGpx();
		GpxType gpxType = gpx1.getValue();
		gpxType.setCreator("AMP LOLZ");
//		gpxType.getTrk().a
		TrksegType track = new TrksegType();
		List<WptType> list = track.getTrkpt();
		for(Point pt : pts) {
			WptType wp = new WptType();
			wp.setLat(new BigDecimal(pt.lat));
			wp.setLon(new BigDecimal(pt.lon));
			wp.setName(pt.name);
			wp.setDesc(pt.name);
			wp.setCmt(pt.name);
			list.add(wp);
		}
		TrkType t = new TrkType();
		t.getTrkseg().add(track);
		gpxType.getTrk().add(t);

		g.write(fout);
	}
	public static void writeWaypoints(ArrayList<Point> pts, String path) throws Exception {
		logger.info("Writing GPX Waypoint File: " + path);
		GPX g = new GPX();
		FileWriter fout= new FileWriter(new File(path));
		JAXBElement<GpxType> gpx1 = g.getGpx();
		GpxType gpxType = gpx1.getValue();
		gpxType.setCreator("AMP LOLZ");
//		gpxType.getTrk().a

		List<WptType> list = gpxType.getWpt();
		for(Point pt : pts) {
			WptType wp = new WptType();
			wp.setLat(new BigDecimal(pt.lat));
			wp.setLon(new BigDecimal(pt.lon));
			wp.setName(pt.name);
			wp.setDesc(pt.name);
			wp.setCmt(pt.name);
			list.add(wp);
		}

		g.write(fout);
	}

	@Override
	public String getName() {
		return "GPX GPS";
	}

	@Override
	public List<String> getExtensions() {
		return Arrays.asList(".gpx");
	}

	@Override
	public DataTable read(File fin) {
		try {
			DataTable table =  new DataTable(read(fin.getAbsolutePath(), false));
			return table;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void write(DataTable table, File fout) {
		logger.info("Writing GPX File to " + fout.getAbsolutePath());
		LinkedHashMap<String, ArrayList<Point>>  lineMap = table.getLinesPointsHash();

		if(lineMap.size() > 1) {
			logger.warn("Multiple Lines Detected: Writing to several files.");
			for(String lineID : lineMap.keySet()) {
				String path = renamePathWithLineNumber(fout.getAbsolutePath(), lineID);
				try {
					writeTrack(lineMap.get(lineID), path);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			try {
				writeTrack(table.getPoints(), fout.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public final static String OUTPUT_ID = "ogpx";

	@Override
	public String getOutputID() {
		return OUTPUT_ID;
	}

}
