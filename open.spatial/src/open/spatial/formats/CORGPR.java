package open.spatial.formats;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.points.Point;
import open.spatial.text.DataTable;



public class CORGPR extends OpenSpatialFormat {
	static final Logger logger = LogManager.getLogger(CORGPR.class.getName());



	public static ArrayList<Point> read(String path, boolean preview) {
		logger.info("Importing COR File: " + path);
		logger.debug("....Assuming lat-lon-metres input");
		ArrayList<Point> points = new ArrayList<Point>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(path)));
			String line = "";
			while((line = in.readLine()) != null) {

				line = line.replaceAll(" ", "\t").replaceAll(",", "\t");
				String [] split = line.split("\t");
				if(split.length >= 10) {
					int i = 0;
					String id = split[i++];
					String date = split[i++];
					String time = split[i++];
					String latitude = split[i++];
					String latitudeDirection = split[i++];
					String longitude = split[i++];
					String longitudeDirection = split[i++];
					String elevation = split[i++];
					String units = split[i++];
					String unknown = split[i++];

					double lat = latitudeDirection.toUpperCase().contains("S") ? -Double.valueOf(latitude) : Double.valueOf(latitude);
					double lon = longitudeDirection.toUpperCase().contains("W") ? -Double.valueOf(longitude) : Double.valueOf(longitude);
					Point pt = new Point(lat, lon, Double.valueOf(elevation));
					pt.name = id;
					points.add(pt);
					if(preview) {
						in.close();
						return points;
					}
				} else {
					logger.error("Not importing line " + line);
				}
			}
			logger.info("...Imported " + points.size() + " points." );
			in.close();
		} catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}


		return points;
	}
	public static void writeMetre(ArrayList<Point> points, String path) {
		logger.info("Writing COR File to " + path);

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(path)));

//			859	2018-07-25	06:09:12:00	32.00633050000	S	115.88997933333	E	-8.4	M	1
			SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss:00");
			Date current = new Date();

			for(Point pt : points) {
				String id = pt.name;
				String date = formatterDate.format(current);
				String time = formatterTime.format(current);
				String easting = "" + pt.getUTM().getEasting();
				String northing = "" + pt.getUTM().getNorthing();
				String latitude = "" + Math.abs(pt.lat);
				String latitudeDirection = (pt.lat < 0) ? "S" : "N";
				String longitude = "" + Math.abs(pt.lon);
				String longitudeDirection = (pt.lon < 0) ? "W" : "E";
				String elevation = "" + pt.z;
//				System.out.println(elevation);
				String units = "M";
				String unknown = "0";
//				String [] split = {id,date,time,latitude,latitudeDirection,longitude,longitudeDirection,elevation,units,unknown};
				out.write(id +"\t"+date+"\t"+time+"\t"+northing+"\t"+latitudeDirection+"\t"+easting+"\t"+longitudeDirection+"\t"+elevation+"\t"+units+"\t"+unknown);
				out.newLine();
			}
			out.close();
		} catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	public static void write(ArrayList<Point> points, String path) {
		logger.info("Writing COR File to " + path);

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(path)));

//			859	2018-07-25	06:09:12:00	32.00633050000	S	115.88997933333	E	-8.4	M	1
			SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss:00");
			Date current = new Date();

			for(Point pt : points) {
				String id = pt.name;
				String date = formatterDate.format(current);
				String time = formatterTime.format(current);
				String latitude = "" + Math.abs(pt.lat);
				String latitudeDirection = (pt.lat < 0) ? "S" : "N";
				String longitude = "" + Math.abs(pt.lon);
				String longitudeDirection = (pt.lon < 0) ? "W" : "E";
				String elevation = "" + pt.z;
//				System.out.println(elevation);
				String units = "M";
				String unknown = "0";
//				String [] split = {id,date,time,latitude,latitudeDirection,longitude,longitudeDirection,elevation,units,unknown};
				out.write(id +"\t"+date+"\t"+time+"\t"+latitude+"\t"+latitudeDirection+"\t"+longitude+"\t"+longitudeDirection+"\t"+elevation+"\t"+units+"\t"+unknown);
				out.newLine();
			}
			out.close();
		} catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	@Override
	public String getName() {
		return "Reflex COR GPR";
	}
	@Override
	public List<String> getExtensions() {
		return Arrays.asList(".cor",".txt");
	}
	@Override
	public DataTable read(File fin) {
		logger.info("Importing COR File: " + fin.getAbsolutePath());
		logger.debug("....Assuming lat-lon-metres input");

		ArrayList<String> header = new ArrayList<String>();
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

		ArrayList<Point> points = new ArrayList<Point>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(fin.getAbsolutePath())));
			String line = "";
			header.add("id");
			header.add("date");
			header.add("time");
			header.add("latitude");
			header.add("latitudeDirection");
			header.add("longitude");
			header.add("longitudeDirection");
			header.add("elevation");
			header.add("units");
			header.add("unknown");

			while((line = in.readLine()) != null) {

				line = line.replaceAll(" ", "\t").replaceAll(",", "\t");
				String [] split = line.split("\t");


				if(split.length >= 10) {
					ArrayList<String> row = new ArrayList<String>(Arrays.asList(split));
					values.add(row);
					int i = 0;

					String id = split[i++]; //0
					String date = split[i++]; //1
					String time = split[i++]; //2
					String latitude = split[i++]; //3
					String latitudeDirection = split[i++]; //4
					String longitude = split[i++]; //5
					String longitudeDirection = split[i++]; //6
					String elevation = split[i++]; //7
					String units = split[i++]; //8
					String unknown = split[i++]; //9



					double lat = latitudeDirection.toUpperCase().contains("S") ? -Double.valueOf(latitude) : Double.valueOf(latitude);
					double lon = longitudeDirection.toUpperCase().contains("W") ? -Double.valueOf(longitude) : Double.valueOf(longitude);
					row.add("" + lat);
					row.add("" + lon);
					Point pt = new Point(lat, lon, Double.valueOf(elevation));
					pt.name = id;
					points.add(pt);

				} else {
					logger.error("Not importing line " + line);
				}


			}


			logger.info("...Imported " + points.size() + " points." );
			in.close();
		} catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		ArrayList<ArrayList<String>> headerLine = new ArrayList<ArrayList<String>>();
		headerLine.add(header);
		DataTable dt = new DataTable(headerLine, values);
		dt.xcol.set(10);
		dt.ycol.set(11);
		dt.zcol.set(7);

		dt.setPoints(points);
		return dt;
	}
	@Override
	public void write(DataTable table, File fout) {
		logger.info("Writing COR File to " + fout.getAbsolutePath());
		LinkedHashMap<String, ArrayList<Point>>  lineMap = table.getLinesPointsHash();

		if(lineMap.size() > 1) {
			logger.warn("Multiple Lines Detected: Writing to several files.");
			for(String lineid : lineMap.keySet()) {


				String name = fout.getAbsolutePath();
				name = renamePathWithLineNumber(name,lineid);


				logger.info("...Writing Line " + lineid + " to " + name);
				try {
					BufferedWriter out = new BufferedWriter(new FileWriter(name));

//					859	2018-07-25	06:09:12:00	32.00633050000	S	115.88997933333	E	-8.4	M	1
					SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss:00");
					Date current = new Date();

					int nth = 0;
					for(Point pt : lineMap.get(lineid)) {
						String id = pt.name;

						ArrayList<String> dateColumn = table.getColumn("date");
						ArrayList<String> dateTime = table.getColumn("time");



						String date = dateColumn == null ? formatterDate.format(current) : dateColumn.get(nth);
						String time = dateTime == null ? formatterTime.format(current) : dateTime.get(nth);
						String latitude = "" + Math.abs(pt.lat);
						String latitudeDirection = (pt.lat < 0) ? "S" : "N";
						String longitude = "" + Math.abs(pt.lon);
						String longitudeDirection = (pt.lon < 0) ? "W" : "E";
						String elevation = "" + pt.z;
		//				System.out.println(elevation);
						String units = "M";
						String unknown = "0";
		//				String [] split = {id,date,time,latitude,latitudeDirection,longitude,longitudeDirection,elevation,units,unknown};
						out.write(id +"\t"+date+"\t"+time+"\t"+latitude+"\t"+latitudeDirection+"\t"+longitude+"\t"+longitudeDirection+"\t"+elevation+"\t"+units+"\t"+unknown);
						out.newLine();
						nth++;
					}
					//

					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}


		} else {


			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(fout));

	//			859	2018-07-25	06:09:12:00	32.00633050000	S	115.88997933333	E	-8.4	M	1
				SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss:00");
				Date current = new Date();

				int nth = 0;
				for(Point pt : table.getPoints()) {
					String id = pt.name;

					ArrayList<String> dateColumn = table.getColumn("date");
					ArrayList<String> dateTime = table.getColumn("time");



					String date = dateColumn == null ? formatterDate.format(current) : dateColumn.get(nth);
					String time = dateTime == null ? formatterTime.format(current) : dateTime.get(nth);
					String latitude = "" + Math.abs(pt.lat);
					String latitudeDirection = (pt.lat < 0) ? "S" : "N";
					String longitude = "" + Math.abs(pt.lon);
					String longitudeDirection = (pt.lon < 0) ? "W" : "E";
					String elevation = "" + pt.z;
	//				System.out.println(elevation);
					String units = "M";
					String unknown = "0";
	//				String [] split = {id,date,time,latitude,latitudeDirection,longitude,longitudeDirection,elevation,units,unknown};
					out.write(id +"\t"+date+"\t"+time+"\t"+latitude+"\t"+latitudeDirection+"\t"+longitude+"\t"+longitudeDirection+"\t"+elevation+"\t"+units+"\t"+unknown);
					out.newLine();
					nth++;
				}
				out.close();
			} catch(Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public final static String OUTPUT_ID = "ocor";

	@Override
	public String getOutputID() {
		return OUTPUT_ID;
	}



}
