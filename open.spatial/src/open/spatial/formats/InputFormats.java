package open.spatial.formats;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.cli.Option;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import open.spatial.OpenSpatial;

public class InputFormats {

	public static LinkedHashMap<Option, List<String>>  extensions = createExtensions();
	public static LinkedHashMap<ExtensionFilter, Option>  extensionFilters = createExtensionFilters();

	private static LinkedHashMap<Option, List<String>>  createExtensions() {
		LinkedHashMap<Option, List<String>> extensions = new LinkedHashMap<Option, List<String> >();

		extensions.put(OpenSpatial.INPUT_ASCII_UTM,  Arrays.asList(".txt",".csv",".ascii","*.*"));
		extensions.put(OpenSpatial.INPUT_ASCII_LATLON,  Arrays.asList("txt",".csv",".ascii","*.*"));
		extensions.put(OpenSpatial.INPUT_KML, Arrays.asList("*.kml"));
		extensions.put(OpenSpatial.INPUT_GPX,  Arrays.asList("*.gpx"));
		extensions.put(OpenSpatial.INPUT_COR,  Arrays.asList("*.cor"));
		extensions.put(OpenSpatial.INPUT_CUSTOM_WTK,  Arrays.asList("*.wtk"));
		extensions.put(OpenSpatial.INPUT_SGY,  Arrays.asList("*.sgy","*.segy"));
		return extensions;
	}

	private static LinkedHashMap<ExtensionFilter,Option> createExtensionFilters() {
		LinkedHashMap<ExtensionFilter,Option> filters = new LinkedHashMap<FileChooser.ExtensionFilter,Option>();
		for(Option o : InputFormats.extensions.keySet()) {
			List<String> extensions = InputFormats.extensions.get(o);
			ExtensionFilter e = new ExtensionFilter(o.getDescription(), extensions);
			filters.put(e,o);

		}
		return filters;
	}



}
