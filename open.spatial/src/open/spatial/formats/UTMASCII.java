package open.spatial.formats;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.Text;
import open.spatial.Transect;
import open.spatial.crs.CoordinateTransformation;
import open.spatial.crs.WTKInstance;
import open.spatial.points.Point;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class UTMASCII extends OpenSpatialFormat{
	static final Logger logger = LogManager.getLogger(UTMASCII.class.getName());
	public final static void printPointsUTM(ArrayList<Point> pts, File fout, int zoneN, char zoneID) throws Exception{
		logger.info("Writing UTM ASCII File: " + fout.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
			out.write(("P,X,Y,Z"));
			out.newLine();
			for(Point p : pts) {
				LatLng pl = new LatLng(p.lat, p.lon);
				UTMRef pu = pl.toUTMRef();
				out.write(p.name +"," + pu.getEasting() + "," + pu.getNorthing() + "," + p.z);
				out.newLine();
			}

		out.close();

	}
	public final static void printPointsUTMLatLon(ArrayList<Point> pts, File fout) throws Exception{
		logger.info("Writing ASCII File: " + fout.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
		logger.info("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon");
		out.write("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon");
		out.newLine();
		for(Point p: pts) {
			LatLng pl = new LatLng(p.lat, p.lon);
			UTMRef r = pl.toUTMRef();
//			logger.info(p.name + "," + r.getEasting() + "," + r.getNorthing() + "," + p.z +"," + r.getLngZone() +"," + r.getLatZone() +"," + p.lat + "," + p.lon);
			out.write(p.name + "," + r.getEasting() + "," + r.getNorthing() + "," + p.z +"," + r.getLngZone() +"," + r.getLatZone() +"," + p.lat + "," + p.lon);
			out.newLine();
		}

		out.close();

	}
	public static ArrayList<Point> loadUTMPoints(String absolutePath, int zoneNumber, char zoneID,int colid,int colx,int coly, int colz, int nHeaderLines, boolean preview) {
		File f = new File(absolutePath);
		ArrayList<Point> points = new ArrayList<>();
		try {
			logger.info("Loading UTM Point File..." + absolutePath);
			BufferedReader in = new BufferedReader(new FileReader(f));


			String line = "";
			for(int i = 0 ; i < nHeaderLines ; i++){
				logger.info("Skipping Header Line "+(i+1)+": " + in.readLine()); //skip header
			}
			while((line = in.readLine()) !=null) {
				String [] split = Text.sanitizeString(line);
				String id = (colid <= -1) ? "Unknown" : split[colid];
				if(split.length >= 2) {
					double easting = Double.valueOf(split[colx]);
					double northing = Double.valueOf(split[coly]);
					double z = (colz <= -1 || colz >= split.length) ? 0.0 : Double.valueOf(split[colz]);

	//				System.out.println(easting + "\t" + northing);
					UTMRef r = new UTMRef(zoneNumber, zoneID, easting, northing);
					LatLng ll = r.toLatLng();

					Point p = new Point(ll.getLatitude(), ll.getLongitude());
					p.z = z;
					p.name = id;
					points.add(p);
					if(preview) {
						in.close();
						return points;
					}
				}
			}


			in.close();
		} catch(Exception e ) {
			e.printStackTrace();
		}

		return points;
	}
	public static ArrayList<Point> interpolate(ArrayList<Point> pts, double dx, int zoneNumber, char zoneID, boolean useLinearInterpolation) {
		logger.info("Interpolating Points in UTM Coords " + zoneNumber + "" + zoneID + " @" + dx + "m" );

		ArrayList<UTMRef> positions = new ArrayList<UTMRef>();
		for(Point p : pts) {
			LatLng l = new LatLng(p.lat,p.lon);
			UTMRef ref = l.toUTMRef();
			positions.add(ref);
		}
		Transect t = new Transect(positions);
		ArrayList<Point> points = new ArrayList<Point>();
		for(double offset = 0 ; offset <= t.totalLength ; offset+= dx) {
			Point p = useLinearInterpolation ? t.interpolateOffsetLinear(offset, zoneID, zoneNumber) : t.interpolateOffset(offset, zoneID, zoneNumber);
//			System.out.println(offset + "\t" + new LatLng(p.lat, p.lon).toUTMRef());
			points.add(p);
		}
		logger.debug("...Interpolated "  + points.size() + " points.");
		return points;
	}
	@Override
	public String getName() {
		return "ASCII Selected Coordinates";
	}
	@Override
	public List<String> getExtensions() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public DataTable read(File fin) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void write(DataTable table, File fout) {
		try {

			logger.info("Writing ASCII File: " + fout.getAbsolutePath());
			BufferedWriter out = new BufferedWriter(new FileWriter(fout));
			WTKInstance wtk = table.wtkOutputInstance;
			

			if(table.customTranformEnabled) {
				
				String customx = "Local_x";
				String customy = "Local_y";
				String customz = "Local_z";
				
				CoordinateTransformation tf = table.customTranform;
				CoordinateTransformation.saveTransformation(tf, new File("Projections/Custom/default.proj"));
				logger.info("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon,"+customx + "," + customy + "," + customz);
				out.write("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon,"+customx + "," + customy + "," + customz);
				out.newLine();
				ArrayList<double []> ptsproj = 	table.getLocalProjectedXYZ();

				int ith = 0;
				for(Point p: table.getPoints()) {
					LatLng pl = new LatLng(p.lat, p.lon);
					UTMRef r = pl.toUTMRef();
					double [] pr = ptsproj.get(ith);

					String line = p.name + "," + r.getEasting() + "," + r.getNorthing() + "," + p.z +"," + r.getLngZone() +"," + r.getLatZone() +"," + p.lat + "," + p.lon + "," + pr[0] + "," + pr[1] + "," + pr[2];
					logger.info(line);
					out.write(line);
					out.newLine();
					ith++;
				}
			} else {
				String wtkx = wtk.getName() + "_x";
				String wtky = wtk.getName() + "_y";
				String wtkz = wtk.getName() + "_z";
				logger.info("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon,"+wtkx + "," + wtky + "," + wtkz);
				out.write("ID,Easting,Northing,Z,ZoneNumber,ZoneID,Lat,Lon,"+wtkx + "," + wtky + "," + wtkz);
				out.newLine();
				ArrayList<double []> ptsproj = 	table.getProjectedXYZ();

				int ith = 0;
				for(Point p: table.getPoints()) {
					LatLng pl = new LatLng(p.lat, p.lon);
					UTMRef r = pl.toUTMRef();
					double [] pr = ptsproj.get(ith);

					String line = p.name + "," + r.getEasting() + "," + r.getNorthing() + "," + p.z +"," + r.getLngZone() +"," + r.getLatZone() +"," + p.lat + "," + p.lon + "," + pr[0] + "," + pr[1] + "," + pr[2];
					logger.info(line);
					out.write(line);
					out.newLine();
					ith++;
				}
			}
			
			
//			table.getProjectedXYZ()

			

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public final static String OUTPUT_ID = "o";


	@Override
	public String getOutputID() {
		return OUTPUT_ID;
	}
}
