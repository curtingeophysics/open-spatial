package open.spatial.examples;

import java.io.File;

import open.spatial.OpenSpatial;

public class RottnestDom {
	public static void main(String[] args) {
		File inDir = new File("ExampleData/Rottnest_Dom/");
		File outDir = new File("ExampleData/Rottnest_Dom/Out/");
		outDir.mkdirs();
		int [] spacings = {5};
		for(File f : inDir.listFiles()) {
			if(f.isFile()) {
				for(int spacing : spacings ) {
					
//					String command1 = "-iutm " + f.getAbsolutePath() + " " + " -okml " +dirpathout + "/" + f.getName() +".kml " + "-utm 50 H -colx 2 -coly 3 -colz 4 -colid 1 -mapd C:\\temp\\thomson\\CLIP_0_0.asc";
					String command1 = "-ill " + f.getAbsolutePath() + " " + " -okml " +outDir.getAbsolutePath() + "/" + f.getName() +".kml " + "-utm 50 H -colx 5 -coly 6 -colz 7 -colid 1 -p" ;
					
			
					try {
						OpenSpatial.main((command1.split(" ")));
//						OpenSpatial.main((command2.split(" ")));
//						OpenSpatial.main((command3.split(" ")));
					} catch(Exception e) {
						e.printStackTrace();
					}
			}
			}
		}
		
	}
}
