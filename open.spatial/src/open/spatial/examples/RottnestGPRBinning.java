package open.spatial.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class RottnestGPRBinning{
	public static void main(String[] args)  throws Exception {
		File f = new File("E:\\2021_GPR_Rottnest_Island\\Output\\GPR_Points.txt");
		File dir = new File("E:\\2021_GPR_Rottnest_Island\\Output\\Binned\\");
		
		BufferedReader in = new BufferedReader(new FileReader(f));
		
		String header = in.readLine();
		
		String line = "";
		
		double [] depths = {0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,999999};
		
		LinkedHashMap<Integer, ArrayList<String>> depthMap = new LinkedHashMap<Integer, ArrayList<String>>();
		
		
		while((line = in.readLine()) != null) {
			line = line.replaceAll(" 1", "\t1");
			String [] sp = line.split("\t");
			if(sp.length > 6) {
			double depth = Double.valueOf(sp[6]);
			
			boolean found = false;
			for(int i = 0 ; i < depths.length ; i++) {
				if(!found) {
					if(depth <= depths[i]) {
						ArrayList<String>strings = depthMap.get(i);
						if(strings == null) {
							strings = new ArrayList<String>();
							depthMap.put(i, strings);
						}
						strings.add(line);
						found = true;
					} 
				}
			}
			if(!found) {
				System.err.println("SOMETHING WRONG");
			}
			
//			for(int i = 0 ; i< sp.length ; i++) {
//				System.out.println(i + ": " + sp[i] );
//				
//			}
			}
		}
		for(Integer depthKey : depthMap.keySet()) {
			ArrayList<String> lines = depthMap.get(depthKey);
			double depth = depths[depthKey];
			String fileDepth = "";
			if(depthKey == 0) {
				fileDepth = ("" + depths[0]).replaceAll("\\.", "_") + ".txt";
			} else if(depthKey == (depths.length -1)) {
				fileDepth = ("1.6").replaceAll("\\.", "_") + ".txt";
			} else {
				fileDepth = ("" + depths[depthKey-1] + "_to_" + depths[depthKey]).replaceAll("\\.", "_") + ".txt";
			}
			File fout = new File(dir.getAbsolutePath() + "/" + fileDepth);
			BufferedWriter out = new BufferedWriter(new FileWriter(fout));
			out.write(header.replaceAll(" ", "\t"));
			out.newLine();
			for(String l : lines) {
				out.write(l);
				out.newLine();
			}
			out.close();
			
		}
		
		in.close();
		
	}
	
	
	
}
