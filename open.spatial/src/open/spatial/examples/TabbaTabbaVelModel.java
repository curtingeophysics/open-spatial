package open.spatial.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javafx.geometry.Point2D;
import open.spatial.OpenSpatial;
import open.spatial.Text;
import open.spatial.points.Point;

public class TabbaTabbaVelModel {
	public static void main(String[] args) throws Exception{
		File vfile = new File("ExampleData/TabbaTabba/1_CDPs_Without_Coordinates.txt");
		File cfile = new File("ExampleData/TabbaTabba/1_Coordinates_of_CDPs.txt");
		File fout = new File("ExampleData/TabbaTabba/Velocity_XYZ_1.txt");
		BufferedReader cin = new BufferedReader(new FileReader(cfile));
		BufferedReader vin = new BufferedReader(new FileReader(vfile));
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
		String line = "";
		System.out.println(cin.readLine());
		LinkedHashMap<Integer, Point2D> cdps = new LinkedHashMap<Integer, Point2D>();
		while((line = cin.readLine()) !=null) {
			ArrayList<Double> split = Text.split(line);
			int cdp = split.get(0).intValue();
			double x = split.get(1);
			double y = split.get(2);
			cdps.put(cdp, new Point2D(x, y));
			
//			System.out.println(line);
		}
		cin.close();
		line = "";
		
		vin.readLine(); //header
		vin.readLine(); //header
		
		while((line = vin.readLine()) !=null) {
//			System.out.println(line);
			ArrayList<Double> split = Text.split(line);
			int cdp = split.get(0).intValue();
			double x = cdps.get(cdp).getX();
			double y = cdps.get(cdp).getY();
			double depth = split.get(1);
			double vel = split.get(2);
			double elev = split.get(3)-depth;	
			out.write(x + "\t"+ y + "\t" + elev +"\t" + vel);
			out.newLine();
//			System.out.println(line);
		}
		out.close();
		vin.close();
		
	
	}
}
