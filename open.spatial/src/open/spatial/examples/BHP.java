package open.spatial.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import open.spatial.OpenSpatial;
import open.spatial.Text;

public class BHP {


	public static void exportLatLon() throws Exception{
		File dir = new File("C:\\temp\\BHP\\Run");
		BufferedWriter out = new BufferedWriter(new FileWriter(new File("ExampleData/BHP/mt_coords.txt")));
		for(File f : dir.listFiles()) {
			BufferedReader in = new BufferedReader(new FileReader(f));
			String line = in.readLine(); //header
			String firstLine = in.readLine();
			if(firstLine != null) {
				ArrayList<Double> vals = Text.split(firstLine);
				double lon = vals.get(1);
				double lat = vals.get(2);
				double elev = vals.get(1);
				out.write(lon + "\t" + lat + "\t" + elev + "\t" + f.getName());
				out.newLine();
				in.close();

			}
		}
		out.close();
	}

	public static void main(String[] args) {
		String command = "-ill ExampleData/BHP/mt_coords.txt -okml ExampleData/BHP/bhpmt.kml  -o ExampleData/BHP/bhp_utm.txt" + " -utm 51 H -colx 2 -coly 1 -colz 3 -colid 4";
		String command2 = "-ikml ExampleData/BHP/MT_Lines/ -oputm ExampleData/BHP/MT_Lines_Out -utm 51 H -ptl 500";
		try {
			exportLatLon();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			OpenSpatial.main((command.split(" ")));
			OpenSpatial.main((command2.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
