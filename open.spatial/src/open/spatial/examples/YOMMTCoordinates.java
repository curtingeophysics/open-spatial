package open.spatial.examples;

import open.spatial.OpenSpatial;

public class YOMMTCoordinates {
//

	public static void main(String[] args) {
		String filein = "ExampleData/YOM/MT_coordinates_unique.txt ";
		String fileout = "ExampleData/YOM/MT_coordinates_unique.kml ";
		String command = "-ill "+filein+" -okml " + fileout  + " -utm 52 J -colx 1 -coly 2";
		try {
			OpenSpatial.main((command.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
