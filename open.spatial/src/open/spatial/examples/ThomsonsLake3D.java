package open.spatial.examples;

import java.io.File;

import open.spatial.OpenSpatial;

public class ThomsonsLake3D {
//	public static String dirpath = "C:\\Users\\242151A\\Documents\\Thomsons_Lake_3D";
//	public static String dirpathout = "C:\\Users\\242151A\\Documents\\Thomsons_Lake_3D\\output\\";
//	public static String dirIn = "C:\\Users\\242151A\\Documents\\Thomsons_Lake_3D\\Processed\\";
//	
//	public static void main(String[] args) {
//		new File(dirpathout).mkdirs();
//		int [] spacings = {5};
//		for(File f : new File(dirpath).listFiles()) {
//			if(f.isFile()) {
//				for(int spacing : spacings ) {
//					
//					String command1 = "-ikml " + f.getAbsolutePath() + " " + " -outm " +dirpathout + "/" + f.getName() + "_" +spacing+"m.txt  " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln " + f.getName().substring(0,4);
//					
//			
//					try {
//						OpenSpatial.main((command1.split(" ")));
////						OpenSpatial.main((command2.split(" ")));
////						OpenSpatial.main((command3.split(" ")));
//					} catch(Exception e) {
//						e.printStackTrace();
//					}
//			}
//			}
//		}
//		
//	}
	
//	public static String dirpath = "C:\\Users\\242151A\\Documents\\Thomsons_Lake_3D";
	public static String dirpathout = "C:\\temp\\thomson\\RAW\\Out\\";
	public static String dirIn = "C:\\temp\\thomson\\RAW\\";
	
	public static void main(String[] args) {
		new File(dirpathout).mkdirs();
		int [] spacings = {5};
		for(File f : new File(dirIn).listFiles()) {
			if(f.isFile()) {
				for(int spacing : spacings ) {
					
//					String command1 = "-iutm " + f.getAbsolutePath() + " " + " -okml " +dirpathout + "/" + f.getName() +".kml " + "-utm 50 H -colx 2 -coly 3 -colz 4 -colid 1 -mapd C:\\temp\\thomson\\CLIP_0_0.asc";
					String command1 = "-iutm " + f.getAbsolutePath() + " " + " -okml " +dirpathout + "/" + f.getName() +".kml " + "-utm 50 H -colx 2 -coly 3 -colz 4 -colid 1";
					
			
					try {
						OpenSpatial.main((command1.split(" ")));
//						OpenSpatial.main((command2.split(" ")));
//						OpenSpatial.main((command3.split(" ")));
					} catch(Exception e) {
						e.printStackTrace();
					}
			}
			}
		}
		
	}
}
