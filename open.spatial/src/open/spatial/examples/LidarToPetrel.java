package open.spatial.examples;

import java.io.File;

import open.spatial.srtm.ARCGISASCII;

public class LidarToPetrel {
	public static void main(String[] args) throws Exception {
		ARCGISASCII.convertToPetrelPoints(new File("C:\\temp\\thomson\\CLIP_0_0.asc"), new File("C:\\temp\\thomson\\Thomson_Lake_LIDAR.ascii"), 381862.5, 394687.5, 6440414, 6444217, 1);
	}
}
