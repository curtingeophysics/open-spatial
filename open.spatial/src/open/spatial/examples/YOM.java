package open.spatial.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import open.spatial.Text;
import uk.me.jstott.jcoord.LatLng;

public interface YOM {
	public static void main(String[] args) {
		File fin = new File("ExampleData/YOM/MT_coordinates");
		File fout = new File("ExampleData/YOM/MT_coordinates_unique.txt");
		try {
			saveUnique(fin,fout);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void saveUnique(File fin, File fout) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(fin));
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
		out.write("Lat\tLon\tUTME\tUTMN\tOffset\tlng_zone\tlat_zone");
		out.newLine();
		String line = "";
		Set<Double> lats = new HashSet<Double>();
		while((line =  in.readLine())!= null) {
			ArrayList<Double> values = Text.split(line);
//			-26.180  126.425       0.000  454752.037       0.000
			double lat = values.get(0);
			double lon = values.get(1);
			double off = values.get(3);
			if(lats.contains(lat)) {

			} else {
				LatLng l = new LatLng(lat, lon);
				double e = l.toUTMRef().getEasting();
				double n = l.toUTMRef().getNorthing();

				String outline = lat + "\t" + lon + "\t" +e + "\t" + n + "\t" + off + "\t" + l.toUTMRef().getLngZone()+ "\t" + l.toUTMRef().getLatZone() ;
				out.write(outline);
				out.newLine();
				lats.add(lat);

			}
		}

		out.close();
		in.close();
	}
}
