package open.spatial.examples;

import open.spatial.OpenSpatial;

public class KMLToKMLPoints {
	public static void main(String[] args) {
		String dirpath = "ExampleData/Beeliar/";
		int [] spacings = {2,3,4,5};
		for(int spacing : spacings ) {
				String command1 = "-ikml " + dirpath + "/Beeliar_L01.kml " + " -okml " +dirpath + "/Out/Beeliar_L01_"+spacing+"m.kml  " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln 01";
				String command2 = "-ikml " + dirpath + "/Beeliar_L02.kml " + " -okml " +dirpath + "/Out/Beeliar_L02_"+spacing+"m.kml  " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln 02";
				String command3 = "-ikml " + dirpath + "/Beeliar_L03.kml " + " -okml " +dirpath + "/Out/Beeliar_L03_"+spacing+"m.kml  " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln 03";
		
				try {
					OpenSpatial.main((command1.split(" ")));
					OpenSpatial.main((command2.split(" ")));
					OpenSpatial.main((command3.split(" ")));
				} catch(Exception e) {
					e.printStackTrace();
				}
		}
	}
}
