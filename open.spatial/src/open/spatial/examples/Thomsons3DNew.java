package open.spatial.examples;

import java.io.File;

import open.spatial.OpenSpatial;

public class Thomsons3DNew {
	public static String dirIn= "C:\\Users\\242151A\\Documents\\Thomsons\\Mod\\";
	public static String dirOut = "C:\\Users\\242151A\\Documents\\Thomsons\\Mod\\Out\\";
	
	
	public static void main(String[] args) {
		
		int [] spacings = {5};
		for(File f : new File(dirIn).listFiles()) {
			if(f.isFile()) {
				for(int spacing : spacings ) {
					
					String command1 = "-ikml " + f.getAbsolutePath() + " " + " -outm " +dirOut + "/" + f.getName() + "_" +spacing+"m.txt  " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln " + f.getName().substring(0,4);
					String command2 = "-ikml " + f.getAbsolutePath() + " " + " -okml " +dirOut + "/" + f.getName() + "_" +spacing+"m.kml " +"-pti "+spacing+" " + "-utm 50 H -fnaming -ln " + f.getName().substring(0,4);
					
			
					try {
						OpenSpatial.main((command1.split(" ")));
						OpenSpatial.main((command2.split(" ")));
//						OpenSpatial.main((command3.split(" ")));
					} catch(Exception e) {
						e.printStackTrace();
					}
			}
			}
		}
		
	}
}
