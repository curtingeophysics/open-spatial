package open.spatial.examples;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;

import open.spatial.OpenSpatial;

public class Curtin2019 {
	public static void main(String[] args) {
		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties"));



		
		String filePathIn = "ExampleData/CurtinProject2019/GPS_Curtin_2019.kml";
		String filePathOut = "ExampleData/CurtinProject2019/GPS_Curtin_2019_10m.kml";
		String filePathOutASCII = "ExampleData/CurtinProject2019/GPS_Curtin_2019_10m.txt";
		
		String comm = "-ikml "+filePathIn+" -o " + filePathOutASCII + " -utm 51 J -pti 10 -fnaming -zsrtm";

		try {

			OpenSpatial.main((comm.split(" ")));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
