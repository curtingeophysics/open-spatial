package open.spatial.examples;

import java.io.BufferedWriter;
import java.io.File;

import org.apache.log4j.PropertyConfigurator;

import open.spatial.OpenSpatial;
import open.spatial.formats.PetrelASCII;
import open.spatial.srtm.SRTM;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class SRTMToPetrelPilbara {
	public static void main(String[] args) throws Exception{
		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties"));
		double minEasting = 270000;
		double maxEasting = 810000;
		double minNorthing = 7520000;
		double maxNorthing = 7860000;

		double dx = 100;
		double dy = 100;

		double nx = (int) ((maxEasting - minEasting)/dx);
		double ny = (int) ((maxNorthing- minNorthing)/dy);

		UTMRef rmin = new UTMRef(50,'J',minEasting, minNorthing);
		UTMRef rmax = new UTMRef(50,'J',maxEasting, maxNorthing);

		double minLon = Math.min(rmax.toLatLng().getLongitude(),rmin.toLatLng().getLongitude());
		double maxLon = Math.max(rmax.toLatLng().getLongitude(),rmin.toLatLng().getLongitude());
		double minLat = Math.min(rmax.toLatLng().getLatitude(),rmin.toLatLng().getLatitude());
		double maxLat = Math.max(rmax.toLatLng().getLatitude(),rmin.toLatLng().getLatitude());
		double dLon = (maxLon - minLon)/(double) nx;
		double dLat = (maxLat - minLat)/(double) ny;
		File fout = new File("C:\\temp\\thomson\\");
		BufferedWriter out = PetrelASCII.createWriter(fout);
		for(double lon = minLon ; lon < maxLon ; lon += dLon) {
			for(double lat = minLat ; lat < maxLat ; lat += dLat) {
				UTMRef u = new LatLng(lat, lon).toUTMRef();
				double x = u.getEasting();
				double y = u.getNorthing();
			
				try {
					double z = SRTM.getSRTMElevation(lat, lon);
					out.write(x + "\t" + y + "\t" + z);
					out.newLine();
				} catch(Exception e) {
					out.write(x + "\t" + y + "\t" + 0.0);
					out.newLine();
				}

			}
		}
		out.close();
	}
}

