package open.spatial.examples;

import open.spatial.OpenSpatial;

public class Project2020TEM {
//

	public static void main(String[] args) {
		String filein1 = "D:\\20_Work\\2020_Project\\623650_New.dat";
		String filein2 = "D:\\20_Work\\2020_Project\\6523800_New.dat";
		
		String fileout1 = "D:\\20_Work\\2020_Project\\623650_New.kml";
		String fileout2 = "D:\\20_Work\\2020_Project\\6523800_New.kml";
		
		String command1 = "-ill "+filein1 +" -okml " + fileout1  + " -utm 52 J -colx 97 -coly 90 -nhead 9 -colid 1";
		String command2 = "-ill "+filein2 +" -okml " + fileout2  + " -utm 52 J -colx 12 -coly 11 -nhead 13 -colid 1";
		try {
			OpenSpatial.main((command1.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
