package open.spatial.examples;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;

import open.spatial.OpenSpatial;

public class Ord  {
	public static void main(String[] args) {
		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties"));



		String seisDir = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/Seismic/";
		String eriDir = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/ERI/";
		String gprSingleDir = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/GPR/Single_Frequency/";
		String gprDualDir = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/GPR/Dual_Frequency";
		String outReprojectedPathSingle = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/GPR/Single_Frequency/Output/Reprojected/";
		String outReprojectedPathDual = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/GPR/Dual_Frequency/Output/Reprojected/";

		new File(outReprojectedPathSingle).mkdirs();
		new File(outReprojectedPathDual).mkdirs();

		String DEM = "W:/Geoscience_Australia_Ord_Bonaparte/Coordinates/DEM/ob_dem_asc.txt";
		String DEMMap = "-mapd " + DEM;

		String cseis01 = "-iutm "+seisDir+" -okml " + seisDir + "/Output/Waypoints/" + " -utm 52 L";
		String cseis02 = "-iutm "+seisDir+" -okmlt " + seisDir + "/Output/Tracks/" + " -utm 52 L";
		String cseis03 = "-iutm "+seisDir+" -oputm " + seisDir + "/Output/Petrel/" + " -utm 52 L";
		String cseis01map = "-iutm "+seisDir+" -outm " + seisDir + "/Output/Waypoints_Remap_DEM/" + " -utm 52 L " + DEMMap;
		String cseis02map = "-iutm "+seisDir+" -outm " + seisDir + "/Output/Tracks_Remap_DEM/" + " -utm 52 L " + DEMMap;


		String eri01 = "-ill "+eriDir+" -okmlt " + eriDir + "/Output/Tracks/" + " -utm 52 L";
		String eri02 = "-ill "+eriDir+" -okml " + eriDir + "/Output/Waypoints/" + " -utm 52 L";




		String gpr01 = "-icor "+gprSingleDir+" -okmlt " + gprSingleDir + "/Output/Tracks/" + " -utm 52 L ";
		String gpr02 = "-icor "+gprSingleDir+" -okml " + gprSingleDir + "/Output/Waypoints/" + " -utm 52 L";
		String gpr03 = "-icor "+gprDualDir+" -okmlt " + gprDualDir + "/Output/Tracks/" + " -utm 52 L";
		String gpr04 = "-icor "+gprDualDir+" -okml " + gprDualDir + "/Output/Waypoints/" + " -utm 52 L";

		String gpr05 = "-icor "+gprSingleDir+" -ocor " + gprSingleDir + "/Output/Reprojected/" + " -utm 52 L " + DEMMap;
		String gpr06 = "-icor "+gprDualDir+" -ocor " + gprDualDir + "/Output/Reprojected/" + " -utm 52 L "+ DEMMap;

		try {
//			OpenSpatial.main((cseis03.split(" ")));
//			OpenSpatial.main((cseis01map.split(" ")));
//			OpenSpatial.main((cseis02map.split(" ")));
//			OpenSpatial.main((cseis01.split(" ")));
//			OpenSpatial.main((cseis02.split(" ")));
//			OpenSpatial.main((eri01.split(" ")));
//			OpenSpatial.main((eri02.split(" ")));
//			OpenSpatial.main((gpr01.split(" ")));
//			OpenSpatial.main((gpr02.split(" ")));
//			OpenSpatial.main((gpr03.split(" ")));
//			OpenSpatial.main((gpr04.split(" ")));
			OpenSpatial.main((gpr05.split(" ")));
			OpenSpatial.main((gpr06.split(" ")));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



}
