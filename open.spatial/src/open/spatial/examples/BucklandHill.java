package open.spatial.examples;

import open.spatial.OpenSpatial;

public class BucklandHill {
	public static void main(String[] args) {
		String dirpath = "ExampleData/Buckland_Hill/";
//		String command = "-iutm "+dirpath+"BHCoords.txt -okml "+dirpath + "/BH.kml  -oputm " + dirpath + "/Output/BHCoords_out.txt" + " -utm 50 H -colx 2 -coly 3 -colz 4 -colid 1";
//
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}

//		String command = "-iutm "+dirpath+"EM31_Crossline -okml "+dirpath + "/EM31_Crossline.kml  -oputm " + dirpath + "/Output/EM31_Crossline_interp.txt" + " -utm 50 H -colx 2 -coly 3 -colz 4  -ptl 1.02 -fnaming -mapd " + dirpath + "/CLIP.asc";
//
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}

//		String command = "-ikml "+dirpath+"siestrack.kml -okml "+dirpath + "/Seis_Interp.kml  -oputm " + dirpath + "/Output/Seis_interp.txt" + " -utm 50 H -ptl 1.00 -fnaming -mapd " + dirpath + "/CLIP.asc";
//
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}

//		String command = "-iutm "+dirpath+"EM31GridIL0.txt -okml "+dirpath + "/EM31GridIL0.kml  -oputm " + dirpath + "/Output/EM31GridIL0_interp.txt" + " -utm 50 H -colx 1 -coly 2 -colz 3  -pti 0.53 -fnaming -mapd " + dirpath + "/CLIP.asc";
//
//
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}

//		String command = "-ikml "+dirpath+"EM31_GridCL.kml -okml "+dirpath + "/EM31_GridCL_interp.kml  -oputm " + dirpath + "/Output/EM31_GridCL_interp.txt" + " -utm 50 H -ptl 1.02 -fnaming -mapd " + dirpath + "/CLIP.asc";
//
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}

//		String command = "-iutm "+dirpath+"Buckland_Hill_EM31_All_data.txt -okml "+dirpath + "/Buckland_Hill_EM31_All_data.kml -utm 50 H -colx 4 -coly 5 -colz 6 -colid 1";
//		try {
//			OpenSpatial.main((command.split(" ")));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
		String command = "-iutm "+dirpath+"Tunnels/TunnelPtPicks -okml "+dirpath + "/TunnelPtPicks.kml -outm " + dirpath + "/TunnelPtPicks_Interp.txt -utm 50 H -ptl 0.2 -colx 3 -coly 4 -colz 5 -colid 2 -mapd " + dirpath + "/CLIP.asc";
		try {
			OpenSpatial.main((command.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
