package open.spatial.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class DavidsonTops {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(new File("Wells")));
		String [] header = in.readLine().split("\t");
		
		int n = 0;
		for(String s : header) {
			System.out.println(n++ + ": " + s);
		}
		
		int tq = 5;
		int activeBase = 22;
		String line = "";
		while((line = in.readLine()) != null) {
			String [] split = line.split("\t");
			if(!split[activeBase].equals("-")) {
//				System.out.println(line);
				for(int i = activeBase - 1 ; i >= tq ; i--) {
					if(!split[i].equals("-")) {
						if(!split[0].equals("-")) {
							double gl = Double.valueOf(split[0]);
							double e =  Double.valueOf(split[i]);
							System.out.println(split[1] + "\t" + split[2] + "\t" + (gl - e) + "\t" + split[4] );
							break;
						}
					}
				}
			}
		}
				
		
		System.out.println();
		
		in.close();
	}
}
