package open.spatial.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import open.spatial.Text;

public class TEst {
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(new File("HarveyCO2.txt")));

		String line = in.readLine();

		LinkedHashMap<Double, LinkedHashMap<Double, ArrayList<ArrayList<Double>>>> vals = new LinkedHashMap<>();

		while((line = in.readLine())!=null) {
			ArrayList<Double> arr = Text.split(line);


			//5 & 6


			double day = arr.get(0);
			double quad = arr.get(1);
			double depth = arr.get(2);
			double val = arr.get(3);
			LinkedHashMap<Double, ArrayList<ArrayList<Double>>> quads = vals.get(quad);
			if(quads == null) {
				quads = new LinkedHashMap();
				vals.put(quad, quads);
			}
			ArrayList<ArrayList<Double>> quadDepth = quads.get(depth);
			if(quadDepth == null) {
				quadDepth = new ArrayList<ArrayList<Double>>();
				quads.put(depth, quadDepth);
			}
			quadDepth.add(arr);
//			vals.put(, value);
		}

		for(Double quad : vals.keySet()) {
//			System.out.println(quad);
			LinkedHashMap<Double, ArrayList<ArrayList<Double>>> depths = vals.get(quad);
			for(Double depth : depths.keySet()) {
				ArrayList<ArrayList<Double>> initialDays = new ArrayList<ArrayList<Double>>();
				for(ArrayList<Double> v : depths.get(depth)) {
					if(v.get(0) <= 6) {
						initialDays.add(v);
					}
				}
				for(ArrayList<Double> values : depths.get(depth)) {
					System.out.println(values.get(0) + "\t" + quad + "\t" + depth + "\t" + normalize(depths.get(depth),values.get(3)));
				}

//				System.out.println("..." + depth);
			}
		}

		in.close();
	}

	private static Double normalize(ArrayList<ArrayList<Double>> initialDays, Double v) {

		double sum = 0;
		for(ArrayList<Double> a : initialDays) {
			sum += (a.get(3));
		}
//		medi(initialDays.size() + " " + sum);
		double avg = sum/initialDays.size();

		return (v - avg)/avg;

	}
}
