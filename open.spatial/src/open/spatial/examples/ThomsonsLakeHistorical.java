package open.spatial.examples;

import java.io.File;
import java.util.ArrayList;

import open.spatial.OpenSpatial;
import open.spatial.Transect;
import open.spatial.formats.KML;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.UTMRef;

public class ThomsonsLakeHistorical {
	public static void main(String[] args) throws Exception {
		OpenSpatial.initLogger();
		int p72_52L = 683;
		int p72_54L = 352;
		int p81_1 = 359;
		int p81_2 = 144;
		int BR71AW = 1603;
		
		for(File f : new File("E:\\Geophysical\\East_Midlands_Historical\\").listFiles()) {
			if(f.getName().toUpperCase().endsWith(".KML")) {				
				ArrayList<Point> pts = KML.loadKMLPoints(f.getAbsolutePath(), false);
				System.out.println(f.getName());
				ArrayList<UTMRef> refs = new ArrayList<UTMRef>();
				for(Point p : pts) {
					refs.add(p.getUTM());
				}
				
				Transect t = new Transect(refs);
				t.init();
				ArrayList<Double> eastings = new ArrayList<Double>();
				ArrayList<Double> northings = new ArrayList<Double>();
				int N = 0;
				System.out.println(f.getName());
				
				if(f.getName().contains("P72-052L")) {			
					
					N=p72_52L;
				} else if(f.getName().contains("P72-054L")) {
					N=p72_54L;
				} else if(f.getName().contains("P81-1")) {
					N=p81_1;
				} else if(f.getName().contains("P81-2")) {
					N=p81_2;					
				} else if(f.getName().contains("Barragoon_Reconnaissance_SS_BR71_AW")) {
					N=BR71AW;
				}
				double dd = (t.offsets.get(t.offsets.size()-1) - t.offsets.get(0))/(N+1);
				for(int i = 0 ; i < N ; i++) {
					double offset = i*dd;
					Point p = t.interpolateOffset(offset, 'J', 50);
					System.out.println((i+1) + "\t" + p.getUTM().getEasting() + "\t" + p.getUTM().getNorthing());
				}
			}
		}		
	}
}
