package open.spatial.examples;

import org.apache.log4j.PropertyConfigurator;

import open.spatial.OpenSpatial;

public class CityBeach {
	public static void main(String[] args) {
		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties"));

		String inputDirPath = "ExampleData/City_Beach/";
		String outputDirPath = "ExampleData/City_Beach/Output";

		String DEMPath = "ExampleData/City_Beach/DEM/CityBeach_5m_LIDAR.asc";

		String DEMMap = "-mapd " + DEMPath;

		String command = "-icor "+inputDirPath+ " -ocor " + outputDirPath + " -utm 50 J " + DEMMap;
		System.out.println(command);
		try {//
			OpenSpatial.main((command.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
