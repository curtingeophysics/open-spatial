package open.spatial.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import open.spatial.Text;
import open.spatial.Transect;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class HistoricalSeismicConverter {
	public static void main(String[] args) throws Exception{
		new HistoricalSeismicConverter();
	}
	
	public HistoricalSeismicConverter() throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(new File("E:\\Geophysical\\Seismic_Surveys_WA\\Seismic Surveys 2D - WA.kml")));
		String line = "";
		boolean run = true;
		while(run) {
			
			line = in.readLine();
			if(line == null) break;
			if(line.contains("<Placemark id="))	{
				
				String id = line.split("=")[1].split(">")[0].replaceAll("\"", "");
//				System.out.println(id);
				while(!(line =in.readLine()).contains("SchemaData")) {
					
				}
				HistoricalInstance s = new HistoricalInstance();
				s.id = id;
				if(id.contains("S000367")) {
					while(!(line =in.readLine()).contains("SchemaData")) {
	//				System.out.println(line);	
					
					if(line.contains("FIRST_CDP")) s.first_cdp = getInt(line);
					if(line.contains("FIRST_SHOTPOINT")) s.first_shotpoint = getDouble(line);
					if(line.contains("LAST_CDP")) s.last_cdp = getInt(line);
					if(line.contains("LAST_SHOTPOINT")) s.last_shotpoint = getDouble(line);
					if(line.contains("ACQUISITION_ID")) s.acquisition_id = getString(line);
					if(line.contains("LINE_NAME")) s.line_name = getString(line);
					if(line.contains("SURVEY_NAME")) s.survey_name = getString(line);
					if(line.contains("SURVEY_ID")) s.survey_id = getString(line);
					if(line.contains("LINE_LENGTH")) s.line_length = getDouble(line);
					if(line.contains("SHOT_INTERVAL")) s.shot_interval = getDouble(line);
					
	//				FIRST_CDP
	//				FIRST_SHOTPOINT
	//				LAST_CDP
	//				LAST_SHOTPOINT
	//				ACQUISITION_ID
	//				LINE_NAME
	//				SURVEY_NAME
	//				SURVEY_ID
	//				LINE_LENGTH
	//				SHOT_INTERVAL
	//				EXTRACT_DATE
					}
					
					while(!(line =in.readLine()).contains("coordinates")) {
						
					}
					while(!(line =in.readLine()).contains("coordinates")) {
						ArrayList<Double> coords = Text.split(line);
						ArrayList<LatLng> latlons = new ArrayList<LatLng>();
						while(!coords.isEmpty()) {
							double lon = coords.remove(0);
							double lat = coords.remove(0);
							LatLng l = new LatLng(lat, lon);
							latlons.add(l);
							coords.remove(0); //z
						}
						if(latlons.size() == 2) {
							LatLng l1 = latlons.get(0);							
							LatLng l3 = latlons.get(1);
							LatLng l2 = new LatLng((l1.getLatitude() + l3.getLatitude())/2, (l1.getLongitude() + l3.getLongitude())/2);
							latlons.add(1, l2);
						}
						ArrayList<UTMRef> refs = new ArrayList<UTMRef>();
						for(LatLng p : latlons) {
							refs.add(p.toUTMRef());
						}
						
						Transect t = new Transect(refs);
						t.init();
						ArrayList<Double> eastings = new ArrayList<Double>();
						ArrayList<Double> northings = new ArrayList<Double>();
						int N = 0;
//						if(s.line_name.contains("BR71-AS")) {
//							N = 1100;
//						}
//						if(s.line_name.contains("BR71-AT")) {
//							N = 911;
//						} 
//						if(s.line_name.contains("BR71-AU")) {
//							N = 710;
//						}
//						if(s.line_name.contains("BR71-AV")) {
//							N = 478;
//						}
//						if(s.line_name.contains("BR71-AW")) {
//							N = 1603;
//						}
						if(s.line_name.contains("BR71-AX")) {
							N = 949;
						}
						double dd = (t.offsets.get(t.offsets.size()-1) - t.offsets.get(0))/(N+1);
						s.print();
						for(int i = 0 ; i < N ; i++) {
							double offset = i*dd;
							Point p = t.interpolateOffset(offset, 'J', 50);
							System.out.println((i+1) + "\t" + p.getUTM().getEasting() + "\t" + p.getUTM().getNorthing());
						}
					}
			
				}
			}
//			System.out.println(line);
			
		}
		in.close();
	}
	private static String getString(String line) {
		return (line.split(">")[1].split("<")[0]);
	}
	private static double getDouble(String line) {
		return Double.valueOf((line.split(">")[1].split("<")[0]));
	}
	private static int getInt(String line) {
	
		return Integer.valueOf((line.split(">")[1].split("<")[0]));
	}

	private class HistoricalInstance {
		public String id = "";
		
		public int first_cdp = -1;
		public double first_shotpoint = -1;
		public int last_cdp = -1;
		public double last_shotpoint = -1;
		public String acquisition_id = "";
		public String line_name = "";
		public String survey_name = "";
		public String survey_id = "";
		public double line_length = 0;
		public double shot_interval = 0;
		
		public void print() {
			System.out.print("\tID\t" + id);
			System.out.print("\tFIRST_CDP\t" + first_cdp);
			System.out.print("\tFIRST_SHOTPOINT\t" + first_shotpoint);
			System.out.print("\tLAST_CDP\t" + last_cdp);
			System.out.print("\tLAST_SHOTPOINT\t" + last_shotpoint);
			System.out.print("\tACQUISITION_I\t" + acquisition_id);
			System.out.print("\tLINE_NAME\t" + line_name);
			System.out.print("\tSURVEY_NAME\t" + survey_name);
			System.out.print("\tSURVEY_ID\t" + survey_id);
			System.out.print("\tLINE_LENGTH\t" + line_length);
			System.out.println("\tSHOT_INTERVAL\t" + shot_interval);
		}
		
//		FIRST_CDP
//		FIRST_SHOTPOINT
//		LAST_CDP
//		LAST_SHOTPOINT
//		ACQUISITION_ID
//		LINE_NAME
//		SURVEY_NAME
//		SURVEY_ID
//		LINE_LENGTH
//		SHOT_INTERVAL
//		EXTRACT_DATE
		public HistoricalInstance() {
			
		}
	}
}
