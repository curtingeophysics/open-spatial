package open.spatial.examples;

import java.io.BufferedWriter;
import java.io.File;

import org.apache.log4j.PropertyConfigurator;

import open.spatial.OpenSpatial;
import open.spatial.formats.PetrelASCII;
import open.spatial.srtm.SRTM;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class SRTMToPetrelStIves {
	public static void main(String[] args) throws Exception{
		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties"));
		double minEasting = 364732.39 ;
		double maxEasting = 400141.00;
		double minNorthing = 6524187.65;
		double maxNorthing = 6546732.68;

		double dx = 100;
		double dy = 100;

		double nx = (int) ((maxEasting - minEasting)/dx);
		double ny = (int) ((maxNorthing- minNorthing)/dy);

		UTMRef rmin = new UTMRef(51,'J',minEasting, minNorthing);
		UTMRef rmax = new UTMRef(51,'J',maxEasting, maxNorthing);

		double minLon = Math.min(rmax.toLatLng().getLongitude(),rmin.toLatLng().getLongitude());
		double maxLon = Math.max(rmax.toLatLng().getLongitude(),rmin.toLatLng().getLongitude());
		double minLat = Math.min(rmax.toLatLng().getLatitude(),rmin.toLatLng().getLatitude());
		double maxLat = Math.max(rmax.toLatLng().getLatitude(),rmin.toLatLng().getLatitude());
		double dLon = (maxLon - minLon)/(double) nx;
		double dLat = (maxLat - minLat)/(double) ny;
		File fout = new File("D:\\2021_St_Ives\\Data\\SRTM\\St_Ives_SRTM.ascii");
		BufferedWriter out = PetrelASCII.createWriter(fout);
		for(double lon = minLon ; lon < maxLon ; lon += dLon) {
			for(double lat = minLat ; lat < maxLat ; lat += dLat) {
				UTMRef u = new LatLng(lat, lon).toUTMRef();
				double x = u.getEasting();
				double y = u.getNorthing();

				try {
					double z = SRTM.getSRTMElevation(lat, lon);
					out.write(x + "\t" + y + "\t" + z);
					out.newLine();
				} catch(Exception e) {
					out.write(x + "\t" + y + "\t" + 0.0);
					out.newLine();
				}

			}
		}
		out.close();
	}
}

