package open.spatial.examples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import open.spatial.OpenSpatial;
import open.spatial.Text;
import uk.me.jstott.jcoord.LatLng;

public interface Loupe {
	public static void main(String[] args) {
		File fin = new File("ExampleData/Loupe/emloupe_shoemaker_dtm.txt");
		File fout = new File("ExampleData/Loupe/emloupe_shoemaker_dtm_out.txt");
		String command = "-iutm "+fin+" -o " + fout + " -utm 51 J -colx 2 -coly 3 -colz 1 -zsrtm";
		try {
			OpenSpatial.main((command.split(" ")));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}