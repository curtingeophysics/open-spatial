package open.spatial.examples;

import open.spatial.OpenSpatial;

public class Lake {
	public static void main(String[] args) {
		String command1 = "-ikml f:/GEOP3002_ERI/ERI_Transect.kml -outm f:/GEOP3002_ERI/eritransectpts.txt " + "-utm 50 H -pti 5 -fnaming -mapd f:/GEOP3002_ERI/CLIP_0_0.asc";
		OpenSpatial.main((command1.split(" ")));
	}
}

