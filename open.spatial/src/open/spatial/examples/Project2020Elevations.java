package open.spatial.examples;

import java.io.File;

import open.spatial.srtm.ARCGISASCII;

public class Project2020Elevations {
	public static void main(String[] args) throws Exception {
		
		double x1 = 403000;
		double y1 = 6504000;
		
		
		double x2 = 465000;
		double y2 = 6551000;
		
		String dirpath = "D:\\20_Work\\2020_Project\\Data\\Topography\\SRTM\\";
		
		ARCGISASCII.convertToPetrelPoints(new File(dirpath  + "CLIP.asc"), new File(dirpath + "SRTM.ascii"), x1, x2, y1, y2, 1);
	}
}
