package open.spatial.examples;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.OpenSpatial;
import open.spatial.formats.CORGPR;
import open.spatial.formats.KML;
import open.spatial.points.Point;
import open.spatial.process.smoothing.ProcessSmooth1DElevationsFilter;
import open.spatial.srtm.ARCGISASCII;

public class WoodmanPointCockburn {
	static final Logger logger = LogManager.getLogger(WoodmanPointCockburn.class.getName());

	
	public static void main(String[] args) {
		OpenSpatial.initLogger();
		File esriFile = new File("D:\\20_Work\\2020_GPR_Woodman_Pt\\CLIP_0_0.asc");
		File dircoor = new File("D:\\20_Work\\2020_GPR_Woodman_Pt\\COR");
		File dirout = new File("D:\\20_Work\\2020_GPR_Woodman_Pt\\");
		ARCGISASCII argis = null;
		
		try {
			argis = ARCGISASCII.read(esriFile);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		for(File f : dircoor.listFiles()) {
			if(f.isFile()) {
			logger.info("Parsing file: " + f.getAbsolutePath());
			ArrayList<Point> pts = CORGPR.read(f.getAbsolutePath(), false);	
			try {
				KML.exportWaypoints(pts, new File(dirout.getAbsolutePath() + "\\" + f.getName() + ".kml"), false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ArrayList<Point> prjpts = argis.mapElevations(pts,true); 
			ArrayList<Point> smoothPoints = ProcessSmooth1DElevationsFilter.smooth1D(prjpts, 3, 1);
			
//			KML.writeTrackStrings(, pts, "Track " + f.getName(), false);
			CORGPR.write(smoothPoints, (dirout.getAbsolutePath() + "\\" + f.getName()));
			CORGPR.write(smoothPoints, (dirout.getAbsolutePath() + "\\" + f.getName().replaceAll("_1.cor", "_2.cor")));
			}
		} 
		

		
	}
	
}
