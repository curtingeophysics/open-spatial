package open.spatial.process;

import org.apache.commons.cli.Option;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import open.spatial.text.DataTable;

public abstract class OpenSpatialProcess{


	//identifiers
	public final static String UNKNOWN = "unknown";


	public final static String INTERPOLATE_NEAREST = "itn";
	public final static String INTERPOLATE_LINEAR = "itl";
	public final static String INTERPOLATE_SPLINE = "its";
	public final static String INTERPOLATE_DIFFERENCE = "itd";
	public final static String INTERPOLATE_NEVILLE = "itv";

	public final static String CLIPPING_SKIP_N = "cskpn";
	public final static String CLIPPING_VALUE = "cval";

	public final static String SMOOTH_ELEVATIONS_1D = "sz1d";


	public static final String MAPPING_ARCGIS = "mza";
	public static final String MAPPING_SRTM3 = "mzs3";



	public static final String EXPORT_KML = "okml";
//	public static final String EXPORT_KML = "okml";


	public abstract boolean isExport();
	public abstract String getID();
	public abstract String getName();

	public IntegerProperty order = new SimpleIntegerProperty(-1);
	public BooleanProperty enabled = new SimpleBooleanProperty(true);
	public BooleanProperty openOnComplete = new SimpleBooleanProperty(false);
	public ObjectProperty<Option> activeOption = new SimpleObjectProperty<Option>();

	public abstract int nParameters();
	public abstract Node createNode();


//	public abstract Option getOption();
	public abstract DataTable run(DataTable t) throws Exception;
	public abstract int createControlReturnRow(GridPane gp, int row);

	public void bindDouble(DoubleProperty p, TextField f) {
		StringConverter<? extends Number> converter = new DoubleStringConverter();
		Bindings.bindBidirectional(f.textProperty(), p, (StringConverter<Number>) converter);
	}
	public void bindInteger(IntegerProperty p, TextField f) {
		StringConverter<? extends Number> converter = new IntegerStringConverter();
		Bindings.bindBidirectional(f.textProperty(), p, (StringConverter<Number>) converter);
	}
	public abstract String getFunctionName();

}
