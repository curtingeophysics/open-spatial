package open.spatial.process.interpolate;

import java.util.ArrayList;

import javafx.scene.Node;
import javafx.scene.control.Button;
import open.spatial.process.OpenSpatialProcessSet;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;

public class ProcessesInterpolation extends OpenSpatialProcessSet{

	
	@Override
	public String getName() {
		return "Interpolation";
	}

	@Override
	public ArrayList<OpenSpatialProcess> getProcesses() {
		ArrayList<OpenSpatialProcess> list = new ArrayList<OpenSpatialProcess>();
		list.add(new Interpolate(Interpolate.MODE_LINEAR));
		list.add(new Interpolate(Interpolate.MODE_NEAREST));
		list.add(new Interpolate(Interpolate.MODE_NEVILLE));
		list.add(new Interpolate(Interpolate.MODE_SPLINE));
		return list;
	}
	
	

	@Override
	public Node getIcon(float fontsize) {
		return FontAwesome.ELLIPSIS_H(fontsize);
	}

	@Override
	protected OpenSpatialProcess create(String id) {
		switch (id) {
			case OpenSpatialProcess.INTERPOLATE_LINEAR: return (new Interpolate(Interpolate.MODE_LINEAR));
			case OpenSpatialProcess.INTERPOLATE_NEAREST: return (new Interpolate(Interpolate.MODE_NEAREST));
			case OpenSpatialProcess.INTERPOLATE_NEVILLE: return (new Interpolate(Interpolate.MODE_NEVILLE));
			case OpenSpatialProcess.INTERPOLATE_SPLINE: return (new Interpolate(Interpolate.MODE_SPLINE));		
			default: return null;
		}
	}
	
	

}
