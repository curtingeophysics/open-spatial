package open.spatial.process.interpolate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import org.apache.commons.cli.Option;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.DividedDifferenceInterpolator;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.interpolation.LoessInterpolator;
import org.apache.commons.math3.analysis.interpolation.NevilleInterpolator;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.UTMRef;

public class Interpolate extends OpenSpatialProcess{
	
	public DoubleProperty interpolationDistance = new SimpleDoubleProperty(10);
	
	public final static int UNIT_METRES = 1;
	public final static int UNIT_DEGREES = 2;
	
	public final static int MODE_NEAREST = 0;
	public final static int MODE_LINEAR = 1;
	public final static int MODE_SPLINE = 2;
	public final static int MODE_DIFFERENCE = 3;
	public final static int MODE_NEVILLE = 4;
	
	public IntegerProperty unitSelection = new SimpleIntegerProperty(UNIT_METRES);
	public IntegerProperty modeSelection = new SimpleIntegerProperty(MODE_LINEAR);
	public DoubleProperty increment = new SimpleDoubleProperty(100); 
	
	
	static final Logger logger = LogManager.getLogger(Interpolate.class.getName());

	
	@Override
	public String getID() {
		switch (modeSelection.get()) {
			case MODE_NEAREST: return INTERPOLATE_NEAREST;
			case MODE_LINEAR: return INTERPOLATE_LINEAR;
			case MODE_SPLINE: return INTERPOLATE_SPLINE;
			case MODE_DIFFERENCE: return INTERPOLATE_DIFFERENCE;
			case MODE_NEVILLE: return INTERPOLATE_NEVILLE;				
			default: return UNKNOWN;
		}
	}
	
	

	public Interpolate (int initialMode) {
		this.modeSelection.set(initialMode);
	}

	
	@Override
	public DataTable run(DataTable t) {
		
		try {
			double inc = Double.valueOf(increment.get());
			
			int n = t.getPoints().size();
			double[] xs = new double[n];
			double[] ys = new double[n];
			double[] zs = new double[n];
			double[] os = new double[n];
			
			int i = 0;
			double offset = 0;
			
			ArrayList<Point> pta = t.getPoints();
			int lngZone = pta.get(0).getUTM().getLngZone();	
			char latZone =  pta.get(0).getUTM().getLatZone();
			
			HashMap<String, ArrayList<Point>> lines = t.getLinesPointsHash();
			ArrayList<Point> outputPts  = new ArrayList<Point>();
			for(String lineID : lines.keySet()) {
				ArrayList<Point> line = lines.get(lineID);
				for(Point p : line) {
					UTMRef r = p.getUTM();
					if(r.getLngZone() != lngZone) {
						logger.warn("UTM Zone change from zone " + lngZone + " to " + r.getLngZone());
					}
					xs[i] = r.getEasting();
					ys[i] = r.getNorthing();
					zs[i] = p.z;
					os[i] = new Double(offset);
					//calc next point
					boolean isLastPoint = i == (line.size() -1);
					if(!isLastPoint) {
						UTMRef r2 = line.get(i+1).getUTM();
						double dx = (xs[i] - r2.getEasting());
						double dy = (ys[i] - r2.getNorthing());
						double doffset = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
						offset += doffset;
					}
					i++;
					
				}
				
				 
				 UnivariateInterpolator interp = new SplineInterpolator();
				 UnivariateInterpolator linterp = new LinearInterpolator();
				 UnivariateInterpolator diff = new DividedDifferenceInterpolator();
				 UnivariateInterpolator neville = new NevilleInterpolator();
				 
				 //TODO this should be a separate instance for just XY position smoothing points. 
				 LoessInterpolator loess = new LoessInterpolator(); 
				 
	
				 
				 
				 ArrayList<Double> offsets = new ArrayList<Double>();
				 for(double d = 0 ; d<= os[os.length-1] ; d += inc) {
					 offsets.add(d);
				 }
				 
				 UnivariateFunction Xi = linterp.interpolate(os, xs);
				 UnivariateFunction Yi = linterp.interpolate(os, ys);
				 UnivariateFunction Zi = linterp.interpolate(os, ys);
				if(modeSelection.get() == MODE_SPLINE) {
					 Xi = interp.interpolate(os, xs);
					 Yi = interp.interpolate(os, ys);
					 Zi = interp.interpolate(os, zs);
				} else if(modeSelection.get() == MODE_DIFFERENCE) {
						 Xi = diff.interpolate(os, xs);
						 Yi = diff.interpolate(os, ys);
						 Zi = diff.interpolate(os, zs);
				} else if(modeSelection.get() == MODE_NEVILLE) {
					 Xi = neville.interpolate(os, xs);
					 Yi = neville.interpolate(os, ys);
					 Zi = neville.interpolate(os, zs);
				}
					 
				 
				 
				 if(modeSelection.get() == MODE_NEAREST){
					 ArrayList l = new ArrayList(Arrays.asList(os));
					 for(double d : offsets) {					 
						int nearestOffset = Collections.binarySearch(l,d);
						nearestOffset = nearestOffset >= 0 ? nearestOffset : Math.max(0,-nearestOffset-2);
						 //Linear interpolate x + y locations, but nearest neighbour the z-component
						 double x = Xi.value(d);
						 double y = Yi.value(d);
						 double z = zs[nearestOffset];
						 UTMRef ref = new UTMRef(lngZone,latZone,x,y);
						 Point p = new Point(ref,z);
						 p.line = lineID;
						 outputPts.add(p);					
					 }
				 } else {			 
					 for(double d : offsets) {
						 double x = Xi.value(d);
						 double y = Yi.value(d);
						 double z = Zi.value(d);
						 UTMRef ref = new UTMRef(lngZone,latZone,x,y);
						 Point p = new Point(ref,z);						 
						 p.line = lineID;
						 outputPts.add(p);
					 }
				 }
			}
			 DataTable dt = t.clone();
			 dt.setPoints(outputPts);
			 return dt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
		
	}

	@Override
	public int createControlReturnRow(GridPane gp, int row) {
		TextField interpolationDistance = new TextField(this.interpolationDistance.get() + "");
		ComboBox<String> units = new ComboBox<String>();
		units.getItems().addAll("metres (UTM Domain)");
		ComboBox<String> method = new ComboBox<String>();
		method.getItems().addAll("Nearest-Neighbour Z","Linear","Cubic-Spline","Divided-Difference","Neville's Algorithm");
		
		interpolationDistance.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					Interpolate.this.interpolationDistance.set(Double.valueOf(newValue));
				} catch(Exception e) {
					logger.error(e);
				}
			}
		});
		
		units.selectionModelProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				Interpolate.this.unitSelection.set(units.getSelectionModel().getSelectedIndex() + 1);
			}
		});
		method.selectionModelProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				Interpolate.this.modeSelection.set(method.getSelectionModel().getSelectedIndex() + 1);
			}
		});
		gp.add(interpolationDistance, 0, row);
		gp.add(units, 1, row);
		gp.add(new Label("Method"), 2, row);
		gp.add(method, 3, row);
		return row+1;
	}

	@Override
	public String getName() {
		switch (this.modeSelection.get()) {
		case MODE_NEAREST: return "Nearest-Neighbour";	
		case MODE_LINEAR: return "Linear";	
		case MODE_DIFFERENCE: return "Difference";	
		case MODE_NEVILLE: return "Neville";	
		case MODE_SPLINE: return "Spline";	

		default:
			break;
		}
		return "Unknown";
	}


	@Override
	public int nParameters() {
		return 3;
	}


	@Override
	public Node createNode() {
		TextField interpolationDistance = new TextField(this.interpolationDistance.get() + "");
		
		GridPane gp = new GridPane();
		
		
		ComboBox<String> units = new ComboBox<String>();
		units.getItems().addAll("metres (UTM Domain)");
		units.getItems().addAll("degrees (WGS84 Domain)");
		
		
		
		ComboBox<String> method = new ComboBox<String>();
		method.getItems().addAll("Nearest-Neighbour Z","Linear","Cubic-Spline","Divided-Difference","Neville's Algorithm");
		gp.add(new Label("Mode"), 0, 0);
		gp.add(method, 1, 0);
		
		gp.add(new Label("Distance"), 0, 1);
		gp.add(interpolationDistance, 1, 1);
		
		
		gp.add(new Label("Units"), 0, 2);
		gp.add(units, 1, 2);
		
		interpolationDistance.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					Interpolate.this.interpolationDistance.set(Double.valueOf(newValue));
				} catch(Exception e) {
					logger.error(e);
				}
			}
		});
		
		units.selectionModelProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				Interpolate.this.unitSelection.set(units.getSelectionModel().getSelectedIndex() + 1);
			}
		});
		method.selectionModelProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				Interpolate.this.modeSelection.set(method.getSelectionModel().getSelectedIndex() + 1);
			}
		});
		
		return gp;
	}

	


	@Override
	public boolean isExport() {
		return false;
	}



	@Override
	public String getFunctionName() {
		return "Interpolate Elevation";
	}




}
