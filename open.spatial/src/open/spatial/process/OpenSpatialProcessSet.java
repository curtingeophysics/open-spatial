package open.spatial.process;

import java.util.ArrayList;

import javafx.scene.Node;

public abstract class OpenSpatialProcessSet {

	public abstract String getName();
	public abstract ArrayList<OpenSpatialProcess> getProcesses();
	public abstract Node getIcon(float size);
	protected abstract OpenSpatialProcess create(String id);
	
}
