package open.spatial.process.smoothing;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;

public class ProcessSmooth1DElevationsFilter extends OpenSpatialProcess {

	public IntegerProperty filterWidth = new SimpleIntegerProperty(5);
	public IntegerProperty iterations = new SimpleIntegerProperty(2);
	


	@Override
	public String getName() {
		return "Smooth 1D Elevations Filter";
	}

	@Override
	public DataTable run(DataTable t) throws Exception {
		ArrayList<Point> smoothed = new ArrayList<Point>();
		for(ArrayList<Point> line : t.getLinesPointsHash().values()) {
			smoothed.addAll(smooth1D(line, filterWidth.get(), iterations.get()));
		}
		DataTable dt = t.clone();
		dt.setPoints(smoothed);
		return dt;
	}

	public static ArrayList<Point> smooth1D(ArrayList<Point> pts, int filtwidth, int iterations) {
		ArrayList<Point> out = new ArrayList<Point>();
		
		int di = (int) ((double) filtwidth / (double) 2);
		
		for(int i = 0 ; i < pts.size() ; i++) {
			int mini = Math.max(0,i - di);
			int maxi = Math.min(pts.size()-1,i + di);
			Point po = pts.get(i).clone();
			double sum = 0;
			int count = 0;
			for(int j = mini ; j <= maxi ; j++) {
				count++;
				sum += pts.get(j).z;
			}
			po.z = sum / (double) count;
			out.add(po);
		}
		ArrayList<Point> outit = iterations >= 1 ? smooth1D(out, filtwidth, iterations-1) : out;		
		return outit;
	}
	
	
	@Override
	public int createControlReturnRow(GridPane gp, int row) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getID() {
		return SMOOTH_ELEVATIONS_1D;
	}

	@Override
	public int nParameters() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Node createNode() {
		Label l = new Label(getName());
		TextField filter = new TextField(this.filterWidth.get() + "");
		TextField iterations = new TextField(this.iterations.get() + "");
		
		bindInteger(this.filterWidth, filter);
		bindInteger(this.iterations, iterations);
					
		HBox b = new HBox(l,new Label("Filter Width (N Samples"),filter,new Label("Smoothing Iterations"),iterations);
		return b;
	}

	@Override
	public boolean isExport() {
		return false;
	}

	@Override
	public String getFunctionName() {
		return "Smooth Elevations";
	}

}
