package open.spatial.process;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.formats.ProcessesExporting;
import open.spatial.process.clipping.ProcessesClipping;
import open.spatial.process.interpolate.ProcessesInterpolation;
import open.spatial.process.mapping.ProcessesMapping;
import open.spatial.process.ordering.ProcessesOrdering;

public class OpenSpatialProcesses {

	static final Logger logger = LogManager.getLogger(OpenSpatialProcesses.class.getName());

	public static ArrayList<OpenSpatialProcessSet> processSets = createProcessSets();

	private static ArrayList<OpenSpatialProcessSet> createProcessSets() {
		ArrayList<OpenSpatialProcessSet> sets = new ArrayList<OpenSpatialProcessSet>();
		sets.add( new ProcessesInterpolation());
		sets.add( new ProcessesClipping());
		sets.add( new ProcessesOrdering());
		sets.add( new ProcessesMapping());
		sets.add( new ProcessesExporting());
		return sets;
	}

	public static OpenSpatialProcess create(String id) {
		for(OpenSpatialProcessSet s : processSets) {
			for(OpenSpatialProcess p : s.getProcesses()) {
				if(p.getID().equals(id)) {
					return s.create(id);
				}
			}
		}
		logger.warn("Cannot find id: " + id);
		return null;
	}





}
