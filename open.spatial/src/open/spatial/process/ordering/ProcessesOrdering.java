package open.spatial.process.ordering;

import java.util.ArrayList;

import javafx.scene.Node;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.OpenSpatialProcessSet;

public class ProcessesOrdering extends OpenSpatialProcessSet{

	@Override
	public String getName() {
		return "Ordering";
	}

	@Override
	public ArrayList<OpenSpatialProcess> getProcesses() {
		ArrayList<OpenSpatialProcess> processes = new ArrayList<OpenSpatialProcess>();
		
		return processes;
	}

	@Override
	public Node getIcon(float size) {

		return FontAwesome.SORT_AMOUNT_DESC(size);

	}

	@Override
	protected OpenSpatialProcess create(String id) {
		return null;
	}
}
