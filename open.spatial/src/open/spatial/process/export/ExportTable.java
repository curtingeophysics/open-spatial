package open.spatial.process.export;

import java.util.ArrayList;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import open.spatial.formats.OpenSpatialExportFormats;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;

public class ExportTable extends OpenSpatialProcess{

	
	private int formatID;

	public ExportTable(int formatID) {
		this.formatID = formatID;
	}
	
	@Override
	public boolean isExport() {
		return true;
	}

	@Override
	public String getID() {
		return OpenSpatialExportFormats.getID(formatID);
	}

	@Override
	public String getName() {
		return OpenSpatialExportFormats.formats.get(formatID).getName();
	}

	@Override
	public int nParameters() {
		return OpenSpatialExportFormats.formats.get(formatID).getNParameters();
	}

//	@Override
//	public Node createNode() {
//		
//	}

	@Override
	public DataTable run(DataTable t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int createControlReturnRow(GridPane gp, int row) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node createNode() {
		// TODO Auto-generated method stub
		return null;
	}

	



}
