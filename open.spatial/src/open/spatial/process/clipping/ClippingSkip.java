package open.spatial.process.clipping;

import java.util.ArrayList;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;

public class ClippingSkip extends OpenSpatialProcess{

	


	public DoubleProperty skipNth = new SimpleDoubleProperty(1);

	
	
	

	@Override
	public DataTable run(DataTable t) throws Exception{
		Integer skipN = skipNth.intValue();
	
		
		ArrayList<Point> pts = t.getPoints();
		ArrayList<Point> outputpts = new ArrayList<Point>();
		ArrayList<ArrayList<String>> outputvalues = new ArrayList<ArrayList<String>>();
		
		t.clone();
		
		for(int i = 0 ; i < t.values.size() ; i+=skipN) {
			outputpts.add(pts.get(i));
			outputvalues.add(t.values.get(i));

			
		}
		
		DataTable dt = new DataTable(t.header, t.values);
		dt.setPoints(outputpts);
		
		return dt;
	}



	private int stripNumber(String name) {
		if(name == null) return 0;
		if(name.length() == 0) return 0;
		boolean removeLeadingChars = !Character.isDigit(name.charAt(0));
		while(removeLeadingChars) {
			if(name.length() == 1) return Integer.MAX_VALUE;
			name = name.substring(1);
			removeLeadingChars = !Character.isDigit(name.charAt(0));
		}
		boolean removeTrailingChars = !Character.isDigit(name.charAt(name.length()-1));
		while(removeLeadingChars) {
			if(name.length() == 1) return Integer.MAX_VALUE;
			name = name.substring(0,name.length()-2);
			removeTrailingChars = !Character.isDigit(name.charAt(name.length()-1));
		}
		if(name.length() != 0) return Integer.valueOf(name);
		
		return -1;
	}



	@Override
	public String getName() {
		return "Skip Nth";
	}


	public Node createNode() {
		Label l = new Label("Skip N Values");
		TextField f = new TextField(skipNth.get() + "");
		StringConverter<? extends Number> converter = new DoubleStringConverter();
		Bindings.bindBidirectional(f.textProperty(), skipNth, (StringConverter<Number>) converter);		
		HBox b = new HBox(l,f);
		return b;
		
	}

	@Override
	public int createControlReturnRow(GridPane gp, int row) {
		gp.add(new Label("Skip N Values"), 0, row);
		TextField f = new TextField(skipNth.get() + "");
		StringConverter<? extends Number> converter = new DoubleStringConverter();
		Bindings.bindBidirectional(f.textProperty(), skipNth, (StringConverter<Number>) converter);		
		gp.add(f, 1, row++);		
		return row;
	}





	@Override
	public String getID() {
		return CLIPPING_SKIP_N;
	}



	@Override
	public int nParameters() {
		return 1;
	}



	@Override
	public boolean isExport() {
		return false;
	}



	@Override
	public String getFunctionName() {
		return "Skip";
	}




}
