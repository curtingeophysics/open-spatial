package open.spatial.process.clipping;

import java.util.ArrayList;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.UTMRef;

public class ClippingXYZV extends OpenSpatialProcess{

	DoubleProperty minX = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty maxX = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty minY = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty maxY= new SimpleDoubleProperty(Double.NaN);
	DoubleProperty minZ = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty maxZ = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty minID = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty maxID = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty minLine = new SimpleDoubleProperty(Double.NaN);
	DoubleProperty maxLine = new SimpleDoubleProperty(Double.NaN);

	
	

	@Override
	public DataTable run(DataTable t) throws Exception{
		Double minX = this.minX.doubleValue();
		Double maxX = this.maxX.doubleValue();
		Double minY = this.minY.doubleValue();
		Double maxY = this.maxY.doubleValue();
		Double minZ = this.minZ.doubleValue();
		Double maxZ = this.maxZ.doubleValue();
		Double minID = this.minID.doubleValue();
		Double maxID = this.maxID.doubleValue();
		Double minLine = this.minLine.doubleValue();
		Double maxLine = this.maxLine.doubleValue();
		
		ArrayList<Point> pts = t.getPoints();
		ArrayList<Point> outputpts = new ArrayList<Point>();
		ArrayList<ArrayList<String>> outputvalues = new ArrayList<ArrayList<String>>();
		
		for(int i = 0 ; i < t.values.size() ; i++) {
			boolean include = true;
			Point p = pts.get(i);
			UTMRef r = p.getUTM();
			double x = r.getEasting();
			double y = r.getNorthing();
			double z = p.z;
			int id = stripNumber(p.name);
			int line = stripNumber(p.line);
			include = (Double.isNaN(minX) ? include : include && x >= minX);
			include = (Double.isNaN(maxX) ? include : include && x <= maxX);
			include = (Double.isNaN(minY) ? include : include && y >= minY);
			include = (Double.isNaN(maxY) ? include : include && y <= maxY);
			include = (Double.isNaN(minZ) ? include : include && z >= minZ);
			include = (Double.isNaN(maxZ) ? include : include && z <= maxZ);
			include = (Double.isNaN(minID) ? include : include && id >= minID);
			include = (Double.isNaN(maxID) ? include : include && id <= maxID);
			include = (Double.isNaN(minLine) ? include : include && line >= minLine);
			include = (Double.isNaN(maxLine) ? include : include && line <= maxLine);
			
			if(include) {
				outputpts.add(p);
				outputvalues.add(t.values.get(i));
			}
			
		}
		
		DataTable dt = new DataTable(t.header, t.values);
		dt.setPoints(outputpts);
		
		return dt;
	}



	private int stripNumber(String name) {
		if(name == null) return 0;
		if(name.length() == 0) return 0;
		boolean removeLeadingChars = !Character.isDigit(name.charAt(0));
		while(removeLeadingChars) {
			if(name.length() == 1) return Integer.MAX_VALUE;
			name = name.substring(1);
			removeLeadingChars = !Character.isDigit(name.charAt(0));
		}
		boolean removeTrailingChars = !Character.isDigit(name.charAt(name.length()-1));
		while(removeLeadingChars) {
			if(name.length() == 1) return Integer.MAX_VALUE;
			name = name.substring(0,name.length()-2);
			removeTrailingChars = !Character.isDigit(name.charAt(name.length()-1));
		}
		if(name.length() != 0) return Integer.valueOf(name);
		
		return -1;
	}



	@Override
	public String getName() {
		return "Clip by Value";
	}



	@Override
	public int createControlReturnRow(GridPane gp, int row) {
//		gp.add(new Label("Min UTM Easting"), 0, row);
//		gp.add(minX, 1, row++);
//		gp.add(new Label("Max UTM Easting"), 0, row);
//		gp.add(maxX, 1, row++);
//		gp.add(new Label("Min UTM Northing"), 0, row);
//		gp.add(minY, 1, row++);
//		gp.add(new Label("Max UTM Northing"), 0, row);
//		gp.add(maxY, 1, row++);
//		gp.add(new Label("Min Z"), 0, row);
//		gp.add(minZ, 1, row++);
//		gp.add(new Label("Max Z"), 0, row);
//		gp.add(maxZ, 1, row++);
//		gp.add(new Label("Min ID"), 0, row);
//		gp.add(minID, 1, row++);
//		gp.add(new Label("Max ID"), 0, row);
//		gp.add(maxID, 1, row++);
//		gp.add(new Label("Min Line"), 0, row);
//		gp.add(minLine, 1, row++);
//		gp.add(new Label("Max Line"), 0, row);
//		gp.add(maxLine, 1, row++);
		return row;
	}



	@Override
	public String getID() {
		return CLIPPING_VALUE;
	}



	@Override
	public int nParameters() {
		return 2;
	}



	@Override
	public Node createNode() {
		
		TextField minX = new TextField();
		TextField maxX = new TextField();
		TextField minY = new TextField();
		TextField maxY = new TextField();
		TextField minZ = new TextField();
		TextField maxZ = new TextField();
		TextField minID = new TextField();
		TextField maxID = new TextField();
		TextField minLine = new TextField();
		TextField maxLine = new TextField();
		
		bindDouble(this.minX, minX);
		bindDouble(this.minY, minY);
		bindDouble(this.minZ, minZ);
		bindDouble(this.minLine, minLine);
		bindDouble(this.minID, minID);
		bindDouble(this.maxX, maxX);
		bindDouble(this.maxY, maxY);
		bindDouble(this.maxZ, maxZ);
		bindDouble(this.maxLine, maxLine);
		bindDouble(this.maxID, maxID);
		
		GridPane p = new GridPane();
		
//		p.add(new Label(getName()), 0, 0,3,1);
		
//		p.add(new Separator(), 0, 1);
		p.add(new Label("Min"), 1, 1);
		p.add(new Label("Max"), 2, 1);
		
		p.add(new Label("X/Lon"), 0, 2);
		p.add(new Label("Y/Lat"), 0, 3);
		p.add(new Label("Z/Elev"), 0, 4);
		p.add(new Label("Station ID"), 0, 5);
		p.add(new Label("Line"), 0, 6);
		
		p.add(minX, 1, 2);
		p.add(minY, 1, 3);
		p.add(minZ, 1, 4);
		p.add(minID, 1, 5);
		p.add(minLine, 1, 6);
		
		p.add(maxX, 2, 2);
		p.add(maxY, 2, 3);
		p.add(maxZ, 2, 4);
		p.add(maxID, 2, 5);
		p.add(maxLine, 2, 6);
		return p;
	}


	@Override
	public boolean isExport() {
		return false;
	}



	@Override
	public String getFunctionName() {
		return "Clip";
	}



	

}
