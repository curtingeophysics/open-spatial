package open.spatial.process.clipping;

import java.util.ArrayList;

import javafx.scene.Node;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.OpenSpatialProcessSet;

public class ProcessesClipping extends OpenSpatialProcessSet {

	@Override
	public String getName() {
		return "Clipping";
	}

	@Override
	public ArrayList<OpenSpatialProcess> getProcesses() {
		ArrayList<OpenSpatialProcess> processes = new ArrayList<OpenSpatialProcess>();
		processes.add(new ClippingXYZV());
		processes.add(new ClippingSkip());
		return processes;
	}

	@Override
	public Node getIcon(float size) {
		return FontAwesome.CROP(size);
	}

	@Override
	protected OpenSpatialProcess create(String id) {
		switch (id) {
		case OpenSpatialProcess.CLIPPING_SKIP_N: return new ClippingSkip();
		case OpenSpatialProcess.CLIPPING_VALUE: return new ClippingXYZV();			
		default: return null;
		}
	}

}
