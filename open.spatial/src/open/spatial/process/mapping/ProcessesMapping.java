package open.spatial.process.mapping;

import java.util.ArrayList;

import javafx.scene.Node;
import open.spatial.fonts.FontAwesome;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.OpenSpatialProcessSet;

public class ProcessesMapping extends OpenSpatialProcessSet{



	@Override
	public String getName() {
		return "Mapping";
	}

	@Override
	public ArrayList<OpenSpatialProcess> getProcesses() {
		ArrayList<OpenSpatialProcess> processes = new ArrayList<OpenSpatialProcess>();
		processes.add(new MappingARCGISElevation());
		processes.add(new MappingSRTM3());
		return processes;
	}

	@Override
	public Node getIcon(float size) {

		return FontAwesome.GLOBE(size);
//		Button ordering = new Button("Ordering",FontAwesome.SORT_AMOUNT_DESC(fontsize));

	}

	@Override
	protected OpenSpatialProcess create(String id) {
		switch (id) {
			case OpenSpatialProcess.MAPPING_ARCGIS: return new MappingARCGISElevation();
			case OpenSpatialProcess.MAPPING_SRTM3: return new MappingSRTM3();				
			default: return null;
		}
	}
}
