package open.spatial.process.mapping;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import open.spatial.OpenSpatial;
import open.spatial.fonts.FontAwesome;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.interpolate.Interpolate;
import open.spatial.srtm.ARCGISASCII;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.UTMRef;

public class MappingARCGISElevation extends OpenSpatialProcess{


	public StringProperty fileSelection = new SimpleStringProperty();
	public BooleanProperty isSmooth = new SimpleBooleanProperty(false);

	static final Logger logger = LogManager.getLogger(MappingARCGISElevation.class.getName());

	@Override
	public String getName() {
		return "Map ARCGIS ASCII ELevations";
	}

	@Override
	public DataTable run(DataTable t) throws Exception {
		logger.info("Running Mapping ARCGIS Elevation Elevations");
		ArrayList<File> selectedFiles = new ArrayList<File>();
		for(String s : fileSelection.get().split(";")) {
			File f = new File(s);
			if(f.exists()) {
				selectedFiles.add(f);
			}
		}

		ArrayList<ARCGISASCII> maps = new ArrayList<ARCGISASCII>();
		for(File f : selectedFiles) {
			try {
				logger.info("Remapping Elevations");
				ARCGISASCII elevations = ARCGISASCII.read(f);
				maps.add(elevations);
			} catch(Exception e) {
				logger.error(e);
			}
		}

		boolean isSmoothed = isSmooth.get();
		ArrayList<Point> pts = t.getPoints();
		ArrayList<Point> ptsout = new ArrayList<Point>();
		for(Point p : pts) {
			Point pm = p.clone();
			UTMRef r = pm.getUTM();
			double e = r.getEasting();
			double n = r.getNorthing();

			for(ARCGISASCII m : maps) {
				if(m.within(e,n)) {
					pm.z = isSmoothed ? m.getClosestElevationSmooth(e, n) : m.getClosestElevation(e, n);
					break;
				}
			}

			ptsout.add(pm);
		}
		DataTable dtout = t.clone();
		dtout.setPoints(ptsout);
		return dtout;

	}

	@Override
	public int createControlReturnRow(GridPane gp, int row) {

		Label pathLabel = new Label("Path");
		TextField path = new TextField();
		path.textProperty().bindBidirectional(this.fileSelection);
		Button load = new Button("...", FontAwesome.FOLDER());
		load.setOnAction(getLoadAction());

		gp.add(pathLabel, 0, row);
		gp.add(path, 1, row);
		gp.add(load, 2, row);


		return row+1;
	}

	private EventHandler<ActionEvent> getLoadAction() {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fc = new FileChooser();
				String path = OpenSpatial.properties.get("file.arcgis.previous");
				if(path != null) {
					File dir = new File(path);
					if(dir.exists()) {
						fc.setInitialDirectory(dir);
					}
				}
				ArrayList<String> extensions = new ArrayList<String>();
				extensions.add(".asc");
				extensions.add(".ascii");
				extensions.add(".txt");


				ExtensionFilter filt = new ExtensionFilter("ESRI ASCII Raster", extensions);
				fc.getExtensionFilters().add(filt);


				try {
					Iterator<File> files = fc.showOpenMultipleDialog(null).iterator();
					ExtensionFilter ext = fc.getSelectedExtensionFilter();



					String paths = "";
					while(files.hasNext()) {
						File f = files.next();
						paths += f.getAbsolutePath();
						OpenSpatial.properties.store("file.previous", f.getParentFile().getAbsolutePath());
						if(files.hasNext()) {
							paths += ";";
						}

					}
					fileSelection.set(paths);



				} catch(Exception e) {
					logger.warn("No Files");
				}
			}
		};


	}
	@Override
	public String getID() {
		return MAPPING_ARCGIS;
	}

	@Override
	public int nParameters() {
		return 3;
	}

	@Override
	public Node createNode() {
//		Label l = new Label(getName());
		TextField filePath = new TextField(this.fileSelection.get());
		Button select = new Button("...",FontAwesome.FOLDER());
		filePath.textProperty().bindBidirectional(this.fileSelection);
		select.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser fc = new FileChooser();
				File f = fc.showOpenDialog(null);
				filePath.setText(f.getAbsolutePath());
			}
		});

		CheckBox smooth = new CheckBox("Smooth Elevations (Apply Inverse Distance?)");
		smooth.selectedProperty().bindBidirectional(this.isSmooth);
		HBox b = new HBox(new Label("Path"),filePath,select);
		VBox vb = new VBox(b,smooth);
		return vb;
	}

	@Override
	public boolean isExport() {
		return false;
	}

	@Override
	public String getFunctionName() {
		return "Map Elevations from ARGIS";
	}

}
