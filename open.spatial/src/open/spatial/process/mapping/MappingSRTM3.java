package open.spatial.process.mapping;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import open.spatial.OpenSpatial;
import open.spatial.fonts.FontAwesome;
import open.spatial.points.Point;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.interpolate.Interpolate;
import open.spatial.srtm.ARCGISASCII;
import open.spatial.srtm.SRTM;
import open.spatial.text.DataTable;
import uk.me.jstott.jcoord.UTMRef;

public class MappingSRTM3 extends OpenSpatialProcess{



	static final Logger logger = LogManager.getLogger(MappingSRTM3.class.getName());

	@Override
	public String getName() {
		return "Map SRTM-3 ELevations";

	}

	@Override
	public DataTable run(DataTable t) throws Exception {
		logger.info("Running " + getName());

		ArrayList<Point> pts = t.getPoints();
		ArrayList<Point> ptsout = new ArrayList<Point>();
		for(Point p : pts) {
			Point pm = p.clone();
			pm.z = SRTM.getSRTMElevation(p.lat, p.lon);
			ptsout.add(pm);
		}
		DataTable dtout = t.clone();
		dtout.setPoints(ptsout);
		logger.info("..." + dtout.getPoints().size() + " points projected.");
		return dtout;

	}

	@Override
	public int createControlReturnRow(GridPane gp, int row) {

		return row+1;
	}

	@Override
	public String getID() {
		return MAPPING_SRTM3;
	}

	@Override
	public int nParameters() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Node createNode() {
//		Label l = new Label(getName());
		Label url = new Label("https://dds.cr.usgs.gov/srtm/");
		return new HBox(url);
	}

	@Override
	public boolean isExport() {
		return false;
	}

	@Override
	public String getFunctionName() {
		return "Map Elevations from SRTM3";
	}

}
