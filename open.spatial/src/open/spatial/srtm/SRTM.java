package open.spatial.srtm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.OpenSpatial;
import open.spatial.examples.SRTMToPetrelPilbara;
import open.spatial.formats.CORGPR;
import open.spatial.formats.PetrelASCII;
import open.spatial.utils.UnZip;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class SRTM {
	static final Logger logger = LogManager.getLogger(SRTM.class.getName());

//	private final static String URL_Path_ROOT = "https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/";
	private final static String URL_PATH_ROOT = "https://web.archive.org/web/20200423075815/https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/";

	private final static String URL_Path_ROOT = "https://web.archive.org/web/20210207093316/https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/";

	private final static String [] dirs = {"Africa","Australia","Eurasia","Islands","North_America","South_America"};

	public final static NumberFormat nf = new DecimalFormat("000");
	public final static Properties SRTM_INDICES = loadSRTMProps();

	private final static LinkedHashMap<String, HGTInstance> SRTMBuffer = new LinkedHashMap<>();

	public static double getSRTMElevation(double lat, double lon) {
		File dir = new File("./SRTM3/");
		dir.mkdirs();
		boolean isNorth = lat >= 0;
		boolean isEast = lon >= 0;
		int zoneSouth = isNorth ? (int) Math.floor(Math.abs(lat)) : (int) Math.ceil(Math.abs(lat));
		int zoneEast = isEast ? (int) Math.floor(Math.abs(lon)) : (int) Math.ceil(Math.abs(lon));
		String id = (isNorth ? "N" : "S") + (String.format("%02d", zoneSouth)) + (isEast ? "E" : "W") + (String.format("%03d", zoneEast)) ;

		String d = SRTM_INDICES.getProperty(id);
		if(d == null) return 0;
		String path = URL_PATH_ROOT +  d + "/" + id + ".hgt.zip";

		String zipOut = dir.getAbsolutePath() + "/" + "" + id  + ".hgt.zip";
		String hgtOut = dir.getAbsolutePath() + "/" + "" + id  + ".hgt";
		if(!new File(hgtOut).exists()) {
			logger.info("Downloading: " + path);
			downloadAndUnzip(dir, path,zipOut);
		}

		if(!SRTMBuffer.containsKey(id)) {
			logger.info("Loading: " + id);
			SRTMBuffer.put(id, HGTInstance.load(hgtOut,id));
		}


		return SRTMBuffer.get(id).getElevation(lat,lon);

	}


	private static void downloadAndUnzip(File dir, String path, String zipOut) {
		try {

			if(new File(zipOut).exists()) {
				UnZip.unzip(zipOut, dir.getAbsolutePath());
			} else {

	    		String out = executePost(path, "");
	    		URL url = new URL(path);
	    		URLConnection conn = url.openConnection();

		        InputStream in = conn.getInputStream();

		        FileOutputStream os = new FileOutputStream(zipOut);
		        byte[] b = new byte[1024];
		        int count;
		        while ((count = in.read(b)) >= 0) {
		            os.write(b, 0, count);
		        }
		        os.flush(); os.close(); in.close();
		        UnZip.unzip(zipOut, dir.getAbsolutePath());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	private static Properties loadSRTMProps() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(new File("SRTM.properties"));

			// load a properties file
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}


	public static void main(String[] args) throws Exception{

		OpenSpatial.initLogger();

		File fout = new File("c:\\scratch\\SRTM_S32E121.ascii");
		File f = new File("SRTM3\\S31E121.hgt");
		HGTInstance hgt = HGTInstance.load(f.getAbsolutePath(), "S32E121");
		BufferedWriter out = PetrelASCII.createWriter(fout);
		for(int i = 0 ; i < hgt.latitudes.size() ; i++) {
			for(int j = 0 ; j < hgt.longitudes.size() ; j++) {
				double lat = hgt.latitudes.get(i);
				double lon = hgt.longitudes.get(j);
				UTMRef u = new LatLng(lat, lon).toUTMRef();
				double x = u.getEasting();
				double y = u.getNorthing();
			
				try {
					double z = hgt.elevations.get(hgt.latitudes.size() - 1 - i).get(hgt.longitudes.size() - j);
					if( z < 10000) {
						out.write(x + "\t" + y + "\t" + -z);
						out.newLine();
					}
				} catch(Exception e) {
					out.write(x + "\t" + y + "\t" + 0.0);
					out.newLine();
				}

			}
		}
		out.close();
		
//		double z1 = getSRTMElevation( 19.253624, -6.015227);
//		double z2 = getSRTMElevation( -34.407104,  19.860778);
//		double z3 = getSRTMElevation( -32.031451, 115.821507);
//		double z4 = getSRTMElevation( -31.986046, 116.083962);
//		double z5 = getSRTMElevation(  15.368070, 104.004306);
//		double z6 = getSRTMElevation( -12.200822, -60.535696);
//		System.out.println(z1 + " ~= " + 315);
//		System.out.println(z2 + " ~= " + 211);
//		System.out.println(z3 + " ~= " + 9);
//		System.out.println(z4 + " ~= " + 229);
//		System.out.println(z5 + " ~= " + 122);
//		System.out.println(z6 + " ~= " + 300);
	}
	public static String executePost(String targetURL, String urlParameters) {
		  HttpURLConnection connection = null;

		  try {
		    //Create connection
		    URL url = new URL(targetURL);
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type",
		        "application/x-www-form-urlencoded");

		    connection.setRequestProperty("Content-Length",
		        Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");

		    connection.setUseCaches(false);
		    connection.setDoOutput(true);

		    //Send request
		    DataOutputStream wr = new DataOutputStream (
		        connection.getOutputStream());
		    wr.writeBytes(urlParameters);
		    wr.close();

		    //Get Response
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		      response.append('\r');
		    }
		    rd.close();
		    return response.toString();
		  } catch (Exception e) {
		    e.printStackTrace();
		    return null;
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
		}

}
