package open.spatial.srtm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import open.spatial.OpenSpatial;

public class TimsThicket {
	public static void main(String[] args) throws Exception{
		File path = new File("F:/Tims_Thickett/Elevation_Large.asc");
		File pathOut = new File("F:/Tims_Thickett/TimsThickett_XYZ_Large.txt");
		
		OpenSpatial.initLogger();
		
		ARCGISASCII ascii = ARCGISASCII.read(path);
		BufferedWriter out = new BufferedWriter(new FileWriter(pathOut));
		out.write("E\tN\tZ\n");
		for(int i = 0 ; i < ascii.xs.length ; i++) {
			double [] slice = ascii.elev[i];
			for(int j = 0 ; j < ascii.ys.length ; j++) {
				if(!Double.isNaN(slice[j])) {
					out.write(ascii.xs[i] + "\t" + ascii.ys[j] + "\t" + slice[j] );
					out.newLine();
				}
			}	
		}
		out.close();
		
		
		
	}
}
