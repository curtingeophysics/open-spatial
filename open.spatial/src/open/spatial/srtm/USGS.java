package open.spatial.srtm;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class USGS {
	public static String executePost(String targetURL, String urlParameters) {
		  HttpURLConnection connection = null;

		  try {
		    //Create connection
		    URL url = new URL(targetURL);
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type", 
		        "application/x-www-form-urlencoded");

		    connection.setRequestProperty("Content-Length", 
		        Integer.toString(urlParameters.getBytes().length));
		    connection.setRequestProperty("Content-Language", "en-US");  

		    connection.setUseCaches(false);
		    connection.setDoOutput(true);

		    //Send request
		    DataOutputStream wr = new DataOutputStream (
		        connection.getOutputStream());
		    wr.writeBytes(urlParameters);
		    wr.close();

		    //Get Response  
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		      response.append('\r');
		    }
		    rd.close();
		    return response.toString();
		  } catch (Exception e) {
		    e.printStackTrace();
		    return null;
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
		}
	
	public static double getAltitude(Double longitude, Double latitude) {
	    double result = Double.NaN;
	    
	    String url = "http://gisdata.usgs.gov/"
	            + "xmlwebservices2/elevation_service.asmx/"
	            + "getElevation?X_Value=" + String.valueOf(longitude)
	            + "&Y_Value=" + String.valueOf(latitude)
	            + "&Elevation_Units=METERS&Source_Layer=-1&Elevation_Only=true";
	    HttpURLConnection connection = null;
	    String out = executePost(url, "");
	    System.out.println(out);
	    return -1;
//	    try {
//	        HttpResponse response = httpClient.execute(httpGet, localContext);
//	        HttpEntity entity = response.getEntity();
//	        if (entity != null) {
//	            InputStream instream = entity.getContent();
//	            int r = -1;
//	            StringBuffer respStr = new StringBuffer();
//	            while ((r = instream.read()) != -1)
//	                respStr.append((char) r);
//	            String tagOpen = "<double>";
//	            String tagClose = "</double>";
//	            if (respStr.indexOf(tagOpen) != -1) {
//	                int start = respStr.indexOf(tagOpen) + tagOpen.length();
//	                int end = respStr.indexOf(tagClose);
//	                String value = respStr.substring(start, end);
//	                result = Double.parseDouble(value);
//	            }
//	            instream.close();
//	        }
//	    } catch (ClientProtocolException e) {} 
//	    catch (IOException e) {}
//	    return result;
	}
	public static void main(String[] args) {
		
		getAltitude(115.77882062484198, -31.3095294324518);
	}
}
