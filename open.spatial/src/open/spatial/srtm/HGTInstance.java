package open.spatial.srtm;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class HGTInstance {

	public ArrayList<Double> latitudes;
	public ArrayList<Double> longitudes;
	public ArrayList<ArrayList<Double>> elevations;

	public HGTInstance(ArrayList<Double> latitudes, ArrayList<Double> longitudes, ArrayList<ArrayList<Double>> elevations) {
		this.latitudes = latitudes;
		this.longitudes = longitudes;
		this.elevations = elevations;
	}
	
	public static HGTInstance load(String path, String id) {
			ArrayList<Double> latitudes = new ArrayList<>();
			ArrayList<Double> longitudes = new ArrayList<>();
			ArrayList<ArrayList<Double>> elevations = new ArrayList<>();

	        String inputFilesString = null;
	        String fileName = path;
	        int i = 0;
	        int row, col, rows, cols;
	  
	        int numImages = 0;
	        double noData = -32768;
	        String returnHeaderFile = "";

	        String imageFile = path;

	        try {

	        	int progress = (int) (100f * i / (numImages - 1));


                File file = new File(fileName);
                long fileLength = file.length();
                file = null;

                /* First you need to figure out if this is an SRTM-1 or SRTM-3 image.
                 SRTM-1 has 3601 x 3601 or 12967201 cells and SRTM-3 has 1201 x 1201
                 or 1442401 cells. Each cell is 2 bytes. 
                 */
                String srtmFormat = "SRTM1";
                if (fileLength == 3601 * 3601 * 2) {
                    rows = 3601;
                    cols = 3601;
                } else if (fileLength == 1201 * 1201 * 2) {
                    rows = 1201;
                    cols = 1201;
                    srtmFormat = "SRTM3";
                } else {
                   throw new Exception("Does not appear to be SRTM 1 or SRTM 3");
                }

                double cellSize = 1.0 / cols;

                // Now get the coordinates of the raster edges by breaking the 
                // file name into its components.
                char[] charArray = id.toCharArray();
                char[] tmp = new char[2];
                tmp[0] = charArray[1];
                tmp[1] = charArray[2];
                double south = Double.parseDouble(new String(tmp));
                if (charArray[0] == 'S' || charArray[0] == 's') {
                    south = -south;
                }
                south = south - (0.5 * cellSize); // coordinate of ll cell edge

                tmp = new char[3];
                tmp[0] = charArray[4];
                tmp[1] = charArray[5];
                tmp[2] = charArray[6];
                double west = Double.parseDouble(new String(tmp));
                if (charArray[3] == 'W' || charArray[3] == 'w') {
                    west = -west;
                }
                west = west - (0.5 * cellSize); // coordinate of ll cell edge

                double north = south + 1.0 + cellSize; // coordinate of ur cell edge
                double east = west + 1.0 + cellSize; // coordinate of ur cell edge

               
   
                
                for(int r = 0 ; r < rows ; r++) {
                	double lng = west + r * cellSize;
                	longitudes.add(new Double(lng));
                }
                for(int c = 0 ; c < cols ; c++) {
                	double lat =  south + c * cellSize;
                	latitudes.add(new Double(lat));
                }

                RandomAccessFile rIn = null;
                FileChannel inChannel = null;
                ByteBuffer buf = ByteBuffer.allocate((int) fileLength);
                rIn = new RandomAccessFile(fileName, "r");

                inChannel = rIn.getChannel();

                inChannel.position(0);
                inChannel.read(buf);

                java.nio.ByteOrder byteorder = java.nio.ByteOrder.BIG_ENDIAN;
                // Check the byte order.
                buf.order(byteorder);
                buf.rewind();
                byte[] ba = new byte[(int) fileLength];
                buf.get(ba);
                double z;
                row = 0;
                col = 0;
                int pos = 0;
                int oldProgress = -1;
                //row = latitude
                //col = longitude
                for (row = 0; row < rows; row++) {
                	ArrayList<Double> elevsRow = new ArrayList<>();
                    for (col = 0; col < cols; col++) {
                        z = (double) buf.getShort(pos);
                        elevsRow.add(z);                        
                        pos += 2;
//                        System.out.print((int) z + "\t");
                    }
//                    System.out.println();
                    progress = (int)(100f * row / (rows - 1));
                    elevations.add(0,elevsRow); //flip it so lowest lat is at zero index
                }
                
                inChannel.close();
                
             
	        } catch(Exception e) {
	        	e.printStackTrace();
	        }
	
	        return new HGTInstance(latitudes, longitudes, elevations);
	}

	public double getElevation(double lat, double lon) {
		int nearestLat = Collections.binarySearch(latitudes, lat);
		int nearestLon = Collections.binarySearch(longitudes, lon);
		
		nearestLat = nearestLat >= 0 ? nearestLat : Math.max(0,-nearestLat-2);
		nearestLon = nearestLon >= 0 ? nearestLon : Math.max(0,-nearestLon-2);
//		System.out.println("Find: " + lat);
//		System.out.println(latitudes);
//		System.out.println("Find: " + lon);
//		for(double l : longitudes) {
//			System.out.println(l + ": " + lon);
//		}
		
//		System.out.println(nearestLat);
//		System.out.println(nearestLon);

		return elevations.get(lat < 0 ? nearestLat : latitudes.size() - nearestLat).get(nearestLon);
	}

}
