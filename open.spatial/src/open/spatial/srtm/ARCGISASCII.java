package open.spatial.srtm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import open.spatial.OpenSpatial;
import open.spatial.Text;
import open.spatial.formats.PetrelASCII;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.UTMRef;

public class ARCGISASCII {
	static final Logger logger = LogManager.getLogger(ARCGISASCII.class.getName());
	public double [] xs;
	public double [] ys;
    public double [][] elev;

    private ArrayList<Double> eastings; //may not be ordered the same as xs and ys arrays
    private ArrayList<Double> northings; //may not be ordered the same as xs and ys arrays
    boolean isEastingsReversed;
    boolean isNorthingsReversed;

    private final static LinkedHashMap<String, ARCGISASCII> saved = new LinkedHashMap<>();

    
//    public void convertToUTM() {
//    	logger.info("Converting from Lat Lon to UTM");
//    	logger.info("====WARNING ALL DATA MUST BE IN THE SAME UTM ZONE====");
//    	ArrayList<Double> longitudes = new ArrayList<>(eastings);
//    	ArrayList<Double> latitudes = new ArrayList<>(northings);
//    	ArrayList<Double> es = new ArrayList<>();
//    	ArrayList<Double> ns = new ArrayList<>();
//    	
//    	
//    }
    
    public ARCGISASCII(double [] xs, double [] ys, double [][] elev) {
    	this.xs = xs;
    	this.ys = ys;
    	this.elev = elev;

    	 eastings = new ArrayList<Double>();
    	 northings = new ArrayList<Double>();
    	 for(double d: xs) {
    		 eastings.add(d);
    	 }
    	 for(double d: ys) {
    		 northings.add(d);
    	 }
    	 isEastingsReversed = eastings.get(1) < eastings.get(0);
    	 isNorthingsReversed = northings.get(1) < northings.get(0);
    	 Collections.sort(eastings);
    	 Collections.sort(northings);

	}
    
    
    
    
    public static ArrayList<ARCGISASCII> readInstances(File dir) {
    	ArrayList<ARCGISASCII> list = new ArrayList<>();    
    	for(File f : dir.listFiles()) {
    		System.out.println("...reading " + f.getName());
    		try {
				list.add(read(f));
			} catch (Exception e) {			
				logger.error(e);
			}
    	}
    	return list;
    }
    
	public boolean within(double e, double n) {
		double minX = Double.min(xs[0],xs[xs.length - 1]);
		if(e < minX) return false;
		double maxX = Double.max(xs[0],xs[xs.length - 1]);
		if(e > maxX) return false;		
		double minY = Double.min(ys[0],ys[ys.length - 1]);
		if(n < minY) return false;
		double maxY = Double.max(ys[0],ys[ys.length - 1]);
		if(n > maxY) return false;
		
		return true;
	}



    public double getClosestElevation(double easting, double northing) {
    	int idx = Collections.binarySearch(eastings, easting);
    	int idy = Collections.binarySearch(northings, northing);

    	idx = idx >= 0 ? idx : Math.max(0,-idx-2);
    	idy = idy >= 0 ? idy : Math.max(0,-idy-2);
    	idx = isEastingsReversed ? xs.length - idx - 1 : idx;
    	idy = isNorthingsReversed ? ys.length - idy - 1 : idy;
//    	System.out.println(easting + "\t" + northing + "\t:" + idx + "\t" + idy) ;
    	return elev[idx][idy];
    }
    
    
    public double getClosestElevationSmooth(double easting, double northing) {
    	int idx = Collections.binarySearch(eastings, easting);
    	int idy = Collections.binarySearch(northings, northing);

    	idx = idx >= 0 ? idx : Math.max(0,-idx-2);
    	idy = idy >= 0 ? idy : Math.max(0,-idy-2);
    	idx = isEastingsReversed ? xs.length - idx - 1 : idx;
    	idy = isNorthingsReversed ? ys.length - idy - 1 : idy;    	    	   

    	int idxmin = Math.max(0,idx - 1);
    	int idxmax = Math.min(eastings.size()-1,idx + 1);
    	int idymin = Math.max(0,idy - 1);
    	int idymax = Math.min(northings.size()-1,idy + 1);
    	
    	ArrayList<Double> offsets = new ArrayList<Double>();
    	ArrayList<Double> values = new ArrayList<Double>();
    	for (int i = idxmin ; i <= idxmax ; i++) {
    		double e = eastings.get(i);
    		double es = Math.pow(e, 2);
    		for (int j = idymin ; j <= idymax ; j++) {
    			double n = northings.get(i);
    			double ns = Math.pow(n, 2);
    			double o = Math.sqrt(es+ns);
    			offsets.add(o);
    			double z = elev[i][j];
    			if(o==0) return z;
    			else values.add(z);    			
        	}	
    	}
    	double sumweightedsq = 0;
    	double sumweights = 0;
    	for(int i = 0; i < offsets.size() ;i++) {
    		double o = offsets.get(i);
    		double so = Math.pow(o, 2);    		
    		double weight = 1/so;
    		sumweights += weight;
    		sumweightedsq += values.get(i) * weight;    		    				
    	}
    	return sumweightedsq / sumweights;

    }
    
    public double [] getClosestElevationAndOffset(double easting, double northing) {
    	
    	int idx = Collections.binarySearch(eastings, easting);
    	int idy = Collections.binarySearch(northings, northing);

    	idx = idx >= 0 ? idx : Math.max(0,-idx-2);
    	idy = idy >= 0 ? idy : Math.max(0,-idy-2);
    	
    	double e = eastings.get(idx);
    	double n = northings.get(idy);
    	
    	idx = isEastingsReversed ? xs.length - idx - 1 : idx;
    	idy = isNorthingsReversed ? ys.length - idy - 1 : idy;
//    	System.out.println(easting + "\t" + northing + "\t:" + idx + "\t" + idy) ;
    	
    	
    	
    	double dx = e - easting;
    	double dy = n - northing;
    	
    	double [] z = {elev[idx][idy], Math.sqrt(Math.pow(dx, 2)+Math.pow(dy, 2))};
    	
    	return z;
    }

	public static void main(String[] args) throws Exception{

		OpenSpatial.initLogger();
//		File fin = new File("W:/Geoscience_Australia_Ord_Bonaparte/Data/DEM/OneDrive-2018-07-26/ob_dem_asc.txt");
//		double minEast = 453000;
//		double maxEast = 493000;
//
//		double minNorth= 8322000;
//		double maxNorth = 8350000;
//		File fout = new File("W:/Geoscience_Australia_Ord_Bonaparte/Data/DEM/OneDrive-2018-07-26/Ord_Bonaparte_Petrel_Points_Cut_Decimated.txt");
//		convertToPetrelPoints(fin, fout,minEast,maxEast,minNorth,maxNorth,3);
////		File fin = new File("G:/20_Work/2018_Ord_Bonaparte/DEM/ob_dem_asc.txt");
////		read(fin);

		String dirs = "H:\\Perth_Basin_Model\\002_Bathymetry\\";
		
		File dir = new File("H:\\Perth_Basin_Model\\002_Bathymetry\\ASC_\\");
		File fout = new File("H:\\\\Perth_Basin_Model\\\\002_Bathymetry\\Surface_02_Perth_Basin_Bathy_150_150_v2.ascii");		
		readInstances(dir);
		
		
//		double minX = 207750;
//		double maxX = 450750;
//		double maxY = 6902700;
//		double minY = 6181700;
//		
//		double dx = 100;
//		double dy = 100;
		
		
		double minX = 114;
		double maxX = 116.5;
		double maxY = -28;
		double minY = -34.5;
		
		double dx = 0.001;
		double dy = 0.001;
		
		
//		ArrayList<Double> xs = createOffsets(minX,maxX,dx);
//		ArrayList<Double> ys = createOffsets(minY,maxY,dy);
//		
//		double [][] grid = new double[xs.size()][ys.size()];
//		double [][] offsets = new double[xs.size()][ys.size()];
//		
		BufferedWriter out = PetrelASCII.createWriter(fout);
		
		
		
		for(double x = minX ; x <= maxX ; x += dx) {
			for(double y = minY ; y <= maxY ; y += dy) {
				double closestOffset = Double.MAX_VALUE;
				double elev = -1;
				
				for(String key : saved.keySet()) {
					double [] zz = saved.get(key).getClosestElevationAndOffset(x, y);
					if(zz[1] < closestOffset) {
						closestOffset = zz[1];
						elev = zz[0];
//						System.out.println(x  + "\t" + y + "\t" + closestOffset);
					}					
					
				}
//				System.out.println(closestOffset);
				if(closestOffset <= 0.0010759314947986199*4) {
					out.write(x + "\t" + y + "\t" + elev);
					out.newLine();
				} else {
					out.write(x + "\t" + y + "\t" + -0.1);
					out.newLine();
				}
			}	
		}
		out.close();
		
		
		
//		for(String key : saved.keySet()) {
//			saved.get(key).convertToPetrelPoints(new File("H:\\Perth_Basin_Model\\001_Land_Surface\\Petrel\\01_Perth_Basin_Topo_" + key + ".ASCII"), 50, 50);
//		}
//		
	}


	private static ArrayList<Double> createOffsets(double min, double max, double d) {
		ArrayList<Double> values = new ArrayList<>(); 
				 
		for(double x = min ; x <= max ; x += d) {
			values.add(x);
		}
		return values;
	}

	public void convertToPetrelPoints(File fout, double dx, double dy) throws Exception{
		logger.info("Converting DEM to Petrel Points");		
		logger.info("...out: " + fout.getAbsolutePath());

		BufferedWriter out = PetrelASCII.createWriter(fout);

		ArrayList<Double> xsi = createSpacings(Math.min(xs[0], xs[xs.length-1]), Math.max(xs[0], xs[xs.length-1]), dx);
		ArrayList<Double> ysi = createSpacings(Math.min(ys[0], ys[ys.length-1]), Math.max(ys[0], ys[ys.length-1]), dy);
		System.out.println(xs[0] + "\t" +xs[xs.length-1] + "\t" + ysi);
		for(double x : xsi) {
			int idx = Collections.binarySearch(eastings, x);	    
	    	idx = idx >= 0 ? idx : Math.max(0,-idx-2);	    	
	    	idx = isEastingsReversed ? xs.length - idx - 1 : idx;	    	
//	    	System.out.println(easting + "\t" + northing + "\t:" + idx + "\t" + idy) ;
	    	
			for(double y : ysi) {
				int idy = Collections.binarySearch(northings, y);
		    	idy = idy >= 0 ? idy : Math.max(0,-idy-2);
		    	idy = isNorthingsReversed ? ys.length - idy - 1 : idy;
		    	double z = elev[idx][idy];
		    	double elevation = (z != 0.0 && Math.abs(z) < 1E30 && z > -9999) ? z : -999.25;
		    	out.write(x + "\t" + y + "\t" + elevation);
		    	out.newLine();
			}
			
		}
		
		
//		int ncol = Integer.valueOf(in.readLine().replaceAll("ncols ", "").replaceAll(" ", ""));
//		int nrow = Integer.valueOf(in.readLine().replaceAll("nrows ", "").replaceAll(" ", ""));
//		String xllcorner = in.readLine().replaceAll("xllcorner ", "");;
//		String yllcorner = in.readLine().replaceAll("yllcorner ", "");;
//		String cellsize = in.readLine().replaceAll("cellsize ", "");;
//		String nodata = in.readLine().replaceAll("nodata_value ", "");
//		double xll = Double.valueOf(xllcorner);
//		double yll = Double.valueOf(yllcorner);
//		double dx = Double.valueOf(cellsize);
//		double xr = xll + ncol * dx;
//		double yu = yll + nrow * dx;
//		String line = "";
//		int col = 0;
//		int row = 0;
//		int reportingN = 1000;
//		logger.info("Starting...." + fin.getName());
//		int nth = 0;
//		logger.info("...Lower-Left Coordinate: " + xll + "\t" + yll);
//		logger.info("...Upper-Right Coordinate: " + xr + "\t" + yu);
//		int nthExported = 0;
//		while((line = in.readLine()) != null) {
//			double y = yu - row*dx;
//
//			if(y > minNorthing && y < maxNorthing) {
//
//				ArrayList<Double> elevs = Text.split(line.replaceAll("\\+",""));
//				col = 0;
//
//				for(Double z : elevs) {
//					double elevation = (z != 0.0 && Math.abs(z) < 1E30) ? z : -999.25;
//					double x = xll + col*dx;
//					if(x > minEasting && x < maxEasting) {
//						if(nth%nthpoint == 0) {
//							out.write(x + "\t" + y + "\t" + elevation);
//							out.newLine();
//
//						}
//					}
//					col++;
//				}
//				nth++;
//				if(nth%reportingN == 0) {
//					logger.info("...Reading " + nth);
//				}
//			}
//			row++;
//		}
		out.close();
//		in.close();
	}

	
	private static ArrayList<Double> createSpacings(double start, double end, double dx) {
		ArrayList<Double> vals = new ArrayList<>();
		for(double v = start ; v <= end ; v+= dx) {
			vals.add(v);
		}
		return vals;
	}

	public static void convertToPetrelPoints(File fin, File fout, double minEasting, double maxEasting, double minNorthing, double maxNorthing, int nthpoint) throws Exception{
		logger.info("Converting DEM to Petrel Points");
		logger.info("...in: " + fin.getAbsolutePath());
		logger.info("...out: " + fout.getAbsolutePath());
		BufferedReader in = new BufferedReader(new FileReader(fin));
		BufferedWriter out = PetrelASCII.createWriter(fout);

		int ncol = Integer.valueOf(in.readLine().replaceAll("ncols ", "").replaceAll(" ", ""));
		int nrow = Integer.valueOf(in.readLine().replaceAll("nrows ", "").replaceAll(" ", ""));
		String xllcorner = in.readLine().replaceAll("xllcorner ", "");;
		String yllcorner = in.readLine().replaceAll("yllcorner ", "");;
		String cellsize = in.readLine().replaceAll("cellsize ", "");;
		String nodata = in.readLine().replaceAll("nodata_value ", "");
		double xll = Double.valueOf(xllcorner);
		double yll = Double.valueOf(yllcorner);
		double dx = Double.valueOf(cellsize);
		double xr = xll + ncol * dx;
		double yu = yll + nrow * dx;
		String line = "";
		int col = 0;
		int row = 0;
		int reportingN = 1000;
		logger.info("Starting...." + fin.getName());
		int nth = 0;
		logger.info("...Lower-Left Coordinate: " + xll + "\t" + yll);
		logger.info("...Upper-Right Coordinate: " + xr + "\t" + yu);
		logger.info("...Cell Size: " + cellsize);
		int nthExported = 0;
		while((line = in.readLine()) != null) {
			double y = yu - row*dx;

			if(y > minNorthing && y < maxNorthing) {

				ArrayList<Double> elevs = Text.split(line.replaceAll("\\+",""));
				col = 0;

				for(Double z : elevs) {
					double elevation = (z != 0.0 && Math.abs(z) < 1E30) ? z : -999.25;
					double x = xll + col*dx;
					if(x > minEasting && x < maxEasting) {
						if(nth%nthpoint == 0) {
							out.write(x + "\t" + y + "\t" + elevation);
							out.newLine();

						}
					}
					col++;
				}
				nth++;
				if(nth%reportingN == 0) {
					logger.info("...Reading " + nth);
				}
			}
			row++;
		}
		out.close();
		in.close();
	}

	public static ARCGISASCII read(File f) throws Exception{
		if(saved.get(f.getName()) == null) {

		} else {
			logger.info("DEM Preloaded: " + f.getAbsolutePath());
			return saved.get(f.getName());
		}
		logger.info("Reading DEM Lidar");
		logger.info("...in: " + f.getAbsolutePath());
		BufferedReader in = new BufferedReader(new FileReader(f));

		int ncol = Integer.valueOf(in.readLine().replaceAll("ncols ", "").replaceAll("NCOLS","").replaceAll(" ", ""));
		int nrow = Integer.valueOf(in.readLine().replaceAll("nrows ", "").replaceAll("NROWS","").replaceAll(" ", ""));
		String xllcorner = in.readLine().replaceAll("xllcorner ", "").replaceAll("XLLCORNER","");;
		String yllcorner = in.readLine().replaceAll("yllcorner ", "").replaceAll("YLLCORNER","");;
		String cellsize = in.readLine().replaceAll("cellsize ", "").replaceAll("CELLSIZE","");;
		String nodata = in.readLine().replaceAll("nodata_value ", "").replaceAll("NODATA_VALUE","");;
        double [] xs = new double[ncol];
        double [] ys = new double[nrow];


        double [][] elev = new double[ncol][nrow];
		double xll = Double.valueOf(xllcorner);
		double yll = Double.valueOf(yllcorner);
		double dx = Double.valueOf(cellsize);
		double xr = xll + ncol * dx;
		double yu = yll + nrow * dx;


		double startX = new Double(xll);
		String line = "";
		int col = 0;
		int row = 0;


		int reportingN = 500;
		int nth = 0;


		logger.info("...Lower-Left Coordinate: " + xll + "\t" + yll);
		logger.info("...Upper-Right Coordinate: " + xr + "\t" + yu);
		logger.info("...Cell Size: " + cellsize);
		ArrayList<ArrayList<Double>> rows = new ArrayList<ArrayList<Double>>();
		while((line = in.readLine()) != null) {

			rows.add(Text.split(line.replaceAll("\\+","")));
			nth++;
			if(nth%reportingN == 0) {
				logger.info("...Reading " + nth);
			}

		}
		logger.info("...Done!" + nth);



		for(int i = 0 ; i < rows.get(0).size() ; i++) {
			xs[i] =  xll + i*dx;
//			eastings.add(xs[i]);
		}
		for(int i = 0 ; i < rows.size() ; i++) {
			ys[i] =  yu - i*dx;
//			northings.add(ys[i]);
		}

		logger.info("Sanitizing Null Values");
		for(int i = 0 ; i < rows.size() ; i++) {
			ArrayList<Double> list = rows.get(i);
			for(int j = 0 ; j < list.size() ; j++) {
				double z = list.get(j);
				elev[j][i] = (z != 0.0 && Math.abs(z) < 1E30) ? z : Double.NaN;
			}
		}



		logger.info("....Done!");
		in.close();
		ARCGISASCII a = new ARCGISASCII(xs, ys, elev);
		saved.put(f.getName(),a);
		return a;
	}




	public ArrayList<Point> mapElevations(ArrayList<Point> pts,boolean smooth) {
		ArrayList<Point> mpts = new ArrayList<Point>();
		for(Point p : pts) {
			Point pm = p.clone();
			UTMRef r = pm.getUTM();			
			double z = smooth ? getClosestElevationSmooth(r.getEasting(), r.getNorthing()) : getClosestElevation(r.getEasting(), r.getNorthing());
			pm.z = z;
			mpts.add(pm);
		}
		
		return mpts;
	}


}
