package open.spatial.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

	private Properties prop;
	private File f;

	public PropertiesManager(File f) {
		try {
			this.f = f;
			if(!f.exists())  {
				f.createNewFile();
			}
			Properties prop = new Properties();
			InputStream in = new FileInputStream(f);
			prop.load(in);
			in.close();
			this.prop = prop;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public String get(String key) {
		return prop.getProperty(key);
	}
	public String get(String key, String defaultValue) {
		return prop.getProperty(key,defaultValue);			
	}
	public void store(String key, String value) {
		prop.setProperty(key,value);

		try {
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(f));
			prop.store(out, "User key store " + SystemAnalyst.getCompactDate());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	public static PropertiesManager create (File f) {
		try {
			if(f == null) System.err.println("Properties File is null.");
			if(!f.exists()) System.err.println("Cannot find " + f.getAbsolutePath());
			return new PropertiesManager(f);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
