package open.spatial.utils;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

public class SystemAnalyst {
	public static native long getProcessCPUTime();

	public static final int MAC = 2;
	public static final int LINUX = 1;
	public static final int WINDOWS = 0;

	public static final int ANDROID = 3;
	public static final int IOS = 4;

	public static String getOSName() {
		return System.getProperty("os.name");
	}

	public static String getOSName(int i) {
		switch (i) {
		case WINDOWS:
			return "Windows";
		case MAC:
			return "Windows";
		case LINUX:
			return "Linux";
		default:
			return "Unknown";
		}
	}

	public static boolean isWindows() {
		return getOSType() == WINDOWS;
	}

	public static boolean isLinux() {
		return getOSType() == LINUX;
	}

	public static boolean isMac() {
		return getOSType() == MAC;
	}


	public static ArrayList<Integer> getAllOperatingSystems() {
		ArrayList<Integer> l = new ArrayList<Integer>();
		l.add(MAC);
		l.add(LINUX);
		l.add(WINDOWS);
		return l;
	}

	public static File getHomeDirectory() {
		return new File(System.getProperty("user.home"));
	}

	public static int getOSType() {
		if (getOSName().contains("Windows"))
			return WINDOWS;
		else if (getOSName().toLowerCase().contains("mac"))
			return MAC;
		return LINUX;
	}

	public static int getNumberOfAvailableProcessors() {
		int processors = java.lang.Runtime.getRuntime().availableProcessors();
		return processors;
	}

	public static String getReverseDate() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}

	public static String getFormatDate(String format) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	public static String getDate() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(date);
	}

	public static String getCompactDate() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return dateFormat.format(date);
	}

	public static double getFreeMemory() {
		double freeMemory = java.lang.Runtime.getRuntime().freeMemory();
		Set<String> env = java.lang.System.getenv().keySet();

		return freeMemory;
	}

	public static String getProcessorArchitecture() {
		return java.lang.System.getenv().get("PROCESSOR_ARCHITECTURE");
	}

	public static String getProcessorIdentifier() {
		return java.lang.System.getenv().get("PROCESSOR_IDENTIFIER");
	}

	public static String getOS() {
		return java.lang.System.getenv().get("OS");
	}

	public static double getMaxMemory() {
		double maxMemory = java.lang.Runtime.getRuntime().maxMemory();
		return maxMemory;
	}

	public static double getUsedMemory() {
		double usedMemory = java.lang.Runtime.getRuntime().totalMemory() - getFreeMemory();
		return usedMemory;
	}

	public static double getTotalMemory() {
		double totalMemory = java.lang.Runtime.getRuntime().totalMemory();
		return totalMemory;
	}

	public static String getUser() {
		if (isWindows())
			java.lang.System.getenv().get("USERNAME");

		return System.getProperty("user.name");
	}

	public static Dimension getSreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}

	public static String getIP() {

		try {
			InetAddress addr = InetAddress.getByName("127.0.0.1");

			// Get hostname by a byte array containing the IP address
			byte[] ipAddr = new byte[] { 127, 0, 0, 1 };
			addr = InetAddress.getByAddress(ipAddr);

			// Get the host name
			String hostname = addr.getHostName();
			InetAddress addresses[] = InetAddress.getAllByName(hostname);
			for (InetAddress a : addresses)
				System.out.println(a.getCanonicalHostName());
			// Get canonical host name
			String hostnameCanonical = addr.getCanonicalHostName();
			return hostnameCanonical;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return null;

	}

	public static String getSystemInfo() {
		String s = "Optimising System....\n";
		s += ("\t..." + "Operating system " + getOS() + "\n");
		s += ("\t..." + "OS Name " + getOSName() + "\n");
		s += ("\t..." + "Processor architecture " + getProcessorIdentifier() + "\n");
		s += ("\t..." + getNumberOfAvailableProcessors() + " cores present" + "\n");
		s += ("\t..." + getFreeMemory() / 10000 + "\tJVM Free Memory  (Mb)" + "\n");
		s += ("\t..." + getUsedMemory() / 10000 + "\tJVM Used Memory  (Mb)" + "\n");
		s += ("\t..." + getTotalMemory() / 10000 + "\tJVM Total Memory (Mb)") + "\n\n\n";

		Set<String> keys = java.lang.System.getenv().keySet();
		for (String st : keys) {
			s += (st + "\t" + java.lang.System.getenv().get(st) + "\n");
		}
		return s;
	}



	public static void printSystemInfo() {
		System.out.println("System Information");
		System.out.println("\t..." + "Operating system " + getOS());
		System.out.println("\t..." + "OS Name " + getOSName());
		System.out.println("\t..." + "Processor architecture " + getProcessorIdentifier());

		System.out.println("\t..." + getNumberOfAvailableProcessors() + " cores present");
		System.out.println("\t..." + getFreeMemory() / 10000 + "\tJVM Free Memory  (Mb)");
		System.out.println("\t..." + getUsedMemory() / 10000 + "\tJVM Used Memory  (Mb)");
		System.out.println("\t..." + getTotalMemory() / 10000 + "\tJVM Total Memory (Mb)");

		Set<String> keys = java.lang.System.getenv().keySet();
		for (String s : keys) {
			System.out.println(s + "\t\t" + java.lang.System.getenv().get(s));
		}
	}


	public static String getddmmyyy() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(date);
	}

	public static String gethhmm() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		return dateFormat.format(date);
	}
}
