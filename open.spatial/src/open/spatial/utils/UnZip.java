package open.spatial.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip {
	
    public static void unzip(String zipPath, String outputFolder){

     byte[] buffer = new byte[1024];
    	
     try{    		
    	File folder = new File(outputFolder);
    	if(!folder.exists()){
    		folder.mkdir();
    	}    	
    	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipPath));
    	ZipEntry z = zis.getNextEntry();    		
    	while(z!=null){    			
    	   String fileName = z.getName();
           File newFile = new File(outputFolder + File.separator + fileName);
           new File(newFile.getParent()).mkdirs();              
           FileOutputStream fos = new FileOutputStream(newFile);             
           int len;
           while ((len = zis.read(buffer)) > 0) {
        	   fos.write(buffer, 0, len);
           }        		
           fos.close();   
           z = zis.getNextEntry();
    	}    	
        zis.closeEntry();
    	zis.close();    		
    }catch(IOException e){
       e.printStackTrace(); 
    }
   }    
}