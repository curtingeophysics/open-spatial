package open.spatial.log;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import javafx.scene.control.TextArea;


public class OpenSpatialLogging  extends AppenderSkeleton {
    private final TextArea area;

    public OpenSpatialLogging() {
        area = new TextArea();
    }
    protected void append(LoggingEvent event) 
    {
        if(event.getLevel().equals(Level.INFO)){
        area.appendText(event.getMessage().toString() + "\n");
        }
    }
    public void close() 
    {
    }
    public boolean requiresLayout() 
    {
        return false;
    }
}