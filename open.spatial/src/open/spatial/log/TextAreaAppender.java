package open.spatial.log;
import org.apache.log4j.Level;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;

import javafx.application.Platform;
import javafx.scene.control.TextArea;


public class TextAreaAppender extends WriterAppender {

    private static volatile TextArea console = null;

    public static void setConsole(final TextArea console) {
        TextAreaAppender.console = console;
        
    }


    @Override
    public void append(final LoggingEvent loggingEvent) {
//    	System.out.println(this.layout);
//    	System.out.println(loggingEvent);
    	
    	String cat = "";
    	switch(loggingEvent.getLevel().toInt()) {
    	case Level.INFO_INT :
    		cat = "[INFO]";
    		break;
    	case Level.DEBUG_INT :
    		cat = "[DEBUG]";
    		break;    	
    	case Level.ERROR_INT :
    		cat = "[ERROR]";
		break;
		
		case Level.WARN_INT :
			cat = "[WARN]";
		break;
		default :
			cat = "[MSG]";
		}
        final String message =  cat + "\t" + (loggingEvent.getMessage().toString()) +"\n";
        

        try {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (console != null) {
                            if (console.getText().length() == 0) {
                                console.setText(message);
                            } else {
                                console.selectEnd();
                                console.insertText(console.getText().length(),
                                        message);
                            }
                        }
                    } catch (final Throwable t) {
                        System.err.println(t.getMessage());
                    }
                }
            });
        } catch (final IllegalStateException e) {
        
        }
    }
}