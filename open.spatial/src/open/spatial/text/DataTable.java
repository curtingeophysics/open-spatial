package open.spatial.text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import open.spatial.crs.CRSConverter;
import open.spatial.crs.CoordinateTransformation;
import open.spatial.crs.Projections;
import open.spatial.crs.WTKInstance;
import open.spatial.points.Point;
import open.spatial.process.interpolate.Interpolate;

public class DataTable {
	static final Logger logger = LogManager.getLogger(DataTable.class.getName());
	public final static char DEFAULT_QUOTE = '\"';
	public final static char DEFAULT_SEPARATOR = '\t';
	public ArrayList<ArrayList<String>> header = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
	public WTKInstance wtkInputInstance, wtkOutputInstance;
	
	//custom projection for local coordinate transforms using affine.
	public boolean customTranformEnabled = false;
	public CoordinateTransformation customTranform;
	
	public IntegerProperty idcol = new SimpleIntegerProperty(-1);
	public IntegerProperty xcol = new SimpleIntegerProperty(-1);
	public IntegerProperty ycol = new SimpleIntegerProperty(-1);
	public IntegerProperty zcol = new SimpleIntegerProperty(-1);
	public IntegerProperty vcol = new SimpleIntegerProperty(-1);
	public IntegerProperty linecol = new SimpleIntegerProperty(-1);
	private ArrayList<Point> pts = null;
	
	
	

	public DataTable(ArrayList<ArrayList<String>> header,ArrayList<ArrayList<String>> values) {
		this.header = header;
		this.values = values;
	}
	public DataTable(ArrayList<Point> pts) {
		//to be used if no values are required. This will duplicate values to reconstruct values table
		logger.info("Creating Data Table: Reconstructing values Table");
		ArrayList<String> header = new ArrayList(Arrays.asList("id","x","y","z","v","line"));
		ArrayList<ArrayList<String>> headerLines = new ArrayList<>();
		headerLines.add(header);
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		for(Point p : pts) {
			ArrayList<String> row = new ArrayList<String>();
			row.add(p.name);
			row.add(""+ p.lon);
			row.add(""+ p.lat);
			row.add(""+ p.z);
			row.add(""+ p.value);
			row.add(""+ p.line);
			values.add(row);
		}
		this.values = values;
		this.header = headerLines;
		this.idcol.set(0);
		this.xcol.set(1);
		this.ycol.set(2);
		this.zcol.set(3);
		this.vcol.set(4);
		this.linecol.set(5);
		this.wtkInputInstance = Projections.idMap.get("EPSG:4326"); //WGS 84


	}
	public final static NumberFormat nf7 = new DecimalFormat("0000000");
	public final static NumberFormat nf6 = new DecimalFormat("000000");
	public final static NumberFormat nf5 = new DecimalFormat("00000");
	public final static NumberFormat nf4 = new DecimalFormat("0000");
	public final static NumberFormat nf3 = new DecimalFormat("000");
	public final static NumberFormat nf2 = new DecimalFormat("00");
	public final static NumberFormat nf1 = new DecimalFormat("0");


	public DataTable clone() {
		DataTable dt = new DataTable(header, values);
		dt.pts = new ArrayList<Point>(pts);
		return dt;
	}

	public HashMap<String, Integer> getLinesStationCount() {
		HashMap<String, Integer> lineHash = new LinkedHashMap<String, Integer>();
		if(linecol.get() == -1) {
			lineHash.put("0001", values.size());
		} else {
			for(Point row : pts) {
				if(lineHash.containsKey(row.line)) {
					lineHash.put(row.line,lineHash.get(row.line)+1);
				} else {
					lineHash.put(row.line,1);
				}
			}
		}
		return lineHash;
	}

	public int getHeaderID(String name) {
		if(header.size() == 1) {
			int nth = 0;
			for(String s : header.get(0)) {
				if(s.equals(name)) {
					return nth;
				}
				nth++;
			}
		} else {
			int nth = 0;
			for(ArrayList<String> headers : header) {
				for(String s : headers) {
					if(s.equals(name)) {
						return nth;
					}
					nth++;
				}
			}
		}
		return -1;
	}

	public ArrayList<String> getColumn(String name) {
		int col = getHeaderID(name);
		if(col == -1) return null;
		ArrayList<String> vals = new ArrayList<String>();
		for(ArrayList<String> row : values) {
			vals.add(row.get(col));
		}
		return vals;
	}


	public LinkedHashMap<String, ArrayList<Point>> getLinesPointsHash() {

		LinkedHashMap<String, ArrayList<Point>> lineMap = new LinkedHashMap<String, ArrayList<Point>>();

		try {
		for(Point p : getPoints()) {
			ArrayList<Point> pts = lineMap.get(p.line);
			if(pts == null) {
				pts = new ArrayList<Point>();
				lineMap.put(p.line, pts);
			}
			pts.add(p);
		}
		} catch(Exception e) {
			logger.error(e);
		}
		return lineMap;
	}

	public ArrayList<Point> getPoints() throws Exception {
		if(pts == null) {
			final CoordinateReferenceSystem targetCRS = CRS.parseWKT(CRSConverter.WGS84);
			final CoordinateReferenceSystem sourceCRS = CRS.parseWKT(wtkInputInstance.wtk);
			MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS,true);
			int dstOff = 0;
			int numPts = 1;
			ArrayList<Point> pts = new ArrayList<Point>();
			int i = 1;
			int NPoints = values.size();
			NumberFormat nf = NPoints < 10 ? nf1 : (NPoints < 100 ? nf2 : (NPoints < 1000 ? nf3 :  (NPoints < 10000 ? nf4 :  (NPoints < 100000 ? nf5 :  (NPoints < 1000000 ? nf6 : nf7)))));
//			System.out.println(sourceCRS);
			for(ArrayList<String> row : values) {
				double x = Double.valueOf(row.get(xcol.get()));
				double y = Double.valueOf(row.get(ycol.get()));
				double z = zcol.get() == -1 ? 0 : Double.valueOf(row.get(zcol.get()));
//

				double [] srcPts = {x,y,z};
				int srcOff = 0;
				double [] dstPts = {0,0,0};

				trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);
//				logger.info("(" + srcPts[0]+","+srcPts[1]+","+srcPts[2] + ")" + "->" + "(" + dstPts[0]+","+dstPts[1]+","+dstPts[2] + ")");

				Point p = new Point(dstPts[1], dstPts[0],dstPts[2]);
				p.name = (idcol.get() == -1) ? "pt" + nf.format(i) : row.get(idcol.get());
				p.line = (linecol.get() == -1) ? "0001" : row.get(linecol.get());
				pts.add(p);

			}
			this.pts  = pts;
		}

		return pts;
	}
	
	public ArrayList<double[]> getLocalProjectedXYZ() throws Exception {
		ArrayList<double[]> pts = new ArrayList<double[]>();
		int i = 1;
		int NPoints = values.size();
		NumberFormat nf = NPoints < 10 ? nf1 : (NPoints < 100 ? nf2 : (NPoints < 1000 ? nf3 :  (NPoints < 10000 ? nf4 :  (NPoints < 100000 ? nf5 :  (NPoints < 1000000 ? nf6 : nf7)))));
//		System.out.println(sourceCRS);
		for(ArrayList<String> row : values) {
			double x = Double.valueOf(row.get(xcol.get()));
			double y = Double.valueOf(row.get(ycol.get()));
			double z = zcol.get() == -1 ? 0 : Double.valueOf(row.get(zcol.get()));
//
			double [] dstPts = {0,0,z};

			double [] pout = customTranform.globalToLocal(x, y);
			dstPts[0] = pout[0];
			dstPts[1] = pout[1];
			
//			logger.info("(" + srcPts[0]+","+srcPts[1]+","+srcPts[2] + ")" + "->" + "(" + dstPts[0]+","+dstPts[1]+","+dstPts[2] + ")");

//			Point p = new Point(dstPts[1], dstPts[0],dstPts[2]);
//			p.name = (idcol.get() == -1) ? "pt" + nf.format(i) : row.get(idcol.get());
//			p.line = (linecol.get() == -1) ? "0001" : row.get(linecol.get());
			pts.add(dstPts);

		}
			
			

		return pts;
	}
	

	public ArrayList<double []> getProjectedXYZ() throws Exception{
		ArrayList<double [] > projectedPoints = new ArrayList<double[]>();
//		if(pts == null) {
			final CoordinateReferenceSystem targetCRS = CRS.parseWKT(wtkOutputInstance.wtk);
			final CoordinateReferenceSystem sourceCRS = CRS.parseWKT(wtkInputInstance.wtk);
			MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS,true);
			int dstOff = 0;
			int numPts = 1;
			ArrayList<Point> pts = new ArrayList<Point>();
			int i = 1;
			int NPoints = values.size();
			NumberFormat nf = NPoints < 10 ? nf1 : (NPoints < 100 ? nf2 : (NPoints < 1000 ? nf3 :  (NPoints < 10000 ? nf4 :  (NPoints < 100000 ? nf5 :  (NPoints < 1000000 ? nf6 : nf7)))));


			for(Point p : getPoints()) {
				double x = p.lon;
				double y = p.lat;
				double z = p.z;
				double [] srcPts = {x,y,z};
				int srcOff = 0;
				double [] dstPts = {0,0,0};
				trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);
			}

			for(ArrayList<String> row : values) {
				double x = Double.valueOf(row.get(xcol.get()));
				double y = Double.valueOf(row.get(ycol.get()));
				double z = zcol.get() == -1 ? 0 : Double.valueOf(row.get(zcol.get()));

				double [] srcPts = {x,y,z};
				int srcOff = 0;
				double [] dstPts = {0,0,0};
//				System.out.println(x + " " + y + " " + z);
				trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);

				projectedPoints.add(dstPts);
			}

//		}

		return projectedPoints;
	}

	public static DataTable importDataTable(File f, ArrayList<String> delimeters, boolean trimLeading, int nlines, int nheader) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(f));
		String line = "";
		int nth = 0;

		ArrayList<ArrayList<String>> header = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

		while((line = in.readLine()) != null && nth < nlines) {

			ArrayList<String> p = parse(line,delimeters,trimLeading);
			if(nth < nheader) {
				header.add(p);
			} else {
				values.add(p);
			}

			nth++;
		}


		in.close();
		return new DataTable(header, values);
	}

	private static ArrayList<String> parse(String line, ArrayList<String> delimeters, boolean trimLeading) {
		ArrayList<String> p = new ArrayList<String>();
		if(trimLeading) line = line.trim();
		for(String d : delimeters) {
			line = line.replaceAll(d, "\t");
		}

		return parseCSVLine(line,DEFAULT_SEPARATOR,DEFAULT_QUOTE);
	}
	public void setPoints(ArrayList<Point> outputPts) {
		if(outputPts.size() != values.size()) {
			logger.warn("Processed number of data points has changed from original. Output options limited.");
		}
		this.pts = outputPts;
	}

    public static ArrayList<String> parseCSVLine(String cvsLine, char separators, char customQuote) {

    	ArrayList<String> result = new ArrayList<>();

        //if empty, return!
        if (cvsLine == null && cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULT_QUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = cvsLine.toCharArray();

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {

                    //Fixed : allow "" in custom quote enclosed
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }

                }
            } else {
                if (ch == customQuote) {

                    inQuotes = true;

                    //Fixed : allow "" in empty quote enclosed
                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }

                    //double quotes in column will hit this!
                    if (startCollectChar) {
                        curVal.append('"');
                    }

                } else if (ch == separators) {

                    result.add(curVal.toString());

                    curVal = new StringBuffer();
                    startCollectChar = false;

                } else if (ch == '\r') {
                    //ignore LF characters
                    continue;
                } else if (ch == '\n') {
                    //the end, break!
                    break;
                } else {
                    curVal.append(ch);
                }
            }

        }

        result.add(curVal.toString());

        return result;
    }
    
    public void setCustomLocalCoordinateProjectionEnabled(boolean enabled) {
    	this.customTranformEnabled = enabled;
    }
    public void setCustomLocalCoordinateProjection(CoordinateTransformation tf) {
    	this.customTranform = tf;
    }
	public void setInputCRS(WTKInstance wtkInstance) {
		this.wtkInputInstance = wtkInstance;
	}
	public void setOutputCRS(WTKInstance wtkInstance) {
		this.wtkOutputInstance = wtkInstance;
	}
	



}
