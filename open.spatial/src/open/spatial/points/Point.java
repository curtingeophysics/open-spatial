package open.spatial.points;

import java.util.HashMap;
import java.util.LinkedHashMap;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class Point {

	public HashMap<String, Object> properties = new LinkedHashMap<String, Object>();

	public double lat, lon, z=0, value=0;
	public String name = "NA";

	public String line;

	public Point(double lat, double lon) {
		this.lat = lat;
		this.lon = lon;
	}
	public Point(double lat, double lon, double value) {
		this.lat = lat;
		this.lon = lon;
		this.value = value;
	}
	public Point(double lat, double lon, double z, double value) {
		this.lat = lat;
		this.lon = lon;
		this.value = value;
	}
	public Point clone() {
		Point p =  new Point(new Double(lat), new Double(lon), new Double(z), new Double(value));
		p.name = new String(name);
		p.line = new String(line);
		return p;
	}
	public Point(UTMRef ref, double z) {
		this(ref.toLatLng().getLatitude(),ref.toLatLng().getLongitude(),z,0);
	}
	public Point(UTMRef ref) {
		this(ref.toLatLng().getLatitude(),ref.toLatLng().getLongitude());
	}
	public LatLng getLatLon() {
		return new LatLng(lat, lon);
	}
	public UTMRef getUTM() {
		return getLatLon().toUTMRef();
	}


	@Override
	public String toString() {
		return name + "\t(" +lat +"," + lon + "," + z + ")";
	}

}
