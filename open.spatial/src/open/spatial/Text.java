package open.spatial;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Stream;

public class Text {

	private static int MINIMUM_NUMBER = (int) '0';
	private static int MAXIMUM_NUMBER = (int) '9';
	private static int EXCEPTIONS [] = {(int) 'E', (int) 'e',(int) '-', (int) '.'};

	public static ArrayList<Double> splitFastDouble(String s) {
//		int currentIndex = 0;
		ArrayList<Double> values = new ArrayList<Double>();
		String current = "";
		for(int i = 0 ; i < s.length() ; i++) {
			int character = (int) s.charAt(i);
			if(character >= MINIMUM_NUMBER && character <= MAXIMUM_NUMBER || isValid(character)) {
				current += ((char) character);
			} else if(current.length() != 0){
				try {
					values.add(Double.valueOf(current));
					current = "";
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		try {
			values.add(Double.valueOf(current));
			current = "";
		} catch(Exception e) {
			e.printStackTrace();
		}
		return values;
	}

	private static boolean isValid(int character) {
		switch (character) {
		case (int) 'E': return true;
		case (int) 'e': return true;
		case (int) '-': return true;
		case (int) '.': return true;
		default: return false;
		}

	}


	public static ArrayList<Double> split(String s, String token) {
		StringTokenizer st = new StringTokenizer(s, token);
		ArrayList<Double> split = new ArrayList<Double>();

	     while (st.hasMoreTokens()) {
	         try {
	        	 split.add(Double.valueOf(st.nextToken()));
	         } catch(Exception e) {

	         }
	     }
	     return split;
	}

	public static ArrayList<String> splitString(String s) {
		ArrayList<String> split = new ArrayList<String>();
		String st = "";
		boolean starts = false;
		for(int i = 0 ; i < s.length() ; i++) {
			char c = s.charAt(i);
			if(starts) {
				if(isValid(c)) {
					st += c;
				} else {
					try {
						split.add(st);
					} catch(Exception e) {
						e.printStackTrace();
					}
					starts = false;
					st = "";
				}
			} else {
				if(isValid(c)) {
					st += c;
					starts = true;
				}

			}
		}
		if(starts) {
			try {
				split.add(st);
 			} catch(Exception e) {
 				e.printStackTrace();
 			}
		}
		return split;
	}
	public static String[] sanitizeString(String line) {
		String sanitized = line.replaceAll(" ", ",").replaceAll("\t", ",");
		String [] split = sanitized.split(",");		
		return split;
	}
	public static ArrayList<Double> split(String s) {
		ArrayList<Double> split = new ArrayList<Double>();
		String st = "";
		boolean starts = false;
		for(int i = 0 ; i < s.length() ; i++) {
			char c = s.charAt(i);
			if(starts) {
				if(isValid(c)) {
					st += c;
				} else {
					try {
						split.add(Double.valueOf(st));
					} catch(Exception e) {
						System.err.println("PROBLEM WITH LINE " + s);
						e.printStackTrace();
					}
					starts = false;
					st = "";
				}
			} else {
				if(isValid(c)) {
					st += c;
					starts = true;
				}

			}
		}
		if(starts) {
//			try {
				split.add(Double.valueOf(st));
// 			} catch(Exception e) {
//
// 			}
		}
		return split;
	}

	private static boolean isValid(char c) {
		if(c >= MINIMUM_NUMBER && c <= MAXIMUM_NUMBER) return true;
		return  isValid((int) c);
//		for(int i : EXCEPTIONS) if(i == (int) c) return true;
//		return false;
	}


	public static ArrayList<Double> getDoubleArray(String s) {
		ArrayList<Double> values = new ArrayList<Double>();
			try{


			String [] splits = s.split(",");

			s = s.replaceAll("\\(", "");
			s = s.replaceAll("\\)", "");

			s = s.replaceAll("\\[", "");
			s = s.replaceAll("\\]", "");
			s = s.replaceAll(" ", "");

			for(int i = 0 ; i < splits.length ; i++) {
				String st = splits[i];
				String [] groupSplits = st.split(":");
				if(groupSplits.length == 3) {
					String initialString = groupSplits[0];
					String incrementString = groupSplits[1];
					String endString = groupSplits[2];

					String [] initialPower = initialString.split("\\^");

					endString = endString.replace('^', '\0');
					endString = endString.replace('(', '\0');
					endString = endString.replace(')', '\0');
					double end = Double.valueOf(endString);


					if(initialPower.length == 2) {
						String power = initialPower[0];

						initialString = initialString.replaceAll(power, "");
						String [] initialValuesString = initialString.split(";");
						ArrayList<Double> initialValues = new ArrayList<Double>();
						for(String iv : initialValuesString) {
							iv = iv.replace('^', '\0');
							iv = iv.replace('(', '\0');
							iv = iv.replace(')', '\0');
							initialValues.add(Double.valueOf(iv));
						}
						power = power.replaceAll("^", "");

						double pow = Double.valueOf(power);
						double inc = Double.valueOf(incrementString);

						if(inc <= 0) return new ArrayList<Double>();

						for(Double v : initialValues) {
							for(double d = v ; d <= (end + 1E-12) ; d += inc) {
								double n = Math.pow(pow,d);

								values.add(n);
							}
						}

					} else if(incrementString.charAt(0) == '*') {
						String [] initialValuesString = initialString.split(";");
						ArrayList<Double> initialValues = new ArrayList<Double>();
						for(String iv : initialValuesString) {
							iv = iv.replace('(', '\0');
							iv = iv.replace(')', '\0');
							iv = iv.replace('^', '\0');
							initialValues.add(Double.valueOf(iv));
						}
						incrementString = incrementString.replaceAll("\\*", "");
						double inc = Double.valueOf(incrementString);
						for(Double v : initialValues) {
							for(double d = v ; d <= end + 1E-1 ; d *= inc) {
								double n = d;
								values.add(n);
							}

						}
					} else {
						String [] initialValuesString = initialString.split(";");
						ArrayList<Double> initialValues = new ArrayList<Double>();
						for(String iv : initialValuesString) {
							initialValues.add(Double.valueOf(iv));
						}
						double inc = Double.valueOf(incrementString);
						if(inc != 0) {
							for(Double v : initialValues) {
								for(double d = v ; d <= (end + 1E-12) ; d += inc) {
									double n = d;
									values.add(n);
								}
							}
						}
					}
				} else {
					try{
						values.add(Double.valueOf(groupSplits[0]));
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return values;
	}

	public static ArrayList<String> wrap(String s, int nchars) {
		ArrayList<String> list = new ArrayList<String>();
		if(s.length() < nchars) {
			list.add(s);
			return list;
		}

		int start = 0;
		int end = Math.min(s.length(),nchars);

		while(end != s.length()) {
			while(end >= start) {
				if(end == s.length()-1) {
					list.add(s.substring(start));
					return list;
				}
				if(s.charAt(end) == ' ') {
					list.add(s.substring(start,end));

					start = new Integer(end+1);
					end = Math.min(s.length(),end + nchars);
				}
				end--;
				if(end < start) {
					int newEnd = Math.min(s.length(),start + nchars-1);
					list.add(s.substring(start, newEnd) + "-");
					start = newEnd;
					end = Math.min(s.length(), start + nchars);
				}
			}
		}
		list.add(s.substring(start,end));
		return list;

	}
	public static void main(String[] args) throws Exception{
		File f = new File("C:/Projects/git/data/example-data/examples.em/Tempest/located_data/2322_1_Final.dat");


		long start = System.nanoTime();
		FileReader fr = new FileReader(f);
		Path p = Paths.get("c:/Projects/git/data/example-data/examples.em/Tempest/located_data/2322_1_Final.dat");
		Stream<String> lines = Files.lines(p);
		lines.forEachOrdered(s -> new Thread(new Runnable() {

			@Override
			public void run() {
				Text.split(s);
			}
		}).start());

		long end = System.nanoTime();
		System.out.println("Files Threaded Read Raw = " + ((double)end - (double)start)/(double)1E9);

		start = System.nanoTime();

		ArrayList<ArrayList<Double>> readAll = new ArrayList<>();

		BufferedReader in = new BufferedReader(new FileReader(f));
		String st = "";
		lines = in.lines();
		lines.forEachOrdered(s -> new Thread(new Runnable() {

			@Override
			public void run() {
				Text.split(s);
			}
		}).start());
		end = System.nanoTime();
		System.out.println("Buffered Read Raw = " + ((double)end - (double)start)/(double)1E9);




////		String s = arrayToString(1,2,3,4,5,6,7);
////		for(Double d : split(s)) {
////			System.out.println(d);
////		}
//				String s = "3.3\t0.45625188\t1000.00000\t999999.9\t1E9\t1.01E-3";
//		int nLines = 1000000;
////		System.out.println(Text.splitFastDouble(s).size());
//		long start = System.nanoTime();
////		for(int i = 0 ; i < nLines ; i++) Text.splitFastDouble(s);
//		long end = System.nanoTime();
////		System.out.println("SPLIT FAST = " + ((double)end - (double)start)/(double)1E9);
//
//		start = System.nanoTime();
//
//		for(int i = 0 ; i < nLines ; i++) Text.split(s,"\t");
//		end = System.nanoTime();
//		System.out.println("SPLIT TOKENIZER = " + ((double)end - (double)start)/(double)1E9);
//
//		start = System.nanoTime();
//		for(int i = 0 ; i < nLines ; i++) Text.split(s);
//		end = System.nanoTime();
//		System.out.println("SPLIT NORM = " + ((double)end - (double)start)/(double)1E9);
//
//		start = System.nanoTime();
//		for(int i = 0 ; i < nLines ; i++)  {
//			ArrayList<Double> array = new ArrayList<Double>();
//			for(String st : s.split("\t")) array .add(Double.valueOf(st));
//		}
//		end = System.nanoTime();
//		System.out.println("SPLIT STRING = " + ((double)end - (double)start)/(double)1E9);

	}
	public static String arrayToSpaceString(Object ... values) {
		if(values.length == 1) {
			if (values[0] instanceof ArrayList) {
				ArrayList list = (ArrayList) values[0];
				String s = "";
				for(int i = 0 ; i < list.size() ; i++) {
					s += list.get(i).toString() + ((i == list.size() - 1) ? "" : " ");
				}
				return s;
			}
		}

		String s = "";
		for(int i = 0 ; i < values.length ; i++) {
			s += values[i].toString() + ((i == values.length - 1) ? "" : " ");
		}
		return s;
	}
	public static String arrayFixedToTabbedString(Object [] values) {
		if(values.length == 1) {
			if (values[0] instanceof ArrayList) {
				ArrayList list = (ArrayList) values[0];
				String s = "";
				for(int i = 0 ; i < list.size() ; i++) {
					s += list.get(i).toString() + ((i == list.size() - 1) ? "" : "\t");
				}
				return s;
			}
		}

		String s = "";
		for(int i = 0 ; i < values.length ; i++) {
			s += values[i].toString() + ((i == values.length - 1) ? "" : " ");
		}
		return s;
	}
	public static String arrayToString(String delimeter, Object ... values) {
		String s = "";
		for(int i = 0 ; i < values.length ; i++) {
			s += values[i].toString() + ((i == values.length - 1) ? "" : delimeter);
		}
		return s;
	}
	public static String arrayToString(Number ... values) {
		String s = "";
		for(int i = 0 ; i < values.length ; i++) {
			s += values[i] + ((i == values.length - 1) ? "" : ",");
		}
		return s;
	}
	public static String arrayToString(Boolean ... values) {
		String s = "";
		for(int i = 0 ; i < values.length ; i++) {
			s += values[i] + ((i == values.length - 1) ? "" : ",");
		}
		return s;
	}



}