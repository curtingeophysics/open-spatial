package open.spatial.gui;

import org.apache.commons.cli.Option;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import open.spatial.crs.Projections;
import open.spatial.crs.ProjectionsSelectionNode;
import open.spatial.crs.WTKInstance;
import open.spatial.fonts.FontAwesome;

public class OpenSpatialProjectionSelectionNode extends BorderPane{
	static final Logger logger = LogManager.getLogger(OpenSpatialProjectionSelectionNode.class.getName());
	public ObjectProperty<WTKInstance> selectedWTKInstance = new SimpleObjectProperty<WTKInstance>();
	public final TextField projectionIDField = new TextField();

	public OpenSpatialProjectionSelectionNode() {
		GridPane gp2 = new GridPane();
		BorderPane projectionBox = new BorderPane();

		gp2.add(projectionBox,0,0,4,1);
		gp2.setHgap(10); //horizontal gap in pixels => that's what you are asking for
		gp2.setVgap(10); //vertical gap in pixels
		gp2.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid
		Button projectionButton = new Button("...", FontAwesome.COMPASS());
		projectionButton.setId("white");
		projectionBox.setCenter(projectionIDField);
		projectionIDField.setMaxWidth(Double.MAX_VALUE);
		projectionBox.setRight(projectionButton);
		projectionBox.setId("fileinput");
		projectionIDField.setId("clean");
		projectionIDField.setEditable(false);
		projectionButton.setOnAction(getProjectionAction());
		ComboBox<WTKInstance> favoriteProjections = new ComboBox<WTKInstance>(FXCollections.observableArrayList(Projections.favorites));
		favoriteProjections.getSelectionModel().selectedItemProperty().addListener(getWTKComboListener());
		favoriteProjections.getSelectionModel().selectFirst();
		favoriteProjections.setId("white");
		gp2.add(favoriteProjections,0,1,4,1);
		this.getStylesheets().add(OpenSpatialUI.class.getResource("simpledark.css").toExternalForm());
		this.setCenter(gp2);
	}
	public void setWTK(WTKInstance v) {
		projectionIDField.setText(v.getName() + " : " + v.id + " (" + v.getUnits() + ")");
		selectedWTKInstance.set(v);
		logger.debug("Setting WTK to : " + v.getName());
	}
	private ChangeListener<? super WTKInstance> getWTKComboListener() {
		return new ChangeListener<WTKInstance>() {

			@Override
			public void changed(ObservableValue<? extends WTKInstance> observable, WTKInstance oldValue, WTKInstance newValue) {
				projectionIDField.setText(newValue.getName() + " : " + newValue.id + " (" + newValue.getUnits() + ")");
				setWTK(newValue);
				
			}
		};
	}


	private EventHandler<ActionEvent> getProjectionAction() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				ProjectionsSelectionNode n = ProjectionsSelectionNode.show();
				n.selectedWTKInstance.addListener(new ChangeListener<WTKInstance>() {

					@Override
					public void changed(ObservableValue<? extends WTKInstance> observable, WTKInstance oldValue,
							WTKInstance newValue) {
						selectedWTKInstance.set(newValue);
						projectionIDField.setText(newValue.getName() + " : " + newValue.id + " (" + newValue.getUnits() + ")");
					}
				});
			}
		};
	}



}
