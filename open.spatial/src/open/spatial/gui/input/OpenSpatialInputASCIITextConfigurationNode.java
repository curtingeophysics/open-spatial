package open.spatial.gui.input;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import open.spatial.fonts.FontAwesome;

public class OpenSpatialInputASCIITextConfigurationNode extends BorderPane{
	static final Logger logger = LogManager.getLogger(OpenSpatialInputASCIITextConfigurationNode.class.getName());
	public final static String idSelect = "Select";
	public final static String idID = "ID";
	public final static String idX = "X/Lon";
	public final static String idY = "Y/Lat";
	public final static String idZ = "Z";
	public final static String idV = "Value";
	public final static String idL = "Line";


	private final VBox v = new VBox();
	public final TextField headerLinesText = new TextField();
	public final TextField delimeterText = new TextField();

	private final ObjectProperty<ArrayList<ComboBox<String>>> combos = new SimpleObjectProperty<ArrayList<ComboBox<String>>>();

	public ToggleButton tabButton = new ToggleButton("Tab");
	public ToggleButton spaceButton = new ToggleButton("Space");
	public ToggleButton commaButton = new ToggleButton("Comma");
	public ToggleButton semicolonButton = new ToggleButton("Semicolon");

	public final IntegerProperty icol = new SimpleIntegerProperty();
	public final IntegerProperty xcol = new SimpleIntegerProperty();
	public final IntegerProperty ycol = new SimpleIntegerProperty();
	public final IntegerProperty zcol = new SimpleIntegerProperty();
	public final IntegerProperty vcol = new SimpleIntegerProperty();
	public final IntegerProperty lcol = new SimpleIntegerProperty();

	public int getHeaderLines() {
		try {
			return Integer.valueOf(headerLinesText.getText());
			
		} catch(Exception e) {
			return 0;
		}
	}
	
	public void setComboIdentifiers() {
		icol.set(-1);
		xcol.set(-1);
		ycol.set(-1);
		zcol.set(-1);
		vcol.set(-1);
		lcol.set(-1);
		
		int col = 0;
		for(ComboBox<String> s : combos.get()) {
			int selectedIndex = s.getSelectionModel().getSelectedIndex();
			switch (selectedIndex) {
			case 0:  break; //select
			case 1:  icol.set(new Integer(col)); break; //id
			case 2:  xcol.set(new Integer(col)); break; //id
			case 3:  ycol.set(new Integer(col)); break; //id
			case 4:  zcol.set(new Integer(col)); break; //id
			case 5:  vcol.set(new Integer(col)); break; //id
			case 6:  lcol.set(new Integer(col)); break; //id
			

			default: logger.warn("Unknown Column!");	break;
			}
			col++;
		}
	}

	public ArrayList<ComboBox<String>> createComboSelections(int n) {
		ArrayList<ComboBox<String>> combos = new ArrayList<ComboBox<String>>();
		for(int i = 0 ; i < n ; i++) {
			ComboBox<String> c = new ComboBox<String>(FXCollections.observableArrayList(idSelect, idID,idX,idY,idZ,idV,idL));
			if(this.combos.get() != null) {
				if(this.combos.get().size() == n) {
					c.getSelectionModel().select(this.combos.get().get(i).getSelectionModel().getSelectedIndex());

				} else {
					if(i <= 3) {
						c.getSelectionModel().select(i+1);
					} else {
						c.getSelectionModel().select(0);
					}
				}
			} else {
				if(i <= 3) {
					c.getSelectionModel().select(i+1);
				} else {
					c.getSelectionModel().select(0);
				}
			}
			combos.add(c);
		}

		this.combos.setValue(combos);
		return combos;
	}


	BorderPane previewPane = new BorderPane();

	public OpenSpatialInputASCIITextConfigurationNode (){
		Label l = new Label("Text Configuration");
		setTop(l);
		headerLinesText.setPromptText("Number of Header Lines");

		GridPane gp =  new GridPane();
		Label headerLabel = new Label("",FontAwesome.SORT_NUMERIC_ASC());
		Label headerLabelText = new Label("Header: ");
		HBox hb = new HBox(headerLinesText,headerLabel);

		headerLinesText.setId("fileinput");
		delimeterText.setId("fileinput");
		semicolonButton.setId("toggle");
		commaButton.setId("toggle");
		spaceButton.setId("toggle");
		tabButton.setId("toggle");

		gp.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid
		gp.setVgap(10); //vertical gap in pixels



		delimeterText.setPromptText("Other Delimiter");

		Label delimeterLabel = new Label("Delimiters: ");
		HBox delimeterBox = new HBox(new VBox(new HBox(tabButton,spaceButton,commaButton,semicolonButton),delimeterText));
		gp.add(headerLabelText, 0, 0);
		gp.add(hb, 1, 0,3,1);
		gp.add(delimeterLabel, 0, 1);
		gp.add(delimeterBox, 1,   1,3,2);
//		delimeterBox.setId("clean");

		
		BorderPane bp1 = new BorderPane();
		bp1.setTop(gp);
		bp1.setCenter(previewPane);
		setCenter(bp1);
	}


	public ArrayList<String> getActiveDelimeters() {
		ArrayList<String> delimeters = new ArrayList<String>();
		if(commaButton.isSelected()) delimeters.add(",");
		if(tabButton.isSelected()) delimeters.add("\t");
		if(spaceButton.isSelected()) delimeters.add(" ");
		if(semicolonButton.isSelected()) delimeters.add(";");
		if(delimeterText.getText().length() > 0) delimeters.add(delimeterText.getText());
		return delimeters;
	}


	public void setPreview(GridPane gp) {
		ScrollPane sp = new ScrollPane(gp);
		sp.setMaxWidth(Double.MAX_VALUE);
		sp.setStyle("-fx-background-color:transparent;");
		previewPane.setCenter(sp);
	}

}
