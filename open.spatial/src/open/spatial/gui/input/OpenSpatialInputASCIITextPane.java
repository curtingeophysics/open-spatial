package open.spatial.gui.input;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class OpenSpatialInputASCIITextPane extends BorderPane{
	static final Logger logger = LogManager.getLogger(OpenSpatialInputASCIITextPane.class.getName());
	public TextArea asciiData = new TextArea();
	Stage s = new Stage();


	public OpenSpatialInputASCIITextPane () {
		setTop(new Label("Input Data"));
		setPadding(new Insets(10, 10, 10, 10));
		setCenter(asciiData);
		s.setScene(new Scene(this));
		s.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				s.hide();
			}
		});
		s.setTitle("Paste Text Here");
	}


	public void showPane() {

		s.show();

	}


	public static void parseText(File f, String text) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(f));
			out.write(text);
			out.close();
		} catch(Exception e) {
			logger.error(e);
		}
	}

}
