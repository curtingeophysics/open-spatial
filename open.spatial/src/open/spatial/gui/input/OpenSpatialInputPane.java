package open.spatial.gui.input;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.cli.Option;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.WindowEvent;
import open.spatial.OpenSpatial;
import open.spatial.crs.Projections;
import open.spatial.crs.WTKInstance;
import open.spatial.fonts.FontAwesome;
import open.spatial.formats.InputFormats;
import open.spatial.formats.KML;
import open.spatial.gui.OpenSpatialProjectionSelectionNode;
import open.spatial.gui.OpenSpatialUI;
import open.spatial.text.DataTable;
import open.spatial.utils.DateTimeUtilities;

public class OpenSpatialInputPane extends BorderPane{

	public final static int PREVIEW_LENGTH = 15;

	static final Logger logger = LogManager.getLogger(OpenSpatialInputPane.class.getName());
	public final TextField filePath = new TextField();
	public final ObjectProperty<Option> selectedOption = new SimpleObjectProperty<Option>();
	public OpenSpatialProjectionSelectionNode inputProjectionNode = new OpenSpatialProjectionSelectionNode();
	public OpenSpatial OS = OpenSpatialUI.OS;
	public OpenSpatialInputASCIITextPane textInputNode = new OpenSpatialInputASCIITextPane();
	public OpenSpatialInputASCIITextConfigurationNode textConfig = new OpenSpatialInputASCIITextConfigurationNode();

	//keeps a sample of the text for parsing and configuration
	public StringProperty textSample = new SimpleStringProperty();

	public ObjectProperty<ArrayList<File>> inputFiles = new SimpleObjectProperty<ArrayList<File>>();

	private final static String INPUT_FILE_ID = "input.file";
	private final static String INPUT_FILE_SELECTION = "input.selection";
	private final static String INPUT_N_HEADER = "input.nheader";
	private final static String INPUT_DELIMS = "input.delims";



	public OpenSpatialInputPane() {

		Label inputDataLabel = new Label("Input Data");
		filePath.setPromptText("From File");

		Label orLabel = new Label("or");
		Button pasteButton = new Button("Paste Text", FontAwesome.CLIPBOARD());
		BorderPane pathBox = new BorderPane();
		pasteButton.setOnAction(getTextPasteHandler());

		pathBox.setCenter(filePath);
		Button load = new Button("...", FontAwesome.FOLDER());
		load.setOnAction(getLoadAction());
		pathBox.setRight(load);
		load.setId("white");
		pathBox.setId("fileinput");
		filePath.setId("clean");
		GridPane gp = new GridPane();
		gp.add(pathBox,0,0,4,1);
		gp.add(orLabel,4,0,1,1);
		gp.add(pasteButton,5,0,2,1);
		gp.setHgap(10); //horizontal gap in pixels => that's what you are asking for
		gp.setVgap(10); //vertical gap in pixels
		gp.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid

		inputFiles.set(new ArrayList<File>());

		//*********************
		Label projectionLabel = new Label("Select Input Projection");
		textConfig.setMaxWidth(Double.MAX_VALUE);
		textConfig.setPrefSize(500, 800);
		VBox vb = new VBox(inputDataLabel,gp,projectionLabel,inputProjectionNode,textConfig);
		addDelimeterListeners();
		setCenter(vb);
		inputDataLabel.requestFocus();
		vb.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid

		selectedOption.addListener(new ChangeListener<Option>() {

			@Override
			public void changed(ObservableValue<? extends Option> observable, Option oldValue, Option newValue) {
				logger.debug("Selected input : " + newValue );
			}
		});
		filePath.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				inputFiles.set(parseFiles(newValue));

			}
		});
		textConfig.headerLinesText.textProperty().addListener( getHeaderListener());
		inputFiles.addListener(getInputFileListener());


		//load from history after listeners
		String lastOpt = OpenSpatial.properties.get(INPUT_FILE_SELECTION, "");
		String lastPath = OpenSpatial.properties.get(INPUT_FILE_ID,"");
		if(lastOpt.length() > 0 && lastPath.length() > 0) {
			for(Option o : InputFormats.extensions.keySet()) {
				if(o.getOpt().equals(lastOpt)) {
					selectedOption.set(o);
				}
			}
			filePath.setText(lastPath);
			logger.info("Loading from history: " + lastPath);
		}

		String nheadString = OpenSpatial.properties.get(INPUT_N_HEADER, "");
		String delimString  = OpenSpatial.properties.get(INPUT_DELIMS, "");
		if(nheadString.length() > 0) {
			textConfig.headerLinesText.setText(nheadString);
		}
		if(delimString.length() > 0) {
			String [] delims = delimString.split(":::");
			for(String s : delims) {

				if(s.equals(";")) textConfig.semicolonButton.setSelected(true);
				else if(s.equals("\t")) textConfig.tabButton.setSelected(true);
				else if(s.equals(" ")) textConfig.spaceButton.setSelected(true);
				else if(s.equals(",")) textConfig.commaButton.setSelected(true);
				else textConfig.delimeterText.setText(s);
			}
		}


	}

	private ChangeListener<? super ArrayList<File>> getInputFileListener() {
		return new ChangeListener<ArrayList<File>>() {

			@Override
			public void changed(ObservableValue<? extends ArrayList<File>> observable, ArrayList<File> oldValue,ArrayList<File> newValue) {
				setDataInput(newValue.get(0));
			}
		};
	}

	public void addDelimeterListeners() {
		this.textConfig.commaButton.selectedProperty().addListener(getDelimeterListener());
		this.textConfig.semicolonButton.selectedProperty().addListener(getDelimeterListener());
		this.textConfig.spaceButton.selectedProperty().addListener(getDelimeterListener());
		this.textConfig.tabButton.selectedProperty().addListener(getDelimeterListener());
		this.textConfig.delimeterText.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(inputFiles.get().size() > 0)
					setDataInput(inputFiles.get().get(0));
			}
		});

	}
	private ChangeListener<? super Boolean> getDelimeterListener() {
		return new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(inputFiles.get().size() > 0)
					setDataInput(inputFiles.get().get(0));
			}

		};
	}
	private ChangeListener<? super String> getHeaderListener() {
		return new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(inputFiles.get().size() > 0)
					setDataInput(inputFiles.get().get(0));
			}
		};
	}

	public ArrayList<DataTable> getDataTables() {
		ArrayList<DataTable> tables = new ArrayList<DataTable>();
		for (File f : inputFiles.get()) {
			logger.debug("Previewing: " + f.getAbsolutePath());
			ArrayList<String> delims = textConfig.getActiveDelimeters();
			int nHeader = this.textConfig.getHeaderLines();

			OpenSpatial.properties.store(INPUT_N_HEADER, ""+nHeader);
			String delimString = "";
			for(String s : delims) {
				delimString += (s + ":::");
			}
			OpenSpatial.properties.store(INPUT_DELIMS, ""+delimString);

			DataTable table;
			try {
				textConfig.setComboIdentifiers();
				if(textConfig.xcol.get() == -1) {
					logger.error("No Easting / Longitude Column Set");
					break;
				}
				if(textConfig.ycol.get() == -1) {
					logger.error("No Northing / Latitude Column Set");
					break;
				}

				table = DataTable.importDataTable(f, delims, true, Integer.MAX_VALUE, nHeader);
				table.xcol.set(textConfig.xcol.get());
				table.ycol.set(textConfig.ycol.get());
				if(textConfig.zcol.get() != -1) table.zcol.set(textConfig.zcol.get());
				if(textConfig.vcol.get() != -1) table.vcol.set(textConfig.vcol.get());
				if(textConfig.icol.get() != -1) table.idcol.set(textConfig.icol.get());
				if(textConfig.lcol.get() != -1) table.linecol.set(textConfig.lcol.get());


				if(textConfig.xcol.get() != -1) logger.info("...X-COL: " + (textConfig.xcol.get()+1));
				if(textConfig.ycol.get() != -1) logger.info("...Y-COL: " + (textConfig.ycol.get()+1));
				if(textConfig.zcol.get() != -1) logger.info("...Z-COL: " + (textConfig.zcol.get()+1));
				if(textConfig.icol.get() != -1) logger.info("...ID-COL: " + (textConfig.icol.get()+1));
				if(textConfig.lcol.get() != -1) logger.info("...LINE-COL: " + (textConfig.lcol.get()+1));



				table.setInputCRS(inputProjectionNode.selectedWTKInstance.get());

				tables.add(table);
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			}

		}
		return tables;
	}

	public boolean isDark = true;

	private void setDataInput(File file) {
		logger.debug("Previewing: " + file.getAbsolutePath());
		ArrayList<String> delims = textConfig.getActiveDelimeters();
		int nHeader = this.textConfig.getHeaderLines();

			try {

				DataTable table = null;

				if(selectedOption.get().getOpt() == OS.INPUT_ASCII_LATLON.getOpt() || selectedOption.get().getOpt() == OS.INPUT_ASCII_UTM.getOpt()) {
					table = DataTable.importDataTable(file, delims, true, PREVIEW_LENGTH, nHeader);
				} else {
					System.err.println("THIS OPTION");
					table = load(selectedOption.get(),file);
				}
				if(table != null) {
					int maxLength =  0;
					for(ArrayList<String> arr : table.values) {
		//				System.out.println(arr);
						maxLength = Math.max(maxLength, arr.size());
					}
					for(ArrayList<String> arr : table.header) {
		//				System.out.println(arr);
						maxLength = Math.max(maxLength, arr.size());
					}
					GridPane gp = new GridPane();

					int row = 0;
					ArrayList<ComboBox<String>> combos = this.textConfig.createComboSelections(maxLength);

					for(int i = 0 ; i < maxLength ; i++) {
						gp.add(combos.get(i), i+1,row);
						gp.add(new Label("Col " + (i+1)), i+1,row+1);

					}
					row++;
					row++;
//					gp.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;" + "-fx-border-width: 1;"+ " -fx-border-color: rgb(50,100,200);");
					for(ArrayList<String> arr : table.header) {
						gp.add(new Label("Header " + (row-1)),0,row);
						int col = 1;
						int nc = 0;
						for(String s : arr) {
							Label l = new Label("" + s);
							nc++;
							if(isDark) {
								l.setStyle("-fx-border-style: solid inside;" + "  -fx-border-color: rgba(50,50,50,0.5); -fx-background-color: rgb(40,40,40)");
							} else {
								l.setStyle("-fx-border-style: solid inside;" + "  -fx-border-color: rgba(150,150,150,0.5); -fx-background-color: rgb(242,242,242)");
							}
							l.setMaxWidth(Double.MAX_VALUE);
							gp.add(l,col++,row);
						}
						row++;
					}

					for(ArrayList<String> arr : table.values) {
						Label lab = new Label("Data " + (row-1 - table.header.size()));
						int nc = 0;
						lab.setMinWidth(60);
						gp.add(lab,0,row);
						int col = 1;

						for(String s : arr) {
							Label l = new Label("" + s);
							nc++;
							if(isDark) {
								l.setStyle("-fx-border-style: solid inside;" + " -fx-border-color: rgba(60,60,60,0.5); -fx-background-color:" + (nc%2 == 0 ? "rgb(20,20,20)" : "rgb(69,98,107)"));
							} else {
								l.setStyle("-fx-border-style: solid inside;" + " -fx-border-color: rgba(150,150,150,0.5); -fx-background-color:" + (nc%2 == 0 ? "rgb(255,255,255)" : "rgb(222,235,247)"));
							}

							l.setMaxWidth(Double.MAX_VALUE);
							gp.add(l,col++,row);
						}
						row++;
					}


					textConfig.setPreview(gp);
				}

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			}


	}



	private DataTable load(Option option, File file) {
		//INPUT FILE HANDLING
		String key = option.getOpt();
		ArrayList<ArrayList<Double>> points = new ArrayList<ArrayList<Double>>();
		if(key.equals(OS.INPUT_KML.getOpt())) {
			try {
				String wtkid = "EPSG:4326";
				WTKInstance wtkinstance = Projections.idMap.get(wtkid);
				inputProjectionNode.setWTK(wtkinstance);
				DataTable table = KML.loadTable(file.getAbsolutePath());
				return table;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;
//		else if(key.equals(OS.INPUT_ASCII_UTM.getOpt())) {
//			points = UTMASCII.loadUTMPoints(file.getAbsolutePath(),utmZoneNumber,utmZoneID,colid,colx,coly,colz,headerLines,false);
//		} else if(key.equals(OS.INPUT_ASCII_LATLON.getOpt())) {
//			points = LatLonASCII.loadLatLonPoints(file.getAbsolutePath(),colid,colx,coly,colz,headerLines,false);
//		} else if(key.equals(OS.INPUT_GPX.getOpt())) {
//			try {
//				ArrayList<Point> pts = GPXFormat.read(file.getAbsolutePath(),false);
//				for(Point p : pts) {
//					p.lon =
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			} break;
//		} else if(key.equals(OS.INPUT_COR.getOpt())) {
//			ArrayList<Point> pts = CORGPR.read(file.getAbsolutePath(),false);
//		} else if(key.equals(OS.INPUT_SGY.getOpt())) {
//			points = SEGY.read(file,false);
//		}
	}

	private ArrayList<File> parseFiles(String paths) {
		ArrayList<File> files = new ArrayList<File>();
		for(String s : paths.split(";")) {
			File f = new File(s);
			if(f.exists()) {
				if(f.isFile()) {
					files.add(f);
					logger.info("Selecting File: " + f.getAbsolutePath());
				}
			}

		}
		return files;
	}



	private EventHandler<ActionEvent> getTextPasteHandler() {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				textInputNode.showPane();
				textInputNode.s.setOnHidden(new EventHandler<WindowEvent>() {
					@Override
					public void handle(WindowEvent event) {
						String date = DateTimeUtilities.getCurrentTimeStamp();
						File f = new File("Temp_" + date + ".txt");
						OpenSpatialInputASCIITextPane.parseText(f,textInputNode.asciiData.getText());
						logger.info("Created Temporary File from pasted text: " + f.getAbsolutePath());
						ArrayList<File> files = new ArrayList<File>();
						files.add(f);
						inputFiles.set(files);
					}
				});
			}
		};
	}






	private EventHandler<ActionEvent> getLoadAction() {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fc = new FileChooser();
				String path = OpenSpatial.properties.get("file.previous");
				if(path != null) {
					File dir = new File(path);
					if(dir.exists()) {
						fc.setInitialDirectory(dir);
					}
				}
				for(ExtensionFilter e : InputFormats.extensionFilters.keySet()) {
					fc.getExtensionFilters().add(e);
				}
				try {
					Iterator<File> files = fc.showOpenMultipleDialog(null).iterator();
					ExtensionFilter ext = fc.getSelectedExtensionFilter();

					selectedOption.set(InputFormats.extensionFilters.get(ext));

					if(InputFormats.extensionFilters.get(ext).getId() == OS.INPUT_ASCII_LATLON.getId()) {
						WTKInstance wtk = Projections.idMap.get("EPSG:63266405");
						inputProjectionNode.selectedWTKInstance.set(wtk);
						inputProjectionNode.projectionIDField.setText(wtk.getName() + " : " + wtk.id + " (" + wtk.getUnits() + ")");
					}
					String paths = "";
					while(files.hasNext()) {
						File f = files.next();
						paths += f.getAbsolutePath();

						if(files.hasNext()) {
							paths += ";";
						}
					}
					filePath.setText(paths);
					logger.info("Saving in history: " + paths);
					OpenSpatial.properties.store(INPUT_FILE_ID, paths);
					OpenSpatial.properties.store(INPUT_FILE_SELECTION, InputFormats.extensionFilters.get(ext).getOpt());

				} catch(Exception e) {
					logger.warn("No Files");
				}
			}
		};


	}




}
