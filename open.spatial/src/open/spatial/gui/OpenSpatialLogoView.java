package open.spatial.gui;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class OpenSpatialLogoView extends BorderPane{
	
	public OpenSpatialLogoView() {
		 Image imag = new Image(getClass().getResourceAsStream("logo.png"));
	        ImageView logo = new ImageView(imag);
	        logo.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					try {
			            Desktop.getDesktop().browse(new URI("http://www.DigitalEarthLab.com"));
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        } catch (URISyntaxException e1) {
			            e1.printStackTrace();
			        }
				}

			});

	        setLeft(logo);
	        BorderPane bp = new BorderPane(new Label("  "));
	        bp.setBottom(new Separator());
	        setBottom(bp);
	}

}
