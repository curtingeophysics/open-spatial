package open.spatial.gui.processing;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.commons.cli.Option;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.controlsfx.control.ToggleSwitch;
import org.controlsfx.tools.Borders;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.Glow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import open.spatial.OpenSpatial;
import open.spatial.fonts.FontAwesome;
import open.spatial.formats.OpenSpatialExportFormats;
import open.spatial.formats.OpenSpatialFormat;
import open.spatial.gui.export.OpenSpatialExportNode;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.process.OpenSpatialProcessSet;
import open.spatial.process.OpenSpatialProcesses;

public class OpenSpatialProcessesNode extends BorderPane{

	static final Logger logger = LogManager.getLogger(OpenSpatialProcessesNode.class.getName());

	static Option MODIFIER_NEGATE_Z 	             = OpenSpatial.MODIFIER_NEGATE_Z 	             ;
//	static Option MODIFIER_HEADER 		             = OpenSpatial.MODIFIER_HEADER 		             ;
	static Option MODIFIER_DLAT 		             = OpenSpatial.MODIFIER_DLAT 		             ;
	static Option MODIFIER_DLON		             = OpenSpatial.MODIFIER_DLON		             ;

	static Option MODIFIER_LAT_MIN 	             = OpenSpatial.MODIFIER_LAT_MIN 	             ;
	static Option MODIFIER_LAT_MAX 	             = OpenSpatial.MODIFIER_LAT_MAX 	             ;
	static Option MODIFIER_LON_MIN 	             = OpenSpatial.MODIFIER_LON_MIN 	             ;
	static Option MODIFIER_LON_MAX 	             = OpenSpatial.MODIFIER_LON_MAX 	             ;
//	static Option MODIFIER_UTM 		             = OpenSpatial.MODIFIER_UTM 		             ;
	static Option MODIFIER_PTI 		             = OpenSpatial.MODIFIER_PTI 		             ;
	static Option MODIFIER_PTI_LINEAR 	             = OpenSpatial.MODIFIER_PTI_LINEAR 	             ;
	static Option MODIFIER_LINENUMBER 	             = OpenSpatial.MODIFIER_LINENUMBER 	             ;
	static Option MODIFIER_CUT_MIN 	             = OpenSpatial.MODIFIER_CUT_MIN 	             ;
	static Option MODIFIER_CUT_MAX 	             = OpenSpatial.MODIFIER_CUT_MAX 	             ;
//	static Option MODIFIER_COLUMN_ID 	             = OpenSpatial.MODIFIER_COLUMN_ID 	             ;
//	static Option MODIFIER_COLUMN_X 	             = OpenSpatial.MODIFIER_COLUMN_X 	             ;
//	static Option MODIFIER_COLUMN_Y 	             = OpenSpatial.MODIFIER_COLUMN_Y 	             ;
//	static Option MODIFIER_COLUMN_Z 	             = OpenSpatial.MODIFIER_COLUMN_Z 	             ;
	static Option MODIFIER_FORCE_UPDATE_NAMING      = OpenSpatial.MODIFIER_FORCE_UPDATE_NAMING      ;
	static Option MODIFIER_PRINT 		             = OpenSpatial.MODIFIER_PRINT 		             ;
	static Option MODIFIER_SKIP_NTH 	             = OpenSpatial.MODIFIER_SKIP_NTH 	             ;
	static Option MODIFIER_REVERSE 	             = OpenSpatial.MODIFIER_REVERSE 	             ;
	static Option MODIFIER_SPLIT_OUTPUT 	         = OpenSpatial.MODIFIER_SPLIT_OUTPUT 	         ;
	static Option MODIFIER_MAP_ELEVATIONS_DEM_LIDAR = OpenSpatial.MODIFIER_MAP_ELEVATIONS_DEM_LIDAR ;
	static Option MODIFIER_MAP_ELEVATIONS_SRTM      = OpenSpatial.MODIFIER_MAP_ELEVATIONS_SRTM      ;
	private GridPane gp;
	private static final String IDLE_BUTTON_STYLE = "-fx-background-color: white;";
	private static final String HOVERED_BUTTON_STYLE = "-fx-background-color: -fx-shadow-highlight-color, -fx-outer-border, -fx-inner-border, -fx-body-color;";

	public ArrayList<OpenSpatialProcess> activeProcesses = new ArrayList<OpenSpatialProcess>();


	public Button run = new Button("Run",FontAwesome.PLAY());

	public OpenSpatialExportNode exportnode;

	public OpenSpatialProcessesNode () {
		this.setTop(new Label("Processes"));
		GridPane gp = new GridPane();
		this.gp = gp;

		BorderPane baseToolbarTop = new BorderPane();
		VBox baseToolbarRight = new VBox();
		float fontsize = 1.5f;
		ArrayList<Button> options = new ArrayList<Button>();
		int nth = 0;



//		Button interpolate = new Button("Interpolate",FontAwesome.ELLIPSIS_H(fontsize));
//		Button clip = new Button("Clipping",FontAwesome.CROP(fontsize));
//		Button ordering = new Button("Ordering",FontAwesome.SORT_AMOUNT_DESC(fontsize));
//		Button mapping = new Button("Elevation",FontAwesome.GLOBE(fontsize));

//		interpolate.setContentDisplay(ContentDisplay.TOP);
//		clip.setContentDisplay(ContentDisplay.TOP);
//
//		mapping.setContentDisplay(ContentDisplay.TOP);


		Button previewText = new Button("Preview ASCII",FontAwesome.FILE_TEXT());
		Button previewKML = new Button("Preview Google Earth",FontAwesome.GLOBE());

		Glow g = new Glow(0.1);
//		blend.setBottomInput(color);
		run.setEffect(g); // apply effect on scene root

		run.setStyle("-fx-font-size:14");
//		run.setStyle(IDLE_BUTTON_STYLE);
//		run.setOnMouseEntered(e -> run.setStyle(HOVERED_BUTTON_STYLE));
//		run.setOnMouseExited(e -> run.setStyle(IDLE_BUTTON_STYLE));

//		Button add = new Button("Add Process", FontAwesome.PLUS_CIRCLE());
//
//		FontAwesome.restyle(add.getGraphic(), "white", FontAwesome.DEFAULT_SIZE);
//		add.setStyle("-fx-text-fill: white; -fx-background-color: "+Theme.COLOR_01+"; ");
//		add.setAlignment(Pos.BASELINE_LEFT);
//		add.setOnAction(addProcessEventHander());

//		Button export = new Button("Add Export", FontAwesome.DOWNLOAD());
//		FontAwesome.restyle(export.getGraphic(), "white", FontAwesome.DEFAULT_SIZE);
//		export.setStyle("-fx-text-fill: white; -fx-background-color: rgb(50,250,160); ");
//		export.setAlignment(Pos.BASELINE_LEFT);
////		export.setOnAction(addExportEventHander());
//		GridPane buttons = new GridPane();
////		buttons.add(add, 0, 0,4,1);
//		buttons.add(export, 4, 0,4,1);


//		baseToolbar.setRight(run);
		run.setPrefHeight(60);
		run.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(run, Priority.ALWAYS);
		baseToolbarTop.setCenter(new HBox(run));
		HBox tools = new HBox();

		for(OpenSpatialProcessSet set : OpenSpatialProcesses.processSets) {
			Button b = new Button(set.getName(), set.getIcon(fontsize));
//			b.setStyle("-fx-text-fill: white; -fx-background-color: "+Theme.colours[nth++]+"; ");
			ContextMenu contextMenu = new ContextMenu();
			b.setPrefWidth(100);
			for (OpenSpatialProcess p : set.getProcesses()) {
				MenuItem item = new MenuItem(p.getName());
				contextMenu.getItems().add(item);
				item.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						addProcess(p);
					}


				});
			}
//			FontAwesome.restyle(b.getGraphic(), "rgb(230,230,230)", FontAwesome.DEFAULT_SIZE);
			b.setContextMenu(contextMenu);
			b.setContentDisplay(ContentDisplay.TOP);
			b.setOnMousePressed(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					contextMenu.show(b, Side.BOTTOM, 0, 0);
				}
			});
			tools.getChildren().add(b);
		}
//
//		Button exportButton = new Button("Export",FontAwesome.DOWNLOAD(fontsize));
//		ContextMenu contextMenu = new ContextMenu();
//		exportButton.setPrefWidth(100);
//
//		for(int id : OpenSpatialExportFormats.formats.keySet()) {
//			OpenSpatialFormat f = OpenSpatialExportFormats.formats.get(id);
//			MenuItem item = new MenuItem(f.getName());
//			contextMenu.getItems().add(item);
//			item.setOnAction(new EventHandler<ActionEvent>() {
//
//				@Override
//				public void handle(ActionEvent event) {
//					OpenSpatialProcess osp = f.getExportProcess();
//
//					addProcess(osp);
//
//				}
//
//
//			});
//		}
//		exportButton.setContentDisplay(ContentDisplay.TOP);
//		exportButton.setOnMousePressed(new EventHandler<Event>() {
//
//			@Override
//			public void handle(Event event) {
//				contextMenu.show(exportButton, Side.BOTTOM, 0, 0);
//			}
//		});
//		exportButton.setContextMenu(contextMenu);
//		tools.getChildren().add(exportButton);
		baseToolbarTop.setTop(tools);
//		baseToolbarRight.getChildren().addAll(previewText,previewKML,run);


		refreshPane(gp);



		BorderPane processExportPane = new BorderPane();
		processExportPane.setCenter(gp);
//		HBox buttonPane = new HBox(add,export);
//		processExportPane.setBottom(buttonPane);
		this.setCenter(processExportPane);
		this.setBottom(new VBox(baseToolbarTop));
//		this.setRight(baseToolbarRight);
		GridPane.setFillWidth(this, true);
		GridPane.setFillHeight(this, true);
//		gp.setStyle("-fx-background-color: rgb(255, 255, 255);");

		this.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid
	}



	private void refreshPane(GridPane gp) {
		gp.getChildren().clear();
		int row = 0;
		for(OpenSpatialProcess o : activeProcesses) {
			setOptionPane(gp,row, row,o);
			row++;
		}
//
//		gp.add(buttons,0,row,7,1);
////		gp.add(export,0,row+1,8,1);
//
//		GridPane.setFillWidth(add, true);
//		add.setMaxWidth(Double.MAX_VALUE);
//		add.setPrefWidth(Double.MAX_VALUE);
//		GridPane.setFillWidth(export, true);
//		export.setMaxWidth(Double.MAX_VALUE);
//		export.setPrefWidth(Double.MAX_VALUE);
////		GridPane.setFillHeight(add, true);
	}





	private void addProcess(OpenSpatialProcess p) {
		logger.info("Add Process " + p.getName() + "(" + p.getID() + ")");
		OpenSpatialProcess cloned = OpenSpatialProcesses.create(p.getID());

		activeProcesses.add(cloned);
		refreshPane(gp);
	}





	private void setOptionPane(GridPane gp, int currentProcessNumber, int row, OpenSpatialProcess p) {

		ArrayList<String> labels = new ArrayList<>();
		LinkedHashMap<String,String> desc = new LinkedHashMap<String, String>();

		int nth = 0;
		int selected = 0;
//		for(Option o : options) {
//			labels.add(o.getOpt() + " (" + o.getDescription() +")");
//			if(p.activeOption.get() != null)
//			if(p.activeOption.get().getOpt().contains(o.getOpt())) {
//				selected = nth;
//			}
//			nth++;
////			desc.put(o.getOpt(), o.getDescription());
//
//		}


		ObservableList<Node> childrens = gp.getChildren();
		for(Node node : childrens) {
		    if(gp.getRowIndex(node) == row && gp.getColumnIndex(node) == 3) {
		        gp.getChildren().remove(node);
		        break;
		    }
		 }
//		gp.add(new Label("",FontAwesome.CROSSHAIRS()), 3, row);

//		optionsCombo

		ToggleSwitch enable = new ToggleSwitch ();
		ToggleSwitch open = new ToggleSwitch ();

		enable.setId(p.isExport() ? "toggleupdatedexport" : "toggleupdatedprocess");
		open.setId(p.isExport() ? "toggleupdatedexport" : "toggleupdatedprocess");
		enable.selectedProperty().bindBidirectional(p.enabled);
		open.selectedProperty().bindBidirectional(p.openOnComplete);

		Button  delete = new Button ("",FontAwesome.REMOVE());

		Button up = new Button ("",FontAwesome.ARROW_CIRCLE_O_UP());
		Button down = new Button ("",FontAwesome.ARROW_CIRCLE_O_DOWN());
//		Button config = new Button ("",FontAwesome.COG());
//		enable.setId("white");
		FontAwesome.restyle(delete.getGraphic(), "rgb(220,30,30)", FontAwesome.DEFAULT_SIZE);
		FontAwesome.restyle(up.getGraphic(), "rgb(100,100,100)", FontAwesome.DEFAULT_SIZE);
		FontAwesome.restyle(down.getGraphic(), "rgb(100,100,100)", FontAwesome.DEFAULT_SIZE);
//		FontAwesome.restyle(config.getGraphic(), "rgb(100,100,100)", FontAwesome.DEFAULT_SIZE);

//		gp.add(new Label(p.getFunctionName()), 2, row);
		Node n = p.createNode();
		BorderPane controls = new BorderPane();
		GridPane toggles = new GridPane();
		toggles.add(new Label("Enabled"), 0, 0);
		toggles.add(enable, 1, 0);
		toggles.add(new Label("Open with default"), 0, 1);
		toggles.add(open, 1, 1);
		controls.setLeft(toggles);
		controls.setRight(new HBox(up,down,delete));
		VBox internal = new VBox(controls,n);
		Node leftWithBorder = Borders.wrap(internal).lineBorder().title(p.getFunctionName()).color(Color.BLACK).buildAll();
//		gp.add(leftWithBorder, 6, row);
//		gp.add(delete, 3, row);
//		gp.add(up, 4, row);
//		gp.add(down, 5, row);

		leftWithBorder.prefHeight(400);

		gp.add(leftWithBorder, 1, row);

		delete.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				activeProcesses.remove(row);
				refreshPane(gp);
			}
		});
		up.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				OpenSpatialProcess proc = activeProcesses.remove(row);
				activeProcesses.add(Math.max(0, row-1), proc);
				refreshPane(gp);
			}
		});
		down.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				OpenSpatialProcess proc = activeProcesses.remove(row);
				activeProcesses.add(Math.min(row+1,activeProcesses.size()),proc);
				refreshPane(gp);
			}
		});

	}



	private ArrayList<Option> createExports() {
		ArrayList<Option> options = new ArrayList<Option>();
		options.addAll(OpenSpatial.outputs);
		return options;
	}



	public static ArrayList<Option> createProcesses() {
		ArrayList<Option> options = new ArrayList<Option>();
		options.add(MODIFIER_NEGATE_Z);
		options.add(MODIFIER_CUT_MIN); //ids
		options.add(MODIFIER_CUT_MAX);
		options.add(MODIFIER_LAT_MIN);
		options.add(MODIFIER_LAT_MAX);
		options.add(MODIFIER_LON_MIN);
		options.add(MODIFIER_LON_MAX);
		options.add(MODIFIER_PTI);
		options.add(MODIFIER_PTI_LINEAR);
		options.add(MODIFIER_LINENUMBER);
		options.add(MODIFIER_FORCE_UPDATE_NAMING);
		options.add(MODIFIER_PRINT);
		options.add(MODIFIER_SKIP_NTH);
		options.add(MODIFIER_REVERSE);
		options.add(MODIFIER_SPLIT_OUTPUT);
		options.add(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR);
		options.add(MODIFIER_MAP_ELEVATIONS_SRTM);

		return options;
	}




}
