package open.spatial.gui;

public class Theme {
	public final static String COLOR_01 = "rgb(30,180,230)";
	public final static String COLOR_02 = "rgb(0,76,162)";
	public final static String COLOR_03 = "rgb(140,81,165)";
	public final static String COLOR_04 = "rgb(203,94,152)";
	public final static String COLOR_05 = "rgb(244,123,138)";
	public final static String COLOR_06 = "rgb(255,163,126)";
	public final static String COLOR_07 = "rgb(255,210,133)";
	public final static String COLOR_08 = "rgb(255,255,165)";
	
	public final static  String [] colours = {COLOR_01,COLOR_02,COLOR_03,COLOR_04,COLOR_05,COLOR_06,COLOR_07,COLOR_08};
}
