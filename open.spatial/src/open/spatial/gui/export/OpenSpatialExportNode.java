package open.spatial.gui.export;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import open.spatial.OpenSpatial;
import open.spatial.fonts.FontAwesome;
import open.spatial.gui.OpenSpatialCustomProjectionNode;
import open.spatial.gui.OpenSpatialProjectionSelectionNode;

public class OpenSpatialExportNode extends BorderPane{
	public final TextField outputDirectory = new TextField();
	static final Logger logger = LogManager.getLogger(OpenSpatialExportNode.class.getName());
	public OpenSpatialProjectionSelectionNode outputProjectionNode = new OpenSpatialProjectionSelectionNode();
	public OpenSpatialCustomProjectionNode openSpatialCustomProjectionNode = new OpenSpatialCustomProjectionNode();
	public OpenSpatialExportNode() {
		this.setTop(new Label("Export"));
		this.outputDirectory.setPromptText("Output Directory");
		this.outputDirectory.setId("clean");

		BorderPane pathBox = new BorderPane();
		pathBox.setCenter(outputDirectory);
		Button load = new Button("...", FontAwesome.FOLDER());
		load.setOnAction(getLoadAction());
		pathBox.setRight(load);
		load.setId("white");
		pathBox.setId("fileinput");
		outputDirectory.setId("clean");
		GridPane gp = new GridPane();
		gp.add(pathBox,0,0,4,1);
		VBox vb = new VBox(gp,new Label("Output Projection Mode"),outputProjectionNode,new Label("Mine Site Custom Projection"),openSpatialCustomProjectionNode);
		
		
		
		this.setCenter(vb);
	}

	private EventHandler<ActionEvent> getLoadAction() {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser dc = new DirectoryChooser();
				String path = OpenSpatial.properties.get("outputdir.previous");
				if(path != null) {
					File dir = new File(path);
					if(dir.exists()) {
						dc.setInitialDirectory(dir);
					}
				}

				try {
					File dir = dc.showDialog(null);
					outputDirectory.setText(dir.getAbsolutePath());
					OpenSpatial.properties.store("outputdir.previous", dir.getAbsolutePath());
				} catch(Exception e) {
					logger.warn("No Directory");
				}
			}
		};

	}

}
