package open.spatial.gui;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import open.spatial.crs.CoordinateTransformation;

public class OpenSpatialCustomProjectionNode extends BorderPane{
	static final Logger logger = LogManager.getLogger(OpenSpatialCustomProjectionNode.class.getName());
	CheckBox enableCustomProjectionOverride = new CheckBox("Custom Projection Override (Minesite/Local Projection)");
	
	Label global = new Label("Global Points");
	Label local = new Label("Local/Minesite Points");
	Label gx1 = new Label("x1");
	Label gx2 = new Label("x2");
	Label gx3 = new Label("x3");
	Label lx1 = new Label("x1");
	Label lx2 = new Label("x2");
	Label lx3 = new Label("x3");
	Label gy1 = new Label("y1");
	Label gy2 = new Label("y2");
	Label gy3 = new Label("y3");
	Label ly1 = new Label("y1");
	Label ly2 = new Label("y2");
	Label ly3 = new Label("y3");
	
	public final TextField globalX1 = new TextField();
	public final TextField globalY1 = new TextField();
	public final TextField globalX2 = new TextField();
	public final TextField globalY2 = new TextField();
	public final TextField globalX3 = new TextField();
	public final TextField globalY3 = new TextField();

	public final TextField localX1 = new TextField();
	public final TextField localY1 = new TextField();
	public final TextField localX2 = new TextField();
	public final TextField localY2 = new TextField();
	public final TextField localX3 = new TextField();
	public final TextField localY3 = new TextField();
	
	public OpenSpatialCustomProjectionNode() {

		
		try {
			new File("Projections/Custom/").mkdirs();
			File defaultProj = new File("Projections/Custom/default.proj");
			CoordinateTransformation tf = CoordinateTransformation.loadTransformation(defaultProj);
			localX1.setText("" + tf.localX1);
			localX2.setText("" + tf.localX2);
			localX3.setText("" + tf.localX3);
			globalX1.setText("" + tf.globalX1);
			globalX2.setText("" + tf.globalX2);
			globalX3.setText("" + tf.globalX3);
			
			localY1.setText("" + tf.localY1);
			localY2.setText("" + tf.localY2);
			localY3.setText("" + tf.localY3);
			globalY1.setText("" + tf.globalY1);
			globalY2.setText("" + tf.globalY2);
			globalY3.setText("" + tf.globalY3);
			
		} catch(Exception e) {
			
		}
		
		GridPane gp = new GridPane();
		int row = 0;
		gp.add(global, 1, row);
		gp.add(local, 5, row);		
		row++;
		gp.add(gx1, 	 0, row);
		gp.add(globalX1, 1, row);
		gp.add(gy1, 	 2, row);
		gp.add(globalY1, 3, row);
		gp.add(lx1, 	 4, row);
		gp.add(localX1,  5, row);
		gp.add(ly1,	 	 6, row);
		gp.add(localY1,  7, row);
		
		row++;
		gp.add(gx2, 	 0, row);
		gp.add(globalX2, 1, row);
		gp.add(gy2, 	 2, row);
		gp.add(globalY2, 3, row);
		gp.add(lx2, 	 4, row);
		gp.add(localX2,  5, row);
		gp.add(ly2,	 	 6, row);
		gp.add(localY2,  7, row);
		
		row++;
		gp.add(gx3, 	 0, row);
		gp.add(globalX3, 1, row);
		gp.add(gy3, 	 2, row);
		gp.add(globalY3, 3, row);
		gp.add(lx3, 	 4, row);
		gp.add(localX3,  5, row);
		gp.add(ly3,	 	 6, row);
		gp.add(localY3,  7, row);
		
		
		
		enableCustomProjectionOverride.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue) {
					setCenter(gp);
				} else {
					getChildren().remove(gp);
				}
			}
		});
		
		
		
		this.getStylesheets().add(OpenSpatialUI.class.getResource("simpledark.css").toExternalForm());
		this.setTop(enableCustomProjectionOverride);
		
	}
	
	public CoordinateTransformation getTransform() {
		
		
		double localX1 = Double.valueOf(this.localX1.getText()); double localY1 = Double.valueOf(this.localY1.getText());
		double localX2 = Double.valueOf(this.localX2.getText()); double localY2 = Double.valueOf(this.localY2.getText());
		double localX3 = Double.valueOf(this.localX3.getText()); double localY3 = Double.valueOf(this.localY3.getText());
		
		double globalX1 = Double.valueOf(this.globalX1.getText()); double globalY1 = Double.valueOf(this.globalY1.getText());
		double globalX2 = Double.valueOf(this.globalX2.getText()); double globalY2 = Double.valueOf(this.globalY2.getText());
		double globalX3 = Double.valueOf(this.globalX3.getText()); double globalY3 = Double.valueOf(this.globalY3.getText());
		
     
		CoordinateTransformation tf = new CoordinateTransformation(localX1, localY1, localX2, localY2, localX3, localY3, globalX1, globalY1, globalX2, globalY2, globalX3, globalY3);
		return tf;
	}

	public boolean getEnabled() { 
		return enableCustomProjectionOverride.isSelected();
	}






}
