package open.spatial.gui;

import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import open.spatial.Console;
import open.spatial.OpenSpatial;
import open.spatial.gui.export.OpenSpatialExportNode;
import open.spatial.gui.input.OpenSpatialInputPane;
import open.spatial.gui.processing.OpenSpatialProcessesNode;
import open.spatial.process.OpenSpatialProcess;
import open.spatial.text.DataTable;

public class OpenSpatialUI extends Application {
	static final Logger logger = LogManager.getLogger(OpenSpatialUI.class.getName());
	public final static OpenSpatial OS = new OpenSpatial();
	public OpenSpatialInputPane inputPane = new OpenSpatialInputPane();
	public OpenSpatialProcessesNode processPane = new OpenSpatialProcessesNode();
	public OpenSpatialExportNode exportNode = new OpenSpatialExportNode();
	Console c = new Console();
     TextArea log = new TextArea();
	@Override
	public void start(Stage primaryStage) throws Exception {
		OpenSpatial.initLogger();
	    log.setStyle("-fx-control-inner-background:#000000; -fx-font-family: Consolas; -fx-highlight-fill: rgb(63,191,239); -fx-highlight-text-fill: #000000; -fx-text-fill: rgb(63,191,239);");
	    log.setEditable(false);
	    Console.setTextArea(log);

		Scene s = createScene();
		processPane.run.setOnAction(e -> run());
		processPane.exportnode = exportNode;


		primaryStage.setScene(s);
		primaryStage.setHeight(950);
		primaryStage.setWidth(1050);
		primaryStage.show();
		primaryStage.setTitle("Digital Earth Lab� Open Spatial v" + OpenSpatial.VERSION);

	}
	private void run() {
		logger.info("Starting Open Spatial UI Process");
		ArrayList<DataTable> tables = inputPane.getDataTables();
		for(DataTable t : tables) {
			t.setOutputCRS(exportNode.outputProjectionNode.selectedWTKInstance.get());
			boolean customEnabled = exportNode.openSpatialCustomProjectionNode.getEnabled();
			if(customEnabled) {
				t.setCustomLocalCoordinateProjectionEnabled(customEnabled);
				t.setCustomLocalCoordinateProjection(exportNode.openSpatialCustomProjectionNode.getTransform());
			}
		}
		logger.info("...Import " + tables.size() + " files.");
		ArrayList<OpenSpatialProcess> processes = processPane.activeProcesses;
		for(DataTable table : tables) {
			DataTable st = table;
			for(OpenSpatialProcess p : processes) {
				try {
					st = p.run(st);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	private Scene createScene() {

		BorderPane pane = new BorderPane();
		pane.setTop(new OpenSpatialLogoView());

		SplitPane spm = new SplitPane(inputPane,new ScrollPane(new VBox(exportNode,processPane)));
//		GridPane gridpane = new GridPane();
//		gridpane.add(inputPane, 0, 0);
//		gridpane.add(, 1, 0);
////		gridpane.add(, 2, 0);
//        ColumnConstraints col1 = new ColumnConstraints();
//        col1.setPercentWidth(50);
//        ColumnConstraints col2 = new ColumnConstraints();
//        col2.setPercentWidth(50);

//        gridpane.getColumnConstraints().addAll(col1,col2);


		pane.setCenter(spm);



	    BorderPane logp = new BorderPane();
	    logp.setTop(new Label("Console Log"));
	    logp.setCenter(log);

		SplitPane sp = new SplitPane(pane,logp);
		sp.setOrientation(Orientation.VERTICAL);
        sp.setDividerPositions(0.90);
		Scene s = new Scene(sp);
//		JMetro jMetro = new JMetro(Style.LIGHT);
//		jMetro.applyTheme(s);



		s.getStylesheets().add(OpenSpatialUI.class.getResource("simpledark.css").toExternalForm());

		return s;
	}

	public static void main(String[] args) {
		OpenSpatialUI.launch();
	}

}
