package open.spatial;

import java.util.ArrayList;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class Transect {
	public final ArrayList<UTMRef> positions;
	public final ArrayList<Double> offsets = new ArrayList<Double>();
	public double totalLength = 0;
	private PolynomialSplineFunction eastingOffsetInterpolator;
	private PolynomialSplineFunction northingOffsetInterpolator;
	private PolynomialSplineFunction eastingOffsetInterpolatorLin;
	private PolynomialSplineFunction northingOffsetInterpolatorLin;
	public Transect(ArrayList<UTMRef> positions) {
		this.positions = positions;
		init();
	}

	public void init() {
		offsets.clear();
		double totalLength = 0;
		UTMRef lastPoint = positions.get(0);
		offsets.add(new Double(totalLength));
		for(int i = 1 ; i < positions.size() ; i++) {
			UTMRef thisPoint = positions.get(i);
			double dx = (thisPoint.getEasting() - lastPoint.getEasting());
			double dy = (thisPoint.getNorthing() - lastPoint.getNorthing());
			double offset = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
			totalLength += offset;
			offsets.add(new Double(totalLength));
			lastPoint = thisPoint;


		}

		ArrayList<Double> offsetsCut = new ArrayList<>();
		ArrayList<Double> xCut = new ArrayList<>();
		ArrayList<Double> yCut = new ArrayList<>();
		ArrayList<Double> zCut = new ArrayList<>();

		double lastOffset = Double.NaN;
		for(int i = 0 ; i < offsets.size() ; i++) {
			double currentOffset = offsets.get(i);
			if(currentOffset != lastOffset) {
				offsetsCut.add(offsets.get(i));
				xCut.add(positions.get(i).getEasting());
				yCut.add(positions.get(i).getNorthing());
//				zCut.add(positions.get(i).z);
			}
			lastOffset = currentOffset;
		}

		double xs [] = new double[offsetsCut.size()];
		double ys [] = new double[offsetsCut.size()];
		double offsets [] = new double[offsetsCut.size()];
		for(int i = 0 ; i < offsetsCut.size() ; i++) {
			xs[i] = xCut.get(i);
			ys[i] = yCut.get(i);
			offsets[i] = offsetsCut.get(i);
		}



		 SplineInterpolator interp = new SplineInterpolator();
		 LinearInterpolator linterp = new LinearInterpolator();

		 PolynomialSplineFunction eastingOffsetInterpolator = interp.interpolate(offsets, xs);
		 PolynomialSplineFunction northingOffsetInterpolator = interp.interpolate(offsets, ys);
		 PolynomialSplineFunction eastingOffsetInterpolatorLin = linterp.interpolate(offsets, xs);
		 PolynomialSplineFunction northingOffsetInterpolatorLin = linterp.interpolate(offsets, ys);
		 this.eastingOffsetInterpolator = eastingOffsetInterpolator;
		 this.northingOffsetInterpolator = northingOffsetInterpolator;
		 this.eastingOffsetInterpolatorLin = eastingOffsetInterpolatorLin;
		 this.northingOffsetInterpolatorLin = northingOffsetInterpolatorLin;
		this.totalLength  = totalLength;
	}

	public Point interpolateOffset(double offset, char latZone, int lngZone) {
		for(int i = 0 ; i < offsets.size() ; i++) {
			double o = offsets.get(i);

			if(o == offset) {
				UTMRef p = positions.get(i);
				LatLng ptl = p.toLatLng();
				return new Point(ptl.getLatitude(), ptl.getLongitude());

			} else if(o > offset){
				double e = eastingOffsetInterpolator.value(offset);
				double n = northingOffsetInterpolator.value(offset);

				UTMRef p = new UTMRef(lngZone, latZone, e, n);
				LatLng ptl = p.toLatLng();
				return new Point(ptl.getLatitude(), ptl.getLongitude());
			}
		}
		return null;

	}
	public Point interpolateOffsetLinear(double offset, char latZone, int lngZone) {
		for(int i = 0 ; i < offsets.size() ; i++) {
			double o = offsets.get(i);

			if(o == offset) {
				UTMRef p = positions.get(i);
				LatLng ptl = p.toLatLng();
				return new Point(ptl.getLatitude(), ptl.getLongitude());

			} else if(o > offset){
				double e = eastingOffsetInterpolatorLin.value(offset);
				double n = northingOffsetInterpolatorLin.value(offset);

				UTMRef p = new UTMRef(lngZone, latZone, e, n);
				LatLng ptl = p.toLatLng();
				return new Point(ptl.getLatitude(), ptl.getLongitude());
			}
		}
		return null;

	}
	private Point interpolate(double offset, double offset1, double offset2, UTMRef p1, UTMRef p2, char latZone, int lngZone) {
		double perc = (offset - offset1)/(offset2 - offset1);
		double dx = p2.getEasting() - p1.getEasting();
		double dy = p2.getNorthing() - p1.getNorthing();
		double x = p1.getEasting() + dx * perc;
		double y = p1.getNorthing() + dy * perc;
		UTMRef ref = new UTMRef(lngZone,latZone,x,y);
		LatLng ptl = ref.toLatLng();
		return new Point(ptl.getLatitude(), ptl.getLongitude());
	}

//	public void print() {
//		for(int i = 0 ; i < offsets.size() ; i++) {
//			System.out.println(i + "\t" + offsets.get(i) + "\t" + positions.get(i).getX() + "\t" + positions.get(i).getY());
//		}
//	}


}
