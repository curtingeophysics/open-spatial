package open.spatial.crs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SpatialReferenceWTKDownloader {

	public final static String ROOT_ADDRESS_EPSG = "https://spatialreference.org/ref/epsg/";
	public final static int MAX_PAGE = 200;

	private final static ArrayList<String> ids = new ArrayList<String>();


	public final static void downloadEPSG(String address, String id, String name) throws Exception{
		String url = ROOT_ADDRESS_EPSG + id.split(":")[1] + "/esriwkt/";
		String outPath = "Projections/EPSG_WTK.txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outPath),true));

		URL epsgurl = new URL(url);
		BufferedReader in = new BufferedReader(new InputStreamReader(epsgurl.openStream()));
		String wtkString = "";
		String inputLine;
        while ((inputLine = in.readLine()) != null) {
        	wtkString += inputLine;
        }
        in.close();
        wtkString.replaceAll("\n", "").replaceAll("\t", "");
		out.write(id + "\t" + name + ">>>" + wtkString);
		out.newLine();
		out.close();
	}


	public static void main(String[] args) throws Exception{

		BufferedReader wtkin = new BufferedReader(new FileReader(new File("Projections/EPSG_WTK.txt")));
		String line = "";
		while((line = wtkin.readLine()) != null) {
			ids.add(line.split("\t")[0]);
		}
		wtkin.close();

		 int page = 1;


		 for(page = 1 ; page < MAX_PAGE ; page++) {
			 try {
				 runEPSGURLPage(page);
			 } catch(Exception e) {
				 try {
					 System.err.println("ATTEMPT 2 ERROR: " +e.getMessage());
					 runEPSGURLPage(page);
				 } catch(Exception e2) {
					 try {
						 System.err.println("ATTEMPT 3 ERROR: " +e.getMessage());
						 runEPSGURLPage(page);
					 } catch(Exception e3) {
						 e.printStackTrace();
					 }
				 }
			 }
		 }
//		System.out.println(content);
//
//
//		BufferedReader in = new BufferedReader(new FileReader(new File("AEM")));
//		BufferedWriter out = new BufferedWriter(new FileWriter(new File("AEM_private")));
//		String line = "";
//
//		boolean first = true;
//		String [] fline = {};
//
//		ArrayList<Integer> ids = new ArrayList();
//
//		while((line = in.readLine()) != null) {
//			if(first) {
//				out.write(line);
//				fline = new String(line).split("\t");
//				out.newLine();
//				first = false;
//			} else {
//			String [] split = line.split("\t");
//			for(int i = 0 ; i < split.length ; i++) {
//				int id = Integer.valueOf(split[1]);
//				String name = (split[2]);
//				String path = (split[18]);
//				if(path.toUpperCase().contains("MAGIX")) {
//					out.write(line);
//					out.newLine();
//				} else {
//					System.out.println(path);
//					if(ids.contains(id)) {
//
//					} else {
//						ids.add(id);
//						try {
//							File fout = new File("E:/Data/DMP/" + name + "/" + id + ".zip");
//							if(!fout.exists())
//						FileUtils.copyURLToFile(
//
//								  new URL(path),
//								  fout,
//
//								  Integer.MAX_VALUE,
//								  Integer.MAX_VALUE);
//						} catch(Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}
//
////				if(split.length != 22) {
////					System.out.println(i + ": " + split[i]);
////				}
//			}
//			}
//
//		}
//		in.close();
//		out.close();
//
	}


	private static void runEPSGURLPage(int page) throws Exception{
		URL epsgurl = new URL(ROOT_ADDRESS_EPSG + "?page=" + page);
	 	BufferedReader in = new BufferedReader(new InputStreamReader(epsgurl.openStream()));
	 	String inputLine;
        while ((inputLine = in.readLine()) != null) {
        	int count = 0;
        	if(inputLine.contains("<li>")) {
        		String address = inputLine.split("\"")[1];
        		String id = inputLine.split(">")[2].split("<")[0];
        		String name = inputLine.split(": ")[1].split("<")[0];
        		if(!ids.contains(id)) {
	        		ids.add(id);
	        		downloadEPSG(address,id,name);
	        		System.out.println("page = " + page + ": " + address +"\t" + id + "\t" + name);
        		}
        		count++;
        	}
        	if(count == 0) {
        		page = MAX_PAGE;
        	}
        }
    in.close();
	}


}
