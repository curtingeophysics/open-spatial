package open.spatial.crs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import open.spatial.gui.OpenSpatialUI;

public class ProjectionsSelectionNode extends BorderPane{
	static final Logger logger = LogManager.getLogger(ProjectionsSelectionNode.class.getName());
	public ObjectProperty<WTKInstance> selectedWTKInstance = new SimpleObjectProperty<WTKInstance>();
	private TableView<WTKInstance> table = new TableView();
	private TableColumn<WTKInstance, String> id = new TableColumn<WTKInstance, String>("id");
	private TableColumn<WTKInstance, String> name = new TableColumn<WTKInstance, String>("name");
	private TableColumn<WTKInstance, String> units = new TableColumn<WTKInstance, String>("units");
	public TextField search = new TextField();
	public TextArea wtkText = new TextArea();
	public BooleanProperty isActive = new SimpleBooleanProperty(true);
	public Button ok = new Button("OK");

	public ProjectionsSelectionNode() {
		table.getColumns().addAll(id,name,units);
		id.setCellValueFactory(new PropertyValueFactory<WTKInstance,String>("id"));
		name.setCellValueFactory(new PropertyValueFactory<WTKInstance,String>("name"));
		units.setCellValueFactory(new PropertyValueFactory<WTKInstance,String>("units"));
		HBox sb = new HBox(new Label("Search:\t"),search);

		sb.setPadding(new Insets(10, 10, 10, 10)); //margins around the whole grid
		search.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				setSearch(newValue);
			}
		});
		setTop(sb);
		setCenter(table);

		ok.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				isActive.set(false);
			}
		});
		setSearch("");
		table.getSelectionModel().selectedItemProperty()
	        .addListener(new ChangeListener<WTKInstance>() {
	          public void changed(ObservableValue<? extends WTKInstance> observable,WTKInstance oldValue, WTKInstance newValue) {



	        	  if(newValue != null) {
	        		  wtkText.setText(newValue.wtk);
	        		  logger.info("Selected Instance: " + newValue);
	        		  selectedWTKInstance.set(newValue);
	        	  } else {

	        	  }
	          }
	        });
		table.setOnKeyPressed(new EventHandler<KeyEvent>()  {
	        @Override
	        public void handle(KeyEvent ke)   {
	            if (ke.getCode().equals(KeyCode.ENTER))        {
	                isActive.set(false);
	            }
	        }
		});
		wtkText.setEditable(false);
		wtkText.setWrapText(true);
		wtkText.setStyle("-fx-control-inner-background:#000000; -fx-font-family: Consolas; -fx-highlight-fill: rgb(63,191,239); -fx-highlight-text-fill: #000000; -fx-text-fill: rgb(63,191,239);");
		VBox b = new VBox(wtkText,ok);
		HBox.setHgrow(ok, Priority.ALWAYS);
		ok.setMaxWidth(Double.MAX_VALUE);
		setBottom(b);
		this.getStylesheets().add(OpenSpatialUI.class.getResource("simpledark.css").toExternalForm());

	}



	public final static ProjectionsSelectionNode show() {
		Stage s = new Stage();
		ProjectionsSelectionNode n = new ProjectionsSelectionNode();
		n.isActive.set(true);
		s.setScene(new Scene(n));
		s.show();
		s.setWidth(800);
		s.setHeight(600);
		s.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						s.close();
					}
				});
			}
		});
		n.isActive.addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				s.close();
			}
		});
		return n;

	}

	private boolean isActive() {
		return isActive.get();
	}



	private void setSearch(String s) {
		table.getItems().clear();
		if(s.length() == 0) {
			for(WTKInstance w : Projections.nameMap.values()) {
				table.getItems().add(w);
			}
		} else {
			for(WTKInstance w : Projections.nameMap.values()) {
				if(w.isValid(s)){
					table.getItems().add(w);
				}
			}
		}

	}


	public static void main(String[] args) {
	
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				ProjectionsSelectionNode n = show();							
			}
		});

	}
}
