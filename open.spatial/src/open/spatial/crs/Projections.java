package open.spatial.crs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import open.spatial.OpenSpatial;

public class Projections {
	static final Logger logger = LogManager.getLogger(Projections.class.getName());

	public final static LinkedHashMap<String, WTKInstance> nameMap = new LinkedHashMap<String, WTKInstance>();
	public final static LinkedHashMap<String, WTKInstance> idMap = loadProjections();

	public static ArrayList<WTKInstance> favorites = loadFavorites();


	public static LinkedHashMap<String, WTKInstance> loadProjections() {
		OpenSpatial.initLogger();
		LinkedHashMap<String, WTKInstance> map =new LinkedHashMap<String, WTKInstance>();
		String path = "Projections/EPSG_WTK.txt";
		logger.info("Loading Projections from "+path);
		int valid = 0;
		int invalid = 0;
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(path)));

			String line = "";
			while((line = in.readLine()) !=null) {
				String name = "";
				String id = "";
				try {

					String [] split = line.replaceAll(">>>", "\t").split("\t");
					id = split[0];
					name = split[1];
					String wtk = split[2];
					CoordinateReferenceSystem ref = CRS.parseWKT(wtk); //ensure it parses

					WTKInstance instance = new WTKInstance(id, name, wtk);
					instance.setReferenceSystem(ref);

					map.put(id, instance);
					nameMap.put(name, instance);
					valid++;
				} catch(Exception e2) {
					invalid++;
				}

			}
			in.close();
		} catch(Exception e) {
			e.printStackTrace();

		}
		logger.info(nameMap.size() + " projections loaded. (" + invalid + " projections not loaded)");
		return map;

	}
	private static ArrayList<WTKInstance> loadFavorites() {
		ArrayList<WTKInstance> list = new ArrayList<WTKInstance>();

		try {
			String path = "Projections/wtk_favorites.txt";
			logger.info("Loading Projection Favorites from "+path);
			if(new File(path).exists()) {
				BufferedReader in = new BufferedReader(new FileReader(new File(path)));
				String line = "";
				while((line = in.readLine()) != null) {
					try {
						list.add(idMap.get(line));
					} catch(Exception e2) {
						logger.error(e2);
					}
				}
				in.close();
			} else {
				new File(path).createNewFile();
			}
		} catch(Exception e) {
			logger.error(e);
		}
		return list;
	}
	public static void main(String[] args) {
		OpenSpatial.initLogger();
//		System.out.println(nameMap.size());
	}


}
