package open.spatial.crs;

import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class WTKTest {

	public static void main(String[] args) throws FactoryException {
		String wtk = "GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]]";
		CoordinateReferenceSystem ref = CRS.parseWKT(wtk); //ensure it parses

		WTKInstance instance = new WTKInstance("TEST", "TEST", wtk);
		instance.setReferenceSystem(ref);
		System.out.println(ref);
//		System.out.println(instance.isValid(wtk));
	}

}
