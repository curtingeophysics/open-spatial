package open.spatial.crs;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CoordinateTransformation {
	static final Logger logger = LogManager.getLogger(CoordinateTransformation.class.getName());
	private AffineTransform transform;

	private final static double EPSILON = 1E-15;
	
	public final double localX1, localY1; //local pt1
	public final double localX2, localY2; //local pt2
	public final double localX3, localY3; //local pt3
	public final double globalX1, globalY1; //global pt1
	public final double globalX2, globalY2; //global pt2
	public final double globalX3, globalY3; //global pt3

	//for saving and loading to java properties file
	private final static String LX1 = "LOCAL_X1";
	private final static String LX2 = "LOCAL_X2";
	private final static String LX3 = "LOCAL_X3";
	private final static String LY1 = "LOCAL_Y1";
	private final static String LY2 = "LOCAL_Y2";
	private final static String LY3 = "LOCAL_Y3";
	private final static String GX1 = "GLOBAL_X1";
	private final static String GX2 = "GLOBAL_X2";
	private final static String GX3 = "GLOBAL_X3";
	private final static String GY1 = "GLOBAL_Y1";
	private final static String GY2 = "GLOBAL_Y2";
	private final static String GY3 = "GLOBAL_Y3";


	public CoordinateTransformation( double localX1, double localY1,
	        double localX2, double localY2,
	        double localX3, double localY3,
	        double globalX1, double globalY1,
	        double globalX2, double globalY2,
	        double globalX3, double globalY3){

			this.localX1 = localX1;
			this.localY1 = localY1;
			this.localX2 = localX2;
			this.localY2 = localY2;
			this.localX3 = localX3;
			this.localY3 = localY3;
			this.globalX1 = globalX1;
			this.globalY1 = globalY1;
			this.globalX2 = globalX2;
			this.globalY2 = globalY2;
			this.globalX3 = globalX3;
			this.globalY3 = globalY3;
			this.transform = deriveAffineTransform(localX1, localY1, localX2, localY2, localX3, localY3, globalX1, globalY1, globalX2, globalY2, globalX3, globalY3);

	}
	public CoordinateTransformation( double localX1, double localY1,
	        double localX2, double localY2,
	        double globalX1, double globalY1,
	        double globalX2, double globalY2){
			//assume linear transformatioon
			double localX3 = localX1;
			double localY3 = localX2;
			double globalX3 = globalX1;
			double globalY3 = globalX2;

			this.localX1 = localX1;
			this.localY1 = localY1;
			this.localX2 = localX2;
			this.localY2 = localY2;
			this.localX3 = localX3;
			this.localY3 = localY3;
			this.globalX1 = globalX1;
			this.globalY1 = globalY1;
			this.globalX2 = globalX2;
			this.globalY2 = globalY2;
			this.globalX3 = globalX3;
			this.globalY3 = globalY3;


			try {
				this.transform = deriveAffineTransform(localX1, localY1, localX2, localY2, localX3, localY3, globalX1, globalY1, globalX2, globalY2, globalX3, globalY3);
			} catch(Exception e) {
				this.transform = deriveAffineTransform(localX1+EPSILON, localY1, localX2, localY2, localX3+EPSILON, localY3, globalX1, globalY1, globalX2+EPSILON, globalY2, globalX3+EPSILON, globalY3);
			}
	}

	public static void saveTransformation(CoordinateTransformation t, File f) {
		logger.info("Saving Coordinate transformation to file: " + f.getAbsolutePath());
	    try {
	        Properties props = new Properties();
	        props.setProperty(LX1, ""+t.localX1);
	        props.setProperty(LX2, ""+t.localX2);
	        props.setProperty(LX3, ""+t.localX3);
	        props.setProperty(LY1, ""+t.localY1);
	        props.setProperty(LY2, ""+t.localY2);
	        props.setProperty(LY3, ""+t.localY3);
	        props.setProperty(GX1, ""+t.globalX1);
	        props.setProperty(GX2, ""+t.globalX2);
	        props.setProperty(GX3, ""+t.globalX3);
	        props.setProperty(GY1, ""+t.globalY1);
	        props.setProperty(GY2, ""+t.globalY2);
	        props.setProperty(GY3, ""+t.globalY3);
	        OutputStream out = new FileOutputStream( f );
	        props.store(out, "Automatically generated Open Spatial coordinate transform file");
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	    logger.info("...Saved!");
	}
	public static CoordinateTransformation loadTransformation(File f) { logger.info("Loading Coordinate transformation from file: " + f.getAbsolutePath());
		Properties props = new Properties();
	    try {
	        InputStream is = new FileInputStream( f );
	        props.load( is );
	    }  catch ( Exception e ) {
	    	logger.error(e);
	    	logger.error("Cannot load coordinate transform file: " + f.getAbsolutePath());
	    }
		double localX1 =  Double.valueOf(props.get(LX1).toString());
		double localY1 =  Double.valueOf(props.get(LY1).toString());
		double localX2 =  Double.valueOf(props.get(LX2).toString());
		double localY2 =  Double.valueOf(props.get(LY2).toString());
		double localX3 =  Double.valueOf(props.get(LX3).toString());
		double localY3 =  Double.valueOf(props.get(LY3).toString());
		double globalX1 =  Double.valueOf(props.get(GX1).toString());
		double globalY1 =  Double.valueOf(props.get(GY1).toString());
		double globalX2 =  Double.valueOf(props.get(GX2).toString());
		double globalY2 =  Double.valueOf(props.get(GY2).toString());
		double globalX3 =  Double.valueOf(props.get(GX3).toString());
		double globalY3 =  Double.valueOf(props.get(GY3).toString());
		logger.info("...Loaded Successfully!");
		return new CoordinateTransformation(localX1, localY1, localX2, localY2, localX3, localY3, globalX1, globalY1, globalX2, globalY2, globalX3, globalY3);
	}

	public double [] localToGlobal(double x, double y) {
		Point2D pin = new Point2D.Double(x,y);
		Point2D pout = new Point2D.Double(0,0);
		transform.transform(pin, pout);
		return new double [] {pout.getX(),pout.getY()};
	}
	public double [] globalToLocal(double x, double y) {
		Point2D pin = new Point2D.Double(x,y);
		Point2D pout = new Point2D.Double(Double.NaN,Double.NaN);
		try {
			transform.inverseTransform(pin, pout);
		} catch (NoninvertibleTransformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new double [] {pout.getX(),pout.getY()};
	}

	 private static AffineTransform deriveAffineTransform(
		        double localX1, double localY1,
		        double localX2, double localY2,
		        double localX3, double localY3,
		        double globalX1, double globalY1,
		        double globalX2, double globalY2,
		        double globalX3, double globalY3) {

		    double[][] oldData = { {localX1, localX2, localX3}, {localY1, localY2, localY3}, {1, 1, 1} };
		    RealMatrix oldMatrix = MatrixUtils.createRealMatrix(oldData);

		    double[][] newData = { {globalX1, globalX2, globalX3}, {globalY1, globalY2, globalY3} };
		    RealMatrix newMatrix = MatrixUtils.createRealMatrix(newData);

		    RealMatrix inverseOld = new LUDecomposition(oldMatrix).getSolver().getInverse();
		    RealMatrix transformationMatrix = newMatrix.multiply(inverseOld);

		    double m00 = transformationMatrix.getEntry(0, 0);
		    double m01 = transformationMatrix.getEntry(0, 1);
		    double m02 = transformationMatrix.getEntry(0, 2);
		    double m10 = transformationMatrix.getEntry(1, 0);
		    double m11 = transformationMatrix.getEntry(1, 1);
		    double m12 = transformationMatrix.getEntry(1, 2);

		    return new AffineTransform(m00, m10, m01, m11, m02, m12);
		}


	 public static void main(String[] args) {

			double localX1 = 0; double localY1 = 0;
	        double localX2 = 1; double localY2= 1;

	        double globalX1 = -10; double globalY1 = -10;
	        double globalX2= 5; double globalY2 = 5;

	        CoordinateTransformation transform =  new CoordinateTransformation(localX1, localY1, localX2, localY2, globalX1, globalY1, globalX2, globalY2);
	        double [] pt = transform.localToGlobal(0.5, 0.0);
	        System.out.println(pt[0] + "\t" + pt[1]);
	 }


}
