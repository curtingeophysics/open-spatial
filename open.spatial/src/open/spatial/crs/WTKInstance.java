package open.spatial.crs;

import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class WTKInstance {
	public final String id;
	public final String name;
	public final String wtk;
	public CoordinateReferenceSystem ref;
	public WTKInstance(String id, String name, String wtk) {
		this.id = id;
		this.name = name;
		this.wtk = wtk;
	}

	public void setReferenceSystem(CoordinateReferenceSystem ref) {
		this.ref = ref;
	}

	public String  getId() {
		return id;
	}
	public String  getName() {
		return name;
	}
	public String  getUnits() {
		String s = ref.getCoordinateSystem().getAxis(0).getUnit().getSymbol();
		if(s == null) {
			return ref.getCoordinateSystem().getAxis(0).getUnit().getName();
		}
		return s;
	}
	@Override
	public String toString() {
		return id + ":\t" + getName() + "\t(" + getUnits() + ")";
	}

	public boolean isValid(String s) {
		if(id.toUpperCase().contains(s.toUpperCase())) return true;
		if(name.toUpperCase().contains(s.toUpperCase())) return true;
		String st = ref.getCoordinateSystem().getAxis(0).getUnit().getSymbol();
		if(st == null) {
			st = ref.getCoordinateSystem().getAxis(0).getUnit().getName();
		}
		if(st != null) {
			if(st.toUpperCase().contains(s.toUpperCase())) return true;
		}
		return false;
	}


}
