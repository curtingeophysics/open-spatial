package open.spatial.crs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.geotools.geometry.iso.io.wkt.Coordinate;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import open.spatial.OpenSpatial;
import open.spatial.Text;
import open.spatial.formats.CORGPR;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;




public class CRSConverter {
	static final Logger logger = LogManager.getLogger(CRSConverter.class.getName());

	public final static String WGS84 = "GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]]";

	public static void main(String[] args) throws Exception {
	//	Geometry source = new

		String source = getWTKString(new File("Projections/WKT/GA94_52SWTK"));
		String target = getWTKString(new File("Projections/WKT/WGS84_Lat_Lon_Global"));

		
		final CoordinateReferenceSystem targetCRS = CRS.parseWKT(target);
		final CoordinateReferenceSystem sourceCRS = CRS.parseWKT(source);
		Coordinate coord = new Coordinate(100, 100);
		Collection<Coordinate> pts = new ArrayList<Coordinate>();
		pts.add(coord);

		
		MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS,true);


		BufferedReader in = new BufferedReader(new FileReader("ExampleData/YOM/Seismic_Lambert"));

		String line = in.readLine(); //header
		double minE = -1;
		double maxE = -1;
		double minN = -1;
		double maxN = -1;

		while((line = in.readLine()) != null) {
			ArrayList<Double> vals = Text.split(line);
			int traceID = vals.get(0).intValue();
			double e = vals.get(1);
			double n = vals.get(2);
			double z = 0;
			double [] srcPts = {e,n,z};
			int srcOff = 0;
			double [] dstPts = {0,0,0};
			int dstOff = 0;
			int numPts = 1;
			trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);

			System.out.println(traceID + "\t" +  dstPts[0] + "\t" + dstPts[1] + "\t" + e  + "\t" + n) ;
		}





//		MultiPoint [] p = GeometryFactory.toMultiPointArray(pts);
//	    // Creation of the MathTransform associated to the reprojection
//
//	    MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS, true);
//	    // Geometry reprojection
//	    Geometry geoPrj;
//	    if (!trans.isIdentity()) {
//	        geoPrj = JTS.transform(p[0].getEnvelope(), trans);
//	    } else {
//	        geoPrj = sourceGeometry;
//	    }
	}



	public static void printPoints(ArrayList<Point> pts, File fout, File wtk) throws Exception {
		logger.info("Writing Custom WTK ASCII File: " + fout.getAbsolutePath());
		BufferedWriter out = new BufferedWriter(new FileWriter(fout));
		out.write(("P,X,Y,Z"));
		out.newLine();

		String target = getWTKString(wtk);
		final CoordinateReferenceSystem targetCRS = CRS.parseWKT(target);
		final CoordinateReferenceSystem sourceCRS = CRS.parseWKT(WGS84);
		MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS);
		logger.info("Using target reference system");
		logger.info(target);

		for(Point p : pts) {
			LatLng pl = new LatLng(p.lat, p.lon);
			double x = pl.getLongitude();
			double y = pl.getLatitude();
			double z = p.z;
			double [] srcPts = {x,y,z};
			int srcOff = 0;
			double [] dstPts = {0,0,0};
			int dstOff = 0;
			int numPts = 1;
			trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);
			out.write(p.name +"," + dstPts[0] + "," + dstPts[1] +"," + dstPts[2]);
			out.newLine();
		}
		out.close();

}


	public static ArrayList<Point> loadPoints(String absolutePath,String pathWTK, int colid,int colx,int coly, int colz, int nHeaderLines, boolean preview) {
		OpenSpatial.initLogger();

		File f = new File(absolutePath);
		ArrayList<Point> points = new ArrayList<>();
		try {
			logger.info("Loading Custom WTK Point File..." + absolutePath);

			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = "";
			for(int i = 0 ; i < nHeaderLines ; i++) {
				logger.info("Skipping Header Line "+(i+1)+": " + in.readLine()); //skip header
			}


			String source = getWTKString(new File(pathWTK));

			logger.info("Using reference system");
			logger.info(source);

			final CoordinateReferenceSystem targetCRS = CRS.parseWKT(WGS84);
			final CoordinateReferenceSystem sourceCRS = CRS.parseWKT(source);
			MathTransform trans = CRS.findMathTransform(sourceCRS, targetCRS);


			while((line = in.readLine()) !=null) {
				String sanitized = line.replaceAll(" ", ",").replaceAll("\t", ",");
				String [] split = sanitized.split(",");

				String id = (colid == -1) ? "Unknown" : split[colid];
				double x = Double.valueOf(split[colx]);
				double y = Double.valueOf(split[coly]);
				double z = (colz == -1 || colz >= split.length) ? 0.0 : Double.valueOf(split[colz]);


				ArrayList<Double> vals = Text.split(line);
				int traceID = vals.get(0).intValue();
				double [] srcPts = {x,y,z};
				int srcOff = 0;
				double [] dstPts = {0,0,0};
				int dstOff = 0;
				int numPts = 1;
				trans.transform(srcPts, srcOff, dstPts, dstOff, numPts);
				Point p = new Point(dstPts[1],dstPts[0]);
//				System.out.println(srcPts[0] + "\t" + srcPts[1]);
				p.z = dstPts[2];
				p.name = id;
				points.add(p);
				if(preview) {
					in.close();
					return points;
				}
			}

			logger.info("Loaded " + points.size() + " points.");

			in.close();
		} catch(Exception e ) {
			e.printStackTrace();
		}

		return points;
	}

	public static String getWTKString(File f) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(f));
			String line = "";
			String wtk = "";
			while((line = in.readLine())!=null) {
				wtk += line;
			}
			return wtk;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
