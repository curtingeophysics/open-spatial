package open.spatial;


import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import open.spatial.log.TextAreaAppender;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

public class OpenSpatialTable extends Application{
	static final Logger logger = LogManager.getLogger(OpenSpatialTable.class.getName());
	private TextArea areaData = new TextArea();
	private TextArea areaOut = new TextArea();

	private TextField zoneField = new TextField("50");
	private TextField zoneLatField = new TextField("J");
	private TextField xField = new TextField("1");
	private TextField yField = new TextField("2");
	private OpenSpatial os = new OpenSpatial();

	public LinkedHashMap<Option, CheckBox> checkboxes = new LinkedHashMap<>();
	public LinkedHashMap<Option, ArrayList<TextField>> textfields = new LinkedHashMap<>();
	File outputDir = new File("./output/");

	ComboBox<String> fileFormatOptions = getFileOptionsCombo();

	private final TextArea loggingView = new TextArea();

	@Override
	public void start(Stage stage) throws Exception {

//	        final Label label = new Label("Paste X Y (Z) data");
//	        label.setFont(new Font("Arial", 20));
			setupConsole();
	        areaData.setEditable(true);
	        areaOut.setEditable(false);

	        BorderPane bp = new BorderPane();
//	        bp.setTop(label);

	        GridPane gp = new GridPane();
	        BorderPane optionsPane = new BorderPane();
	        Image imag = new Image(getClass().getResourceAsStream("logo.png"));
	        ImageView logo = new ImageView(imag);
	        logo.setOnMouseClicked(new EventHandler<Event>() {

				@Override
				public void handle(Event event) {
					try {
			            Desktop.getDesktop().browse(new URI("http://www.DigitalEarthLab.com"));
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        } catch (URISyntaxException e1) {
			            e1.printStackTrace();
			        }
				}

			});
	        optionsPane.setTop(new VBox(new Label(""),logo,new Label("")));
	        optionsPane.setCenter(new ScrollPane(gp));


	        BorderPane input = new BorderPane();
	        BorderPane output = new BorderPane();
	        input.setCenter(areaData);
	        output.setCenter(areaOut);
	        Label inputLabel = new Label("Input Data (Files/Folders/E N Z/Lat Lon Z)");
	        Label outputLabel = new Label("Output");
	        inputLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");
	        outputLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");

	        input.setTop(inputLabel);
	        output.setTop(outputLabel);

	        SplitPane sptop = new SplitPane(input,output);
	        SplitPane boxes = new SplitPane(sptop,loggingView);
	        boxes.setOrientation(Orientation.VERTICAL);
	        SplitPane sp = new SplitPane(optionsPane, boxes);

	        Label zone = new Label("Zone (e.g., 50)");
	        Label zoneChar = new Label("Zone Char (eg., J)");
	        Label colX = new Label("ColX/Lon");
	        Label colY = new Label("ColY/Lat");


	        Button inputDataDir = new Button("... Select Input Folder");
	        Button inputDataFile = new Button("... Select Input File(s)");
	        Button parse = new Button("Parse File/Dir");
	        Button latLonToUTM = new Button("Parse Lat/Lon");
	        Button UTMToLatLon = new Button("Parse UTM");
	        latLonToUTM.setMaxWidth(Double.MAX_VALUE);
	        UTMToLatLon.setMaxWidth(Double.MAX_VALUE);
	        parse.setMaxWidth(Double.MAX_VALUE);
	        inputDataDir.setMaxWidth(Double.MAX_VALUE);
	        inputDataFile.setMaxWidth(Double.MAX_VALUE);
	        fileFormatOptions.setMaxWidth(Double.MAX_VALUE);

	        int row = 0;
	        gp.add(inputDataDir,0,row,1,1);
	        gp.add(inputDataFile,1,row,1,1);
	        gp.add(fileFormatOptions,2,row++,2,1);

	        gp.add(parse,0,row++,4,1);
	        gp.add(latLonToUTM,0,row++,4,1);
	        gp.add(UTMToLatLon,0,row++,4,1);

	        Label modifiersLabel = new Label("Set Modifiers");
	        Label setOutputLabel = new Label("Set Outputs");
	        modifiersLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");
	        setOutputLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");
	        gp.add(new Label(""),0,row++,4,1);
	        gp.add(modifiersLabel,0,row++,4,1);

	        latLonToUTM.setPrefHeight(50);
	        UTMToLatLon.setPrefHeight(50);
	        parse.setPrefHeight(50);
	        for(Option o : os.modifiers) {

	        	Label l2 = new Label(o.getDescription());
	        	gp.add(l2, 0, row++,5,1);

	        	CheckBox b = new CheckBox();
	        	Label l = new Label(o.getOpt());

	        	gp.add(b, 0, row);
	        	gp.add(l, 1, row);
	        	ArrayList<TextField> text = new ArrayList<>();
	        	for(int i = 0 ; i < o.getArgs() ; i++) {
	        	 TextField f = new TextField();
	        	 if(OpenSpatial.defaultModifierValues.containsKey(o)) {
	        		 f.setText(OpenSpatial.defaultModifierValues.get(o).get(i));
	        	 }
	        	 gp.add(f, 2+i, row);
	        	 text.add(f);
	        	}
	        	row++;
	        	checkboxes.put(o, b);
	        	textfields.put(o, text);

	        }

	        gp.add(new Label(""),0,row++,4,1);
	        gp.add(setOutputLabel,0,row++,4,1);
	        for(Option o : os.outputs) {

	        	Label l2 = new Label(o.getDescription());
	        	gp.add(l2, 0, row++,5,1);

	        	CheckBox b = new CheckBox();
	        	Label l = new Label(o.getOpt());

	        	gp.add(b, 0, row);
	        	gp.add(l, 1, row);
	        	ArrayList<TextField> text = new ArrayList<>();
	        	for(int i = 0 ; i < o.getArgs() ; i++) {
	        	 TextField f = new TextField();
	        	 gp.add(f, 2+i, row);
	        	 text.add(f);
	        	}
	        	row++;
	        	checkboxes.put(o, b);
	        	textfields.put(o, text);

	        }
	        parse.setOnAction(getParseFileFolderEvent());
	        latLonToUTM.setOnAction(getLatLonToUTMEvent());
	        UTMToLatLon.setOnAction(getUTMToLatLonEvent());
	        inputDataFile.setOnAction(getOpenInputDataFileEvent());
	        inputDataDir.setOnAction(getOpenInputDataDirEvent());

	        bp.setCenter(sp);

	   	 Scene scene = new Scene(bp);
	        stage.setTitle("Open Spatial v" + OpenSpatial.VERSION + " - Digital Earth Lab 2019");

	        stage.setWidth(1200);
	        stage.setHeight(900);

//	        stage.setScene(value);

	        stage.setScene(scene);
	        stage.show();
	        sp.setDividerPositions(0.4,0.7);
//	        run.setOnAction(getRun());
	}



	private void setupConsole() {
		TextAreaAppender.setConsole(this.loggingView);
		LogManager.getRootLogger().addAppender(new TextAreaAppender());
        loggingView.setWrapText(true);
        loggingView.appendText("Starting Application");
        loggingView.setEditable(false);
	}



	private ComboBox<String> getFileOptionsCombo() {
		ComboBox<String> box = new ComboBox<>();
//		ArrayList<String> longOpts = new ArrayList<>();
		box.getItems().add("Select Input Type");
		box.getItems().add(OpenSpatial.INPUT_KML.getLongOpt());
		box.getItems().add(OpenSpatial.INPUT_ASCII_UTM.getLongOpt());
		box.getItems().add(OpenSpatial.INPUT_ASCII_LATLON.getLongOpt());
		box.getItems().add(OpenSpatial.INPUT_GPX.getLongOpt());
		box.getItems().add(OpenSpatial.INPUT_COR.getLongOpt());

		box.getSelectionModel().select(0);

		return box;
	}

	private EventHandler<ActionEvent> getOpenInputDataDirEvent() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser fc = new DirectoryChooser();
				File dir = fc.showDialog(null);
				if(dir != null) {
					if(dir.exists()) {
						areaData.setText(dir.getAbsolutePath());
					}
				}
			}
		};
	}
	private EventHandler<ActionEvent> getOpenInputDataFileEvent() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				areaData.setText("");
				FileChooser fc = new FileChooser();
				List<File> files = fc.showOpenMultipleDialog(null);
				for(File f : files) {
					areaData.setText(areaData.getText() + f.getAbsolutePath() + "\n");
				}
			}
		};
	}
	LinkedHashMap<String, File> inputs = new LinkedHashMap<>();
	LinkedHashMap<String, Option> outputs = new LinkedHashMap<>();
	LinkedHashMap<String, Option> modifiers = new LinkedHashMap<>();
	private void setInputs()  throws Exception{
		inputs.clear();
		outputs.clear();
		modifiers.clear();
		ArrayList<String> args = new ArrayList<>();
		for(Option o : checkboxes.keySet()) {
			CheckBox c = checkboxes.get(o);
			if(c.isSelected()) {

				args.add("-" + o.getOpt());
				for(TextField f : textfields.get(o)) {
					args.add(f.getText());
				}
			}
		}



		ArrayList<File> ff = parseFilesFolders();

		LinkedHashMap<String, String> keys = new LinkedHashMap<>();
		keys.put(OpenSpatial.INPUT_KML.getLongOpt(), OpenSpatial.INPUT_KML.getOpt());
		keys.put(OpenSpatial.INPUT_ASCII_UTM.getLongOpt(), OpenSpatial.INPUT_ASCII_UTM.getOpt());
		keys.put(OpenSpatial.INPUT_ASCII_LATLON.getLongOpt(), OpenSpatial.INPUT_ASCII_LATLON.getOpt());
		keys.put(OpenSpatial.INPUT_GPX.getLongOpt(), OpenSpatial.INPUT_GPX.getOpt());
		keys.put(OpenSpatial.INPUT_COR.getLongOpt(), OpenSpatial.INPUT_COR.getOpt());

		String key = keys.get(fileFormatOptions.getSelectionModel().getSelectedItem());

		for(File f : ff) {
			inputs.put(key, f);
		}

//		System.out.println("ARGS" + args);
		String[] arr = new String[args.size()];
		arr = args.toArray(arr);


	    CommandLineParser parser = new DefaultParser();
		CommandLine line = parser.parse(OpenSpatial.options, arr);
			for(int i = 0 ; i < line.getOptions().length ; i++) {

				Option o = line.getOptions()[i];
//				System.out.println(i+ ": " + o.getOpt());

				if(o.getOpt().equals(OpenSpatial.INPUT_KML.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_ASCII_UTM.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_ASCII_LATLON.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_GPX.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_COR.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_CUSTOM_WTK.getOpt())) {
					inputs.put(OpenSpatial.INPUT_CUSTOM_WTK.getOpt(), new File(o.getValue(0)));
					OpenSpatial.wtkin = new File(o.getValue(1));
				}
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML_SCALED			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML_TRACK			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_UTM 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_LATLON 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_LATLON_COUNT .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_GPX_TRACK 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_GPX_WAYPOINT 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_COR 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_COR_METRE			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_PETREL_POINTS_LATLON.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_PETREL_POINTS_UTM.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_CUSTOM_WTK.getOpt())) {
					outputs.put(OpenSpatial.OUTPUT_CUSTOM_WTK.getOpt(),o);
					OpenSpatial.wtkout = new File(o.getValue(1));
				}
				else {
					System.out.println(o.getOpt());
					modifiers.put(o.getOpt(), o);
				}
			}
	}
	private ArrayList<Point> run(ArrayList<Point> pts) throws Exception{
		setInputs();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String name = "OpenSpatial_" + dateFormat.format(new Date());

		return OpenSpatial.run(name,pts,outputs,modifiers);
	}

	private EventHandler<ActionEvent> getUTMToLatLonEvent() {
	return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					try {
						setInputs();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					ArrayList<ArrayList<Double>> vals = parsePoints();
					int id = Integer.valueOf(modifiers.get(OpenSpatial.MODIFIER_COLUMN_ID.getOpt()).getValue())-1;
					int x = Integer.valueOf(modifiers.get(OpenSpatial.MODIFIER_COLUMN_X.getOpt()).getValue())-1;
					int y = Integer.valueOf(modifiers.get(OpenSpatial.MODIFIER_COLUMN_Y.getOpt()).getValue())-1;

					areaOut.setText("");
					ArrayList<Point> pts = new ArrayList<>();
					for(ArrayList<Double> row : vals) {
//						System.out.println("" + row.get(x) + "\t" +  row.get(y));
						UTMRef r = new UTMRef(Integer.valueOf(zoneField.getText()), zoneLatField.getText().charAt(0), row.get(x), row.get(y));
						LatLng p = r.toLatLng();
						Point pt = new Point(p.getLatitude(), p.getLongitude());
						pt.name = "" + ((int) row.get(id).intValue());
						pts.add(pt);
//
					}
					ArrayList<Point> ptsOut = run(pts);
					for(Point p : ptsOut) {
						UTMRef r = p.getUTM();
						areaOut.appendText(p.name + "\t" + p.lat + "\t" + p.lon + "\t" + r.getEasting() + "\t" + r.getNorthing() + "\t" + p.z + "\t" + r.getLngZone()  + "\t" + r.getLatZone() + "\n");
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		};
	}

	private EventHandler<ActionEvent> getParseFileFolderEvent() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					setInputs();
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				OpenSpatial.run(inputs, outputs, modifiers);

//				areaOut.setText("");
//				ArrayList<Point> pts = new ArrayList<>();
//				for(ArrayList<Double> row : vals) {
//					Point pt = new Point(row.get(lat), row.get(lon));
//					pts.add(pt);
//					UTMRef r = pt.getUTM();
//					areaOut.appendText(r.getEasting() + "\t" + r.getNorthing() + "\t" + r.getLngZone() + "\t" + r.getLngZone() + "\n");
//				}
//
//				try {
//					ArrayList<Point> ptsOut = run(pts);
//					for(Point p : ptsOut) {
//						UTMRef r = p.getUTM();
//						areaOut.appendText(p.name + "\t" + p.lat + "\t" + p.lon + "\t" + r.getEasting() + "\t" + r.getNorthing() + "\t" + p.z + "\t" + r.getLngZone()  + "\t" + r.getLatZone() + "\n");
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}

			}
		};
	}
	private EventHandler<ActionEvent> getLatLonToUTMEvent() {
		return new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					setInputs();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				ArrayList<ArrayList<Double>> vals = parsePoints();
				int lon = Integer.valueOf(modifiers.get(OpenSpatial.MODIFIER_COLUMN_X.getOpt()).getValue())-1;
				int lat = Integer.valueOf(modifiers.get(OpenSpatial.MODIFIER_COLUMN_Y.getOpt()).getValue())-1;

				areaOut.setText("");
				ArrayList<Point> pts = new ArrayList<>();
				for(ArrayList<Double> row : vals) {
					Point pt = new Point(row.get(lat), row.get(lon));
					pts.add(pt);
					UTMRef r = pt.getUTM();
					areaOut.appendText(r.getEasting() + "\t" + r.getNorthing() + "\t" + r.getLngZone() + "\t" + r.getLngZone() + "\n");
				}

				try {
					ArrayList<Point> ptsOut = run(pts);
					for(Point p : ptsOut) {
						UTMRef r = p.getUTM();
						areaOut.appendText(p.name + "\t" + p.lat + "\t" + p.lon + "\t" + r.getEasting() + "\t" + r.getNorthing() + "\t" + p.z + "\t" + r.getLngZone()  + "\t" + r.getLatZone() + "\n");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
	}

	private ArrayList<File> parseFilesFolders() {
		ArrayList<File>  ff = new ArrayList<>();
		for(String line : areaData.getText().split("\n")) {
			File f = new File(line);
			if(f.exists()) {
				ff.add(f);
			}

		}
		return ff;
	}

	private ArrayList<ArrayList<Double>> parsePoints() {
		ArrayList<ArrayList<Double>>  pts = new ArrayList<>();
		for(String line : areaData.getText().split("\n")) {
			ArrayList<Double> vals = Text.split(line);
			pts.add(vals);

		}
		return pts;
	}



	public static void main(String[] args) {
		OpenSpatialTable.launch();
	}

}
