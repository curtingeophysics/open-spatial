package open.spatial;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Optional;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.converter.NumberStringConverter;

import open.spatial.crs.ProjectionsSelectionNode;
import open.spatial.crs.WTKInstance;
import open.spatial.fonts.FontAwesome;
import open.spatial.formats.CORGPR;
import open.spatial.formats.GPXFormat;
import open.spatial.formats.KML;
import open.spatial.formats.LatLonASCII;
import open.spatial.formats.SEGY;
import open.spatial.formats.UTMASCII;
import open.spatial.points.Point;
import uk.me.jstott.jcoord.UTMRef;

public class OpenSpatialGUI extends Application{
	static final Logger logger = LogManager.getLogger(OpenSpatialGUI.class.getName());

	OpenSpatial osp = new OpenSpatial();



	int selectedInputMode = 0;
	Option selectedInputType = null;
	public TextArea dataIn = new TextArea();
	public TextField filePath = new TextField("./");
	StringProperty inFileBasename = new SimpleStringProperty("");
	public TextField commandPreview = new TextField("OSP.java");
	public TextArea previewPane = new TextArea();
	private Stage stage;

	IntegerProperty idcol = new SimpleIntegerProperty(1);
	IntegerProperty xcol = new SimpleIntegerProperty(2);
	IntegerProperty ycol = new SimpleIntegerProperty(3);
	IntegerProperty zcol = new SimpleIntegerProperty(4);
	IntegerProperty nheader = new SimpleIntegerProperty(1);


	IntegerProperty utmn = new SimpleIntegerProperty(50);
	StringProperty utmc = new SimpleStringProperty("J");

	GridPane previewGP = new GridPane();


	public LinkedHashMap<Option, CheckBox> checkboxes = new LinkedHashMap<>();
	public LinkedHashMap<Option, ArrayList<TextField>> textfields = new LinkedHashMap<>();

	@Override
	public void start(Stage primaryStage) throws Exception {
		OpenSpatial.initLogger();


        BorderPane bp = new BorderPane();
//        bp.setTop(label);

        VBox b = new VBox();
        HBox.setHgrow(b, Priority.ALWAYS);
        b.setMaxWidth(Double.MAX_VALUE);
        BorderPane optionsPane = new BorderPane();
        Image imag = new Image(getClass().getResourceAsStream("logo.png"));
        ImageView logo = new ImageView(imag);
        logo.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				try {
		            Desktop.getDesktop().browse(new URI("http://www.DigitalEarthLab.com"));
		        } catch (IOException e1) {
		            e1.printStackTrace();
		        } catch (URISyntaxException e1) {
		            e1.printStackTrace();
		        }
			}

		});


        Scene s = new Scene(bp);
        Console c = new Console();
        TextArea log = new TextArea();
        log.setStyle("-fx-control-inner-background:#000000; -fx-font-family: Consolas; -fx-highlight-fill: rgb(63,191,239); -fx-highlight-text-fill: #000000; -fx-text-fill: rgb(63,191,239);");
        log.setId("console");
        log.setEditable(false);
        Console.setTextArea(log);
        BorderPane logp = new BorderPane();
        logp.setTop(new Label("Console Log"));
        logp.setCenter(log);
        ScrollPane sp = new ScrollPane(b);
        SplitPane split = new SplitPane(sp,logp);
        split.setDividerPositions(0.85);
        split.orientationProperty().set(Orientation.VERTICAL);
        bp.setTop(logo);
        bp.setCenter(split);
        HBox.setHgrow(bp, Priority.ALWAYS);
        bp.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(sp, Priority.ALWAYS);
        sp.setMaxWidth(Double.MAX_VALUE);
        int row = 0;
        Label stage1 = createLabel("Stage 1: Load Input Data");
        Node inputPane = createInputPane();

        Label stage2 = createLabel("Stage 2: Define Input ID,X,Y,Z Columns");
        Node columnDefPane = createColumnDefPane();

        Label stage3 = createLabel("Stage 3: Set Modifiers");
        Node modifierPane = createModifierPane();

        Label stage4 = createLabel("Stage 4: Set Output Formats");
        Node outputPane = createOutputPane();


        Button run = new Button("EXECUTE");
        run.setStyle("-fx-font-weight: bold; -fx-font-size: 24px; -fx-background-color: rgb(63,191,239);");
        run.setMaxWidth(Double.MAX_VALUE);

        commandPreview.setEditable(false);



        b.getChildren().add(stage1);
        b.getChildren().add(inputPane);

        b.getChildren().add(stage2);
        b.getChildren().add(columnDefPane);

        b.getChildren().add(stage3);
        b.getChildren().add(modifierPane);

        b.getChildren().add(stage4);
        b.getChildren().add(outputPane);
        b.getChildren().add(run);
        b.getChildren().add(commandPreview);

        primaryStage.setScene(s);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
			}
		});
        primaryStage.setWidth(1070);
        primaryStage.setHeight(900);
//        new JMetro(Style.LIGHT).applyTheme(s);
        primaryStage.show();
        this.stage = primaryStage;
        run.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					execute();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


		});
    	Platform.runLater(new Runnable() {

			@Override
			public void run() {
				 logger.info("Starting Open Spatial GUI " + 2019);
					logger.info("Developed by A.M. Pethick (Digital Earth Lab)");
			}
		});
	}

	LinkedHashMap<String, File> inputs = new LinkedHashMap<>();
	LinkedHashMap<String, Option> outputs = new LinkedHashMap<>();
	LinkedHashMap<String, Option> modifiers = new LinkedHashMap<>();
	private void setInputs()  throws Exception{
		inputs.clear();
		outputs.clear();
		modifiers.clear();
		ArrayList<String> args = new ArrayList<>();



		for(Option o : checkboxes.keySet()) {
			CheckBox c = checkboxes.get(o);
			if(c.isSelected()) {

				args.add("-" + o.getOpt());
				for(TextField f : textfields.get(o)) {
					args.add(f.getText());
				}
			}
		}



		ArrayList<File> ff = parseFilesFolders();

		LinkedHashMap<String, String> keys = new LinkedHashMap<>();
		keys.put(OpenSpatial.INPUT_KML.getLongOpt(), OpenSpatial.INPUT_KML.getOpt());
		keys.put(OpenSpatial.INPUT_ASCII_UTM.getLongOpt(), OpenSpatial.INPUT_ASCII_UTM.getOpt());
		keys.put(OpenSpatial.INPUT_ASCII_LATLON.getLongOpt(), OpenSpatial.INPUT_ASCII_LATLON.getOpt());
		keys.put(OpenSpatial.INPUT_GPX.getLongOpt(), OpenSpatial.INPUT_GPX.getOpt());
		keys.put(OpenSpatial.INPUT_COR.getLongOpt(), OpenSpatial.INPUT_COR.getOpt());

		String key = keys.get(selectedInputType.getOpt());
		for(Option o : checkboxes.keySet()) {
			CheckBox c = checkboxes.get(o);
			if(c.isSelected()) {

				args.add("-" + o.getOpt());
				for(TextField f : textfields.get(o)) {
					args.add(f.getText());
				}
			}
		}
		for(Option o : checkboxes.keySet()) {
			CheckBox c = checkboxes.get(o);
			if(c.isSelected()) {

				args.add("-" + o.getOpt());
				for(TextField f : textfields.get(o)) {
					args.add(f.getText());
				}
			}
		}
		for(File f : ff) {
			inputs.put(key, f);
		}

//		System.out.println("ARGS" + args);
		String[] arr = new String[args.size()];
		arr = args.toArray(arr);


	    CommandLineParser parser = new DefaultParser();
		CommandLine line = parser.parse(OpenSpatial.options, arr);
			for(int i = 0 ; i < line.getOptions().length ; i++) {

				Option o = line.getOptions()[i];
//				System.out.println(i+ ": " + o.getOpt());

				if(o.getOpt().equals(OpenSpatial.INPUT_KML.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_ASCII_UTM.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_ASCII_LATLON.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_GPX.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_COR.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(OpenSpatial.INPUT_CUSTOM_WTK.getOpt())) {
					inputs.put(OpenSpatial.INPUT_CUSTOM_WTK.getOpt(), new File(o.getValue(0)));
					OpenSpatial.wtkin = new File(o.getValue(1));
				}

				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML_SCALED			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_KML_TRACK			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_UTM 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_LATLON 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_ASCII_LATLON_COUNT .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_GPX_TRACK 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_GPX_WAYPOINT 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_COR 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_COR_METRE			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_PETREL_POINTS_LATLON.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_PETREL_POINTS_UTM.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OpenSpatial.OUTPUT_CUSTOM_WTK.getOpt())) {
					outputs.put(OpenSpatial.OUTPUT_CUSTOM_WTK.getOpt(),o);
					OpenSpatial.wtkout = new File(o.getValue(1));
				}
				else {
//					System.out.println(o.getOpt());
					modifiers.put(o.getOpt(), o);
				}
			}


	}

	private ArrayList<File> parseFilesFolders() {
		ArrayList<File> files = new ArrayList<File>();
		files.add(new File(filePath.getText()));
		String basename = FilenameUtils.removeExtension(filePath.getText());
		inFileBasename.set(basename);
		return files;
	}

	private ArrayList<Point> execute() throws Exception {
		setInputs();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String name = "OpenSpatial_" + dateFormat.format(new Date());

		showCommand();


		return OpenSpatial.run(name,getPoints(),outputs,modifiers);
	}
	private void showCommand() {
		try {
			setInputs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String command = "java -jar osp.jar -" + selectedInputType.getOpt()+ " " + (selectedInputMode == 0 ? "temp.txt" : filePath.getText());

		if(idcol.get() >= 0)  command += (" -" + OpenSpatial.MODIFIER_COLUMN_ID.getOpt() + " " + idcol.get());
		if(xcol.get() >= 0)  command += (" -" + OpenSpatial.MODIFIER_COLUMN_X.getOpt() + " " + xcol.get());
		if(ycol.get() >= 0)  command += (" -" + OpenSpatial.MODIFIER_COLUMN_Y.getOpt() + " " + ycol.get());
		if(zcol.get() >= 0)  command += (" -" + OpenSpatial.MODIFIER_COLUMN_Z.getOpt() + " " + zcol.get());
		if(utmn.get() >= 0)  command += (" -" + OpenSpatial.MODIFIER_UTM.getOpt() + " " + utmn.get() + " " + utmc.get().toUpperCase().trim().charAt(0));




		for(Option o : checkboxes.keySet()) {
			CheckBox c = checkboxes.get(o);
			if(c.isSelected()) {

				command += (" -" + o.getOpt());
				for(TextField f : textfields.get(o)) {
					command += ( " " + f.getText());
				}
			}
		}
		commandPreview.setText(command);
		logger.info("COMMAND: " + command);
	}

	private Node createModifierPane() {
		int row = 0;
		GridPane gp = new GridPane();
		int count = 0;
		int col = 0;
        for(Option o : OpenSpatial.modifiersGUI) {
        	boolean isFirst = count%2 == 0;
        	if(isFirst) {
        		col = 0;
        	}else {
        		col++;
        	}
        	count++;


        	CheckBox b = new CheckBox();
        	Label l = new Label(o.getOpt());

        	gp.add(b, col++, row);
        	gp.add(l, col++, row);
        	ArrayList<TextField> text = new ArrayList<>();
        	HBox tb = new HBox();
        	for(int i = 0 ; i < o.getArgs() ; i++) {
        	 TextField f = new TextField();
        	 if(OpenSpatial.defaultModifierValues.containsKey(o)) {
        		 f.setText(OpenSpatial.defaultModifierValues.get(o).get(i));
        	 }
        	 tb.getChildren().add(f);
        	 text.add(f);
        	}
        	gp.add(tb, col++, row);
        	Label l2 = new Label(o.getDescription());
        	gp.add(l2, col++, row,1,1);

        	checkboxes.put(o, b);
        	textfields.put(o, text);
        	if(!isFirst) {

        		row++;
        	}

        }
        return gp;


	}

	private Node createOutputPane() {

	  GridPane gp = new GridPane();
	  int row = 1;

      gp.add(new Label(""),0,row++,4,1);
      int count = 0;
	  int col = 0;


      for(Option o : OpenSpatial.outputs) {
    	boolean isFirst = count%2 == 0;
        if(isFirst) {
        	col = 0;
        }else {
        	col++;
        }
        count++;


      	CheckBox b = new CheckBox();
      	Label l = new Label(o.getOpt());

      	gp.add(b, col++, row);
      	gp.add(l, col++, row);
      	ArrayList<TextField> text = new ArrayList<>();
      	HBox tb = new HBox();

      	for(int i = 0 ; i < o.getArgs() ; i++) {
      	final TextField f = new TextField();
      	inFileBasename.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				String ext = OpenSpatial.getOutputExtension(o);
				if(ext != null) {
					f.setText(newValue + ext);
				}
			}
		});
      	Button load = new Button("...");
      	tb.getChildren().add(f);
      	 if(i == 0) {
     		load.setOnAction(new EventHandler<ActionEvent>() {

    			@Override
    			public void handle(ActionEvent event) {
    				FileChooser fc = new FileChooser();
    				File last = new File(f.getText());
    				if(last == null) {
    					fc.setInitialDirectory(new File("./"));
    				} else {
    					fc.setInitialDirectory(last.getParentFile());
    				}
    				File out = fc.showSaveDialog(stage);
    				if(out != null ) {
    					f.setText(out.getAbsolutePath());
    				}
    			}
    		});
     	 	tb.getChildren().add(load);
      	 }
//      	 gp.add(f, col++, row);


    	 text.add(f);
    	}
    	gp.add(tb, col++, row);
    	Label l2 = new Label(o.getDescription().replaceAll("Output an", "").replaceAll("Output a", "").replaceAll("Output", ""));
      	gp.add(l2, col++, row,1,1);
      	checkboxes.put(o, b);
      	textfields.put(o, text);
      	if(!isFirst) {

    		row++;
    	}


      }

      return gp;
	}

	private Node createColumnDefPane() {
		BorderPane bp = new BorderPane();
		GridPane gp = new GridPane();
		int row = 0;

		createField("ID Column",idcol,"(Leave Blank if not present)",gp,row++);
		createField("X/Lon Column",xcol,"",gp,row++);
		createField("Y/Lat Column",ycol,"",gp,row++);
		createField("Z Column",zcol,"(Leave Blank if not present)",gp,row++);
		createField("Header Lines",nheader,"(Use 0 if no header present)",gp,row++);
		createField("UTM Grid Zone",utmn,"(e.g., 50)",gp,row++);
		createField("UTM Grid Band",utmc,"(e.g., J)",gp,row++);
		Button preview = new Button("Preview Configuration (First Point)");
		preview.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				updatePreview();
				showCommand();
			}
		});
		gp.add(preview, 0, row++,3,1);
		bp.setTop(gp);
		bp.setCenter(previewPane);
		BorderPane bpmain = new BorderPane();
		bpmain.setCenter(bp);
		bpmain.setTop(previewGP);

		return bpmain;
	}

	private void createField(String name, IntegerProperty v, String desc, GridPane gp, int row) {
		TextField f = new TextField("" + v.get());

		Bindings.bindBidirectional(f.textProperty(), v, new NumberStringConverter());


//		f.textProperty().addListener(new ChangeListener<String>() {
//
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				int val = -1;
//				try {
//					val = Integer.valueOf(newValue);
//				} catch(Exception e) {
//
//				}
//				v.set(val);
//			}
//		});


		gp.add(new Label(name), 0, row);
		gp.add(f, 1, row);
		gp.add(new Label(desc), 2, row);

	}
	private void createField(String name, StringProperty v, String desc, GridPane gp, int row) {
		TextField f = new TextField("" + v.get());
		f.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				v.set(newValue.trim().toUpperCase().charAt(0) + "");
			}
		});

		gp.add(new Label(name), 0, row);
		gp.add(f, 1, row);
		gp.add(new Label(desc), 2, row);

	}


	private ArrayList<Point> getPoints() {
		switch (selectedInputMode) {
		case 0:
			logger.info("Previewing Text");
			saveText();
			return updatePreviewFile(new File("temp.txt"),false);
		case 1:
			logger.info("Previewing File");
			File file = new File(filePath.getText());
			return updatePreviewFile(file,false);
		}
		return new ArrayList<Point>();
	}

	private void updatePreview() {
		switch (selectedInputMode) {
		case 0:
			logger.info("Previewing Text");
			saveText();
			updatePreviewFile(new File("temp.txt"),true);
			break;
		case 1:
			logger.info("Previewing File");
			File file = new File(filePath.getText());
			updatePreviewFile(file,true);
			break;
		}
	}




	private ArrayList<Point> updatePreviewFile(File file, boolean preview) {
		Option o = selectedInputType;
		String key = o.getOpt();

		int colid = idcol.get()-1;
		int colx = xcol.get()-1;
		int coly = ycol.get()-1;
		int colz = zcol.get()-1;
		int headerLines = nheader.get();
		int utmZoneNumber = 50;
		char utmZoneID = 'J';
		try {
			utmZoneNumber = utmn.get();
			utmZoneID = utmc.get().charAt(0);
		} catch(Exception e) {
			e.printStackTrace();
		}
		ArrayList<Point> points = new ArrayList<Point>();
		//INPUT FILE HANDLING
		if(key.equals(OpenSpatial.INPUT_KML.getOpt())) {
			try {
				points = KML.loadKMLPoints(file.getAbsolutePath(),preview);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(key.equals(OpenSpatial.INPUT_ASCII_UTM.getOpt())) {
			points = UTMASCII.loadUTMPoints(file.getAbsolutePath(),utmZoneNumber,utmZoneID,colid,colx,coly,colz,headerLines,preview);
		} else if(key.equals(OpenSpatial.INPUT_ASCII_LATLON.getOpt())) {
			points = LatLonASCII.loadLatLonPoints(file.getAbsolutePath(),colid,colx,coly,colz,headerLines,preview);
		} else if(key.equals(OpenSpatial.INPUT_GPX.getOpt())) {
			try {
				points = GPXFormat.read(file.getAbsolutePath(),preview);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(key.equals(OpenSpatial.INPUT_COR.getOpt())) {
			points = CORGPR.read(file.getAbsolutePath(),preview);
		} else if(key.equals(OpenSpatial.INPUT_CUSTOM_WTK.getOpt())) {
//			points = CRSConverter.loadPoints(file.getAbsolutePath(),wtkin.getAbsolutePath(),colid,colx,coly,colz,headerLines);
			logger.warn("Cannot use input WTK on preview");
		} else if(key.equals(OpenSpatial.INPUT_SGY.getOpt())) {
			points = SEGY.read(file,utmZoneNumber,utmZoneID,preview);
		}

		String out = "";
		Point p = points.get(0);
		UTMRef r=p.getUTM();
		out += "ID: " + p.name + "\n";
		out += "Latitude: " + p.lat + "\n";
		out += "Longitude: " + p.lon+ "\n";
		out += "Datum: " +  r.getDatum().getName()+ "\n";
		out += "UTM: " + r.getEasting() + " mE " + r.getNorthing() + " mN " + r.getLatZone() + " " + r.getLngZone()+ "\n";
		out += "Z: " + p.z;
		previewPane.setText(out);
		return points;
	}

	private void saveText() {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(new File("temp.txt")));
			out.write(dataIn.getText());
			out.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void updatePreviewColumns() {
		previewGP.getChildren().clear();
		previewGP.setMinWidth(300);
		if(selectedInputType == OpenSpatial.INPUT_ASCII_UTM || selectedInputType == OpenSpatial.INPUT_ASCII_UTM) {
			previewGP.setVisible(true);
			int nHeaders = nheader.get();
			File file = new File(filePath.getText());
			if(file.isFile() && file.exists())
			try {
				BufferedReader in = new BufferedReader(new FileReader(file));
				String line = "";
				for(int i = 0 ; i < nHeaders ; i++) {
					//skip headers
					line = in.readLine();
				}

				line = in.readLine();;
				String [] dat = Text.sanitizeString(line);
				Label column = new Label("Column Number");
				Label value = new Label("Value");
				Label xLabel = new Label("X");
				Label yLabel = new Label("Y");
				Label zLabel = new Label("Z");
				Label idLabel = new Label("ID");
				previewGP.getChildren().clear();
				int row = 0;
				previewGP.add(column, 0, row);
				previewGP.add(value, 1, row);
				previewGP.add(xLabel, 2, row);
				previewGP.add(yLabel, 3, row);
				previewGP.add(zLabel, 4, row);
				previewGP.add(idLabel,5, row);


				row++;
				final ToggleGroup groupX = new ToggleGroup();
				final ToggleGroup groupY = new ToggleGroup();
				final ToggleGroup groupZ = new ToggleGroup();
				final ToggleGroup groupID = new ToggleGroup();

				for(int i = 0 ; i < dat.length ; i++) {
					int col = i+1;
					String val = dat[i];
					Label columnV = new Label("" + col);
					Label valueV = new Label(val);
					RadioButton xr = new RadioButton();
					RadioButton yr = new RadioButton();
					RadioButton zr = new RadioButton();
					RadioButton idr = new RadioButton();
					xr.setToggleGroup(groupX);
					xr.setSelected(false);
					yr.setToggleGroup(groupY);
					yr.setSelected(false);
					zr.setToggleGroup(groupZ);
					zr.setSelected(false);
					idr.setToggleGroup(groupID);
					idr.setSelected(false);

					xr.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event) {
							xcol.set(col);
						}
					});
					yr.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event) {
							ycol.set(col);
						}
					});
					zr.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event) {
							zcol.set(col);
						}
					});
					idr.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event) {
							idcol.set(col);
						}
					});
					previewGP.add(columnV, 0, row);
					previewGP.add(valueV, 1, row);
					previewGP.add(xr, 2, row);
					previewGP.add(yr, 3, row);
					previewGP.add(zr, 4, row);
					previewGP.add(idr, 5, row);
					row++;
				}


				in.close();
			} catch(Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}

		} else {
			previewGP.setVisible(false);
		}
	}

	Button inputProjectionButton = new Button("Select Input Projection");
	Button outputProjectionButton = new Button("Select Output Projection");


	private Node createInputPane() {
		BorderPane bp = new BorderPane();


		Label inputProjectionSelected = new Label("Select Projection");

		inputProjectionButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Dialog<ButtonType> d = new Dialog<ButtonType>();
				d.getDialogPane().getButtonTypes().add(ButtonType.OK);
				d.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
				d.setHeaderText("Select Input Projection");
				d.setContentText("Search and select your input projection method.\tIf your projection is not listed� insert your WTK into the file EPSG_WTK.txt");
				ProjectionsSelectionNode n = new ProjectionsSelectionNode();
				d.setGraphic(n);
				d.setTitle("WTK Projection Selection");
//				new JMetro(Style.LIGHT).applyTheme(n);
				Optional<ButtonType> result = d.showAndWait();
				 if (result.isPresent() && result.get() == ButtonType.OK) {
					 //
				     WTKInstance ref = n.selectedWTKInstance.get();
				     inputProjectionSelected.setText(ref.id + "\t" + ref.name);
				 }
			}
		});



		Label inputModeLabel = new Label("Input Mode");

		ComboBox<String> inputMode = new ComboBox<String>();
		inputMode.getItems().addAll("Copy-Pasted Text","File");
		ComboBox<String> inputTypes = new ComboBox<String>();
		for(Option o : osp.inputOptions.getOptions()) {
			inputTypes.getItems().add(o.getOpt() + ": " + o.getDescription());
		}




		Button load = new Button("...");
		load.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser fc = new FileChooser();
				File last = new File(filePath.getText());
				if(last == null) {
					fc.setInitialDirectory(new File("./"));
				} else {
					fc.setInitialDirectory(last.getParentFile());
				}
				File out = fc.showOpenDialog(stage);
				if(out != null ) {
					filePath.setText(out.getAbsolutePath());
					updatePreviewColumns();
				}
			}
		});
		Node fileChooserBox = new HBox(new Label("File Path"),filePath,load);



		inputMode.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				selectedInputMode = inputMode.getSelectionModel().getSelectedIndex();
				switch(selectedInputMode) {

				case 0:
					bp.setCenter(dataIn);
					break;
				case 1:

					bp.setCenter(fileChooserBox );
					break;
				default:
					break;
				}
			}
		});
		inputTypes.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				selectedInputType = new ArrayList<Option>(osp.inputOptions.getOptions()).get(inputTypes.getSelectionModel().getSelectedIndex());
				updatePreviewColumns();
			}
		});
		inputTypes.getSelectionModel().select(1);
		inputMode.getSelectionModel().selectFirst();
		GridPane gp = new GridPane();
		int row = 0;

		gp.add(FontAwesome.CARET_DOWN(2f), 0, row);
		gp.add(new Label("Input Mode"), 1, row);
		gp.add(inputMode, 2, row++);

		gp.add(FontAwesome.FILE(2f), 0, row);
		gp.add(new Label("File Format"), 1, row);
		gp.add(inputTypes, 2, row++);

		gp.add(FontAwesome.GLOBE(2f), 0, row);
		gp.add(inputProjectionButton, 1, row);
		gp.add(inputProjectionSelected, 2, row++);





		bp.setTop(gp);
		return bp;
	}

	private Label createLabel(String s) {
	      Label l = new Label(s);
	      l.setStyle("-fx-font-weight: bold; -fx-font-size: 18px;");
	      return l;
	}

	public static void main(String[] args) {

		OpenSpatialGUI.launch(args);
	}

}
