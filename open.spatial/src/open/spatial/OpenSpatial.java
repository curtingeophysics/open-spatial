package open.spatial;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javafx.application.Application;
import open.spatial.crs.CRSConverter;
import open.spatial.formats.CORGPR;
import open.spatial.formats.GPXFormat;
import open.spatial.formats.KML;
import open.spatial.formats.LatLonASCII;
import open.spatial.formats.PetrelASCII;
import open.spatial.formats.SEGY;
import open.spatial.formats.UTMASCII;
import open.spatial.points.Point;
import open.spatial.srtm.ARCGISASCII;
import open.spatial.srtm.SRTM;
import open.spatial.utils.PropertiesManager;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;



public class OpenSpatial{

	static final Logger logger = LogManager.getLogger(OpenSpatial.class.getName());
	public final static PropertiesManager properties = new PropertiesManager(new File("OpenSpatial.properties"));

	public final static String VERSION = "1.0 Alpha";

	public final static ArrayList<Option> modifiers = new ArrayList<>();
	public final static ArrayList<Option> outputs = new ArrayList<>();

	public final static ArrayList<Option> modifiersGUI = new ArrayList<Option>();

	public final static LinkedHashMap<Option, ArrayList<String>> defaultModifierValues = new LinkedHashMap<>();

	public final static OptionGroup inputOptions = new OptionGroup();
	public final static Option INPUT_KML = new Option("ikml", "input-kml", true, "Input a single Line from a KML file");
	public final static Option INPUT_ASCII_UTM = new Option("iutm", "input-utm", true, "Input an ASCII UTM file");
	public final static Option INPUT_ASCII_LATLON = new Option("ill", "input-latlon", true, "Input an ASCII Lat-Lon file");
	public final static Option INPUT_GPX = new Option("igpx", "input-gpx", true, "Input GPX file");
	public final static Option INPUT_COR = new Option("icor", "input-gpr-cor", true, "Input an GPR Cor file");
	public final static Option INPUT_CUSTOM_WTK = new Option("icust", "input-cus-file", true, "Input Custom Coords (secondary WTK txt req.)");
	public final static Option INPUT_SGY = new Option("isgy", "input-segy-file", true, "2D SEGY file (no transformations)");




//	public final static OptionGroup outputOptions = new OptionGroup();
	public final static LinkedHashMap<String, String> outputExtensions = new LinkedHashMap<String, String>();
	public final static Option OUTPUT_KML 				= new Option("okml", "output-kml", true, "Output KML (Waypoints)");
	public final static Option OUTPUT_KML_SCALED 				= new Option("okmls", "output-kml-scaled", true, "Output KML (Waypoints Z-Scaled)");
	public final static Option OUTPUT_KML_TRACK 		= new Option("okmlt", "output-kml-track", true, "Output KML (Single Track) file");
	public final static Option OUTPUT_ASCII_UTM 		= new Option("outm", "output-utm", true, "Output ASCII UTM file");
	public final static Option OUTPUT_ASCII_LATLON 		= new Option("oll", "output-latlon", true, "Output ASCII Lat-Lon file");
	public final static Option OUTPUT_ASCII 		= new Option("o", "output-ascii", true, "Output ASCII file with UTM and Lat-Lon coordinates");
	public final static Option OUTPUT_ASCII_LATLON_COUNT = new Option("ollc", "output-latlon-count", true, "Output ASCII Lat-Lon heatmap file");
	public final static Option OUTPUT_GPX_TRACK 		= new Option("ogpxt", "output-gpx-track", true, "Output GPX track file");
	public final static Option OUTPUT_GPX_WAYPOINT 		= new Option("ogpxw", "output-gpx-waypoint", true, "Output GPX waypoint file");
	public final static Option OUTPUT_COR 				= new Option("ocor", "output-gpr-cor", true, "Output COR (GPR) file");
	public final static Option OUTPUT_COR_METRE 				= new Option("ocorm", "output-gpr-corm", true, "Output COR (GPR) file (metres)");
	public final static Option OUTPUT_PETREL_POINTS_LATLON = new Option("opll", "output-petrel-pts-latlon", true, "Output Petrel Points (ASCII) Lat/Lon");
	public final static Option OUTPUT_PETREL_POINTS_UTM = new Option("oputm", "output-petrel-pts-utm", true, "Output Petrel Points (ASCII) UTM");
	public final static Option OUTPUT_CUSTOM_WTK = new Option("ocust", "output-cus-file", true, "Output Custom Coords (secondary WTK txt req.)");

//	public final static OptionGroup modifiers 		= new OptionGroup();
	public final static Option RUN_GUI 		= new Option("gui", "gui", false, "Run Open Spatial using GUI");

	public final static Option MODIFIER_NEGATE_Z 		= new Option("zflip", "z-flip-sign", false, "Flip the sign of Z");
	public final static Option MODIFIER_HEADER 		= new Option("nhead", "number-header", true, "Override the number of header Lines (default = 1)");
	public final static Option MODIFIER_DLAT 		= new Option("dlat", "d-lat", true, "grid spacing latitude");
	public final static Option MODIFIER_DLON		= new Option("dlon", "d-lon", true, "grid spacing longitude");
	public final static Option MODIFIER_LAT_MIN 	= new Option("latmin", "latitude-min", true, "remove latitudes less than this value");
	public final static Option MODIFIER_LAT_MAX 	= new Option("latmax", "latitude-max", true, "remove latitudes greater than this value");
	public final static Option MODIFIER_LON_MIN 	= new Option("lonmin", "longitude-min", true, "remove longitudes less than this value");
	public final static Option MODIFIER_LON_MAX 	= new Option("lonmax", "longitude-max", true, "remove longitudes greater than this value");
	public final static Option MODIFIER_UTM 		= new Option("utm", "utm-zone", true, "the UTM zone e.g. 50 J. Must be included when working with UTM coords.");
	public final static Option MODIFIER_PTI 		= new Option("pti", "point-increment", true, "interpolate points along line at fixed offsets");
	public final static Option MODIFIER_PTI_LINEAR 	= new Option("ptl", "point-linear-increment", true, "linearly interpolate points along line at fixed offsets");
	public final static Option MODIFIER_LINENUMBER 	= new Option("ln", "line-number", true, "set the line number");
	public final static Option MODIFIER_CUT_MIN 	= new Option("cmin", "cut-min", true, "Remove ID's less than cmin");
	public final static Option MODIFIER_CUT_MAX 	= new Option("cmax", "cut-max", true, "Remove ID's greater than cmax");
	public final static Option MODIFIER_COLUMN_ID 	= new Option("colid", "column-id", true, "column ID");
	public final static Option MODIFIER_COLUMN_X 	= new Option("colx", "column-x", true, "Index of column containing easting/longitude (starts at 1)");
	public final static Option MODIFIER_COLUMN_Y 	= new Option("coly", "column-y", true, "Index of column containing northing/latitude (starts at 1)");
	public final static Option MODIFIER_COLUMN_Z 	= new Option("colz", "column-z", true, "Index of column containing depth/elevation (starts at 1)");
	public final static Option MODIFIER_FORCE_UPDATE_NAMING = new Option("fnaming", "force-naming", false, "override point naming/indexing");
	public final static Option MODIFIER_PRINT 		= new Option("p", "print", false, "print points at the end");
	public final static Option MODIFIER_SKIP_NTH 	= new Option("skip", "skip-nth", true, "skip every nth point");
	public final static Option MODIFIER_REVERSE 	= new Option("r", "reverse", false, "reverse output");
	public final static Option MODIFIER_SPLIT_OUTPUT 	= new Option("split", "split-output", true, "Split output into N files");
	public final static Option MODIFIER_MAP_ELEVATIONS_DEM_LIDAR = new Option("mapd", true, "Re-map elevations from an ARCGIS ASCII File");
	public final static Option MODIFIER_MAP_ELEVATIONS_SRTM = new Option("zsrtm", false, "Re-map elevations from SRTM-3 database");


	public static File wtkin = null;
	public static File wtkout = null;

	public final static Options options = generateOptions();

	public final static NumberFormat nf7 = new DecimalFormat("0000000");
	public final static NumberFormat nf6 = new DecimalFormat("000000");
	public final static NumberFormat nf5 = new DecimalFormat("00000");
	public final static NumberFormat nf4 = new DecimalFormat("0000");
	public final static NumberFormat nf3 = new DecimalFormat("000");
	public final static NumberFormat nf2 = new DecimalFormat("00");
	public final static NumberFormat nf1 = new DecimalFormat("0");




	private static Options generateOptions() {
		Options options = new Options();
		initializeImportOptions();
		initializeOutputOptions(options,outputs);
		initializeModifierOptions(options,modifiers);
		initializeDefaultGUIModifiers(modifiersGUI);
		options.addOptionGroup(inputOptions);

		return options;
	}

	private static void initializeDefaultGUIModifiers(ArrayList<Option> list) {
		list.add(MODIFIER_LAT_MIN            );
		list.add(MODIFIER_LAT_MAX            );
		list.add(MODIFIER_LON_MIN            );
		list.add(MODIFIER_LON_MAX            );
		list.add(MODIFIER_MAP_ELEVATIONS_SRTM);
		list.add(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR       );
		list.add(MODIFIER_NEGATE_Z			  );
		list.add(MODIFIER_DLAT               );
		list.add(MODIFIER_DLON               );

		list.add(MODIFIER_PTI                );
		list.add(MODIFIER_PTI_LINEAR         );
		list.add(MODIFIER_LINENUMBER         );
		list.add(MODIFIER_CUT_MIN            );
		list.add(MODIFIER_CUT_MAX            );
		list.add(MODIFIER_FORCE_UPDATE_NAMING);
		list.add(MODIFIER_PRINT              );
		list.add(MODIFIER_SKIP_NTH           );
		list.add(MODIFIER_REVERSE            );
		list.add(MODIFIER_SPLIT_OUTPUT       );
	}

	private static void initializeModifierOptions(Options options, ArrayList<Option> list) {
				MODIFIER_HEADER		 .setArgName(         "n");
				MODIFIER_DLAT 		 .setArgName(         "decimal deg");
				MODIFIER_DLON		 .setArgName(         "decimal deg");
				MODIFIER_LAT_MIN 	 .setArgName(         "value");
				MODIFIER_LAT_MAX 	 .setArgName(         "value");
				MODIFIER_LON_MIN 	 .setArgName(         "value");
				MODIFIER_LON_MAX 	 .setArgName(         "value");
				MODIFIER_UTM 		 .setArgName(         "zone");
				MODIFIER_PTI 		 .setArgName(         "value");
				MODIFIER_PTI_LINEAR .setArgName(          "value");
				MODIFIER_LINENUMBER .setArgName(          "string");
				MODIFIER_CUT_MIN 	 .setArgName(         "value");
				MODIFIER_CUT_MAX 	 .setArgName(         "value");
				MODIFIER_COLUMN_ID 	 .setArgName(         "index");
				MODIFIER_COLUMN_X 	 .setArgName(         "index");
				MODIFIER_COLUMN_Y 	 .setArgName(         "index");
				MODIFIER_COLUMN_Z 	 .setArgName(         "index");
				MODIFIER_SKIP_NTH 	 .setArgName(         "nth");
				MODIFIER_SPLIT_OUTPUT.setArgName("N");
				MODIFIER_MAP_ELEVATIONS_DEM_LIDAR.setArgName("file");
				MODIFIER_MAP_ELEVATIONS_SRTM.setArgName("");
				MODIFIER_MAP_ELEVATIONS_SRTM.setArgs(0);
				MODIFIER_UTM.setArgs(2);

				defaultModifierValues.put(MODIFIER_COLUMN_X, new ArrayList<>(Arrays.asList("1")));
				defaultModifierValues.put(MODIFIER_COLUMN_Y, new ArrayList<>(Arrays.asList("2")));
				defaultModifierValues.put(MODIFIER_COLUMN_Z, new ArrayList<>(Arrays.asList("3")));
				defaultModifierValues.put(MODIFIER_UTM, new ArrayList<>(Arrays.asList("50","J")));
				defaultModifierValues.put(MODIFIER_HEADER, new ArrayList<>(Arrays.asList("1")));


				options.addOption(RUN_GUI                );
				options.addOption(MODIFIER_UTM                );
				options.addOption(MODIFIER_COLUMN_X           );
				options.addOption(MODIFIER_COLUMN_Y           );
				options.addOption(MODIFIER_COLUMN_Z           );
				options.addOption(MODIFIER_COLUMN_ID          );

				options.addOption(MODIFIER_NEGATE_Z			  );
				options.addOption(MODIFIER_HEADER             );
				options.addOption(MODIFIER_DLAT               );
				options.addOption(MODIFIER_DLON               );
				options.addOption(MODIFIER_LAT_MIN            );
				options.addOption(MODIFIER_LAT_MAX            );
				options.addOption(MODIFIER_LON_MIN            );
				options.addOption(MODIFIER_LON_MAX            );
				options.addOption(MODIFIER_PTI                );
				options.addOption(MODIFIER_PTI_LINEAR         );
				options.addOption(MODIFIER_LINENUMBER         );
				options.addOption(MODIFIER_CUT_MIN            );
				options.addOption(MODIFIER_CUT_MAX            );
				options.addOption(MODIFIER_FORCE_UPDATE_NAMING);
				options.addOption(MODIFIER_PRINT              );
				options.addOption(MODIFIER_SKIP_NTH           );
				options.addOption(MODIFIER_REVERSE            );
				options.addOption(MODIFIER_SPLIT_OUTPUT       );
				options.addOption(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR       );
				options.addOption(MODIFIER_MAP_ELEVATIONS_SRTM);

				list.add(MODIFIER_HEADER             );
				list.add(MODIFIER_UTM                );
				list.add(MODIFIER_COLUMN_X           );
				list.add(MODIFIER_COLUMN_Y           );
				list.add(MODIFIER_COLUMN_Z           );
				list.add(MODIFIER_COLUMN_ID          );
				list.add(MODIFIER_MAP_ELEVATIONS_SRTM);
				list.add(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR       );
				list.add(MODIFIER_NEGATE_Z			  );
				list.add(MODIFIER_DLAT               );
				list.add(MODIFIER_DLON               );
				list.add(MODIFIER_LAT_MIN            );
				list.add(MODIFIER_LAT_MAX            );
				list.add(MODIFIER_LON_MIN            );
				list.add(MODIFIER_LON_MAX            );
				list.add(MODIFIER_PTI                );
				list.add(MODIFIER_PTI_LINEAR         );
				list.add(MODIFIER_LINENUMBER         );
				list.add(MODIFIER_CUT_MIN            );
				list.add(MODIFIER_CUT_MAX            );
				list.add(MODIFIER_FORCE_UPDATE_NAMING);
				list.add(MODIFIER_PRINT              );
				list.add(MODIFIER_SKIP_NTH           );
				list.add(MODIFIER_REVERSE            );
				list.add(MODIFIER_SPLIT_OUTPUT       );


	}

	private static void initializeOutputOptions(Options options, ArrayList<Option> list) {
		OUTPUT_KML.setArgName("path");
		OUTPUT_KML_SCALED.setArgName("path");
		OUTPUT_KML_TRACK.setArgName("path");
		OUTPUT_ASCII.setArgName("path");
		OUTPUT_ASCII_UTM.setArgName("path");
		OUTPUT_ASCII_LATLON.setArgName("path");
		OUTPUT_GPX_TRACK.setArgName("path");
		OUTPUT_GPX_WAYPOINT.setArgName("path");
		OUTPUT_COR.setArgName("path");
		OUTPUT_COR_METRE.setArgName("path");
		OUTPUT_PETREL_POINTS_LATLON.setArgName("path");
		OUTPUT_PETREL_POINTS_UTM.setArgName("path");

		OUTPUT_CUSTOM_WTK.setArgs(2);
		OUTPUT_CUSTOM_WTK.setArgName("path_pts path_wtk");

		outputExtensions.put(OUTPUT_ASCII.getOpt(), "csv");
		outputExtensions.put(OUTPUT_KML.getOpt(), "kml");
		outputExtensions.put(OUTPUT_KML_SCALED.getOpt(), "kml");
		outputExtensions.put(OUTPUT_KML_TRACK.getOpt(), "kml");
		outputExtensions.put(OUTPUT_ASCII_UTM.getOpt(), "csv");
		outputExtensions.put(OUTPUT_ASCII_LATLON.getOpt(), "csv");
		outputExtensions.put(OUTPUT_GPX_TRACK.getOpt(), "gpx");
		outputExtensions.put(OUTPUT_GPX_WAYPOINT.getOpt(), "gpx");
		outputExtensions.put(OUTPUT_COR.getOpt(), "cor");
		outputExtensions.put(OUTPUT_COR_METRE.getOpt(), "cor");
		outputExtensions.put(OUTPUT_PETREL_POINTS_UTM.getOpt(), "txt");
		outputExtensions.put(OUTPUT_PETREL_POINTS_LATLON.getOpt(), "txt");
		outputExtensions.put(OUTPUT_CUSTOM_WTK.getOpt(), "txt");

		options.addOption(OUTPUT_KML);
		options.addOption(OUTPUT_KML_SCALED);
		options.addOption(OUTPUT_KML_TRACK);
		options.addOption(OUTPUT_ASCII);
		options.addOption(OUTPUT_ASCII_UTM);
		options.addOption(OUTPUT_ASCII_LATLON);
		options.addOption(OUTPUT_ASCII_LATLON_COUNT);
		options.addOption(OUTPUT_GPX_TRACK);
		options.addOption(OUTPUT_GPX_WAYPOINT);
		options.addOption(OUTPUT_COR);
		options.addOption(OUTPUT_COR_METRE);
		options.addOption(OUTPUT_PETREL_POINTS_LATLON);
		options.addOption(OUTPUT_PETREL_POINTS_UTM);
		options.addOption(OUTPUT_CUSTOM_WTK);

		list.add(OUTPUT_KML);
		list.add(OUTPUT_KML_SCALED);
		list.add(OUTPUT_KML_TRACK);
		list.add(OUTPUT_ASCII);
		list.add(OUTPUT_ASCII_UTM);
		list.add(OUTPUT_ASCII_LATLON);
		list.add(OUTPUT_ASCII_LATLON_COUNT);
		list.add(OUTPUT_GPX_TRACK);
		list.add(OUTPUT_GPX_WAYPOINT);
		list.add(OUTPUT_COR);
		list.add(OUTPUT_COR_METRE);
		list.add(OUTPUT_PETREL_POINTS_LATLON);
		list.add(OUTPUT_PETREL_POINTS_UTM);
		list.add(OUTPUT_CUSTOM_WTK);
	}

	private static void initializeImportOptions() {
		INPUT_KML.setArgName("path");
		INPUT_ASCII_UTM.setArgName("path");
		INPUT_ASCII_LATLON.setArgName("path");
		INPUT_GPX.setArgName("path");
		INPUT_COR.setArgName("path");
		INPUT_SGY.setArgName("path");;

		INPUT_CUSTOM_WTK.setArgs(2);
		INPUT_CUSTOM_WTK.setArgName("path_pts path_wtk");

		inputOptions.addOption(INPUT_KML);
		inputOptions.addOption(INPUT_ASCII_UTM);
		inputOptions.addOption(INPUT_ASCII_LATLON);
		inputOptions.addOption(INPUT_GPX);
		inputOptions.addOption(INPUT_COR);
		inputOptions.addOption(INPUT_CUSTOM_WTK);
		inputOptions.addOption(INPUT_SGY);



	}

	public final static void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "osp", options );
	}

	public static void initLogger() {
	    try {
	      if(new File("log4j.properties").exists()) {
	    	  PropertyConfigurator.configure("log4j.properties");
	      }


	      //uncomment
//	      String filePath = "./log.txt";
//	      PatternLayout layout = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1} - %m%n");
//	      RollingFileAppender appender = new RollingFileAppender(layout, filePath);
//	      appender.setName("Open Spatial Log");
//	      appender.setMaxFileSize("1MB");
//	      appender.activateOptions();
//	      Logger.getRootLogger().addAppender(appender);
//
//	      ConsoleAppender appender2 = new ConsoleAppender(layout);
//	      ******

//	      appender.setWriter(System.out);
//	      Logger.getRootLogger().addAppender(appender2);

//	      # Redirect log messages to console
//	      log4j.appender.stdout=org.apache.log4j.ConsoleAppender
//	      log4j.appender.stdout.Target=System.out
//	      log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
//	      log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n
//
//	      # Redirect log messages to a log file, support file rolling.
//	      log4j.appender.file=org.apache.log4j.RollingFileAppender
//	      log4j.appender.file.File=log4j-application.log
//	      log4j.appender.file.MaxFileSize=5MB
//	      log4j.appender.file.MaxBackupIndex=10
//	      log4j.appender.file.layout=org.apache.log4j.PatternLayout
//	      log4j.appender.file.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	public static void main( String[] args ) {
		initLogger();

		if(args.length == 0) {
        	printHelp();

        	System.exit(0);
        }


//		PropertyConfigurator.configure(OpenSpatial.class.getResource("log4j.properties").toExternalForm());
		logger.info("Running Open Spatial");
		logger.info("...parsing "+args.length+" arguments");
	    CommandLineParser parser = new DefaultParser();

	    try {

	    	LinkedHashMap<String, File> inputs = new LinkedHashMap<>();
			LinkedHashMap<String, Option> outputs = new LinkedHashMap<>();
			LinkedHashMap<String, Option> modifiers = new LinkedHashMap<>();




			 CommandLine line = parser.parse( options, args );
			for(int i = 0 ; i < line.getOptions().length ; i++) {
				Option o = line.getOptions()[i];
				if(o.getOpt().equals(RUN_GUI.getOpt())) {

					 Application.launch(OpenSpatialTable.class, args);
//					.launch("");
				}
				//parse inputs
				if(o.getOpt().equals(INPUT_KML.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(INPUT_ASCII_UTM.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(INPUT_ASCII_LATLON.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(INPUT_GPX.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(INPUT_COR.getOpt())) inputs.put(o.getOpt(), new File(o.getValue()));
				else if(o.getOpt().equals(INPUT_CUSTOM_WTK.getOpt())) {
					inputs.put(INPUT_CUSTOM_WTK.getOpt(), new File(o.getValue(0)));
					wtkin = new File(o.getValue(1));
				}
				else if(o.getOpt().equals(OUTPUT_ASCII 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_KML 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_KML_SCALED			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_KML_TRACK			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_ASCII_UTM 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_ASCII_LATLON 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_ASCII_LATLON_COUNT .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_GPX_TRACK 		    .getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_GPX_WAYPOINT 		.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_COR 				.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_COR_METRE			.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_LATLON.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_UTM.getOpt()))  outputs.put(o.getOpt(),o);
				else if(o.getOpt().equals(OUTPUT_CUSTOM_WTK.getOpt())) {
					outputs.put(OUTPUT_CUSTOM_WTK.getOpt(),o);
					wtkout = new File(o.getValue(1));
				}
				else {
					modifiers.put(o.getOpt(), o);
				}


				}

			run(inputs,outputs,modifiers);

	    }
	    catch( ParseException exp ) {
	    	exp.printStackTrace();
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	        printHelp();
	    }
	}

	public static void run(LinkedHashMap<String, File> inputs, LinkedHashMap<String, Option> outputs,	LinkedHashMap<String, Option> modifiers) {
		if(inputs.size() == 0) {
			logger.error("No inputs found!");
			System.exit(0);
		}
		if(inputs.size() > 1) {
			logger.error("More than one intput found. Use only one input.");
			System.exit(0);
		}
		if(outputs.size() == 0) {
			logger.error("No outputs found!");
			System.exit(0);
		}
//		if(outputs.size() > 1) {
//			logger.error("More than one output found. Use only one output.");
//			System.exit(0);
//		}

		logger.info("...input  =\t" + new ArrayList(inputs.values()).get(0));
		logger.info("...outputs");
		for(String key : outputs.keySet()) {
			logger.info("......" + key + "(" + outputs.get(key).getLongOpt() + ") " + (outputs.get(key).hasArg() ? " = " + outputs.get(key).getValuesList() : ""));
		}
		logger.info("...modifiers");
		for(String key : modifiers.keySet()) {
			logger.info("......" + key + "(" + modifiers.get(key).getLongOpt() + ") " + (modifiers.get(key).hasArg() ? " = " + modifiers.get(key).getValuesList() : ""));
		}




		int headerLines = modifiers.containsKey(MODIFIER_HEADER.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_HEADER.getOpt()).getValue())-1 : 1; //default header lines 1
		int colid = modifiers.containsKey(MODIFIER_COLUMN_ID.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_COLUMN_ID.getOpt()).getValue())-1 : 0;
		int colx = modifiers.containsKey(MODIFIER_COLUMN_X.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_COLUMN_X.getOpt()).getValue())-1 : 1;
		int coly = modifiers.containsKey(MODIFIER_COLUMN_Y.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_COLUMN_Y.getOpt()).getValue())-1 : 2;
		int colz = modifiers.containsKey(MODIFIER_COLUMN_Z.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_COLUMN_Z.getOpt()).getValue())-1 : 3;
		int utmZoneNumber = modifiers.containsKey(MODIFIER_UTM.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_UTM.getOpt()).getValuesList().get(0)) : -1;
		char utmZoneID = modifiers.containsKey(MODIFIER_UTM.getOpt()) ? (modifiers.get(MODIFIER_UTM.getOpt()).getValuesList().get(1)).charAt(0) : Character.MAX_VALUE;



		for(String key : inputs.keySet()) {

			File f = new ArrayList<File>(inputs.values()).get(0);
			ArrayList<File> files = new ArrayList<File>();
			if(!f.exists()) {
				logger.error("Input file does not exist!" + f.getAbsolutePath());
				System.exit(0);
			}
			if(f.isDirectory()) {
				files = new ArrayList<File>(Arrays.asList(f.listFiles()));
			} else {
				files.add(f);
			}
			for(File file : files) {
				if(file.isFile()) {
					String name = file.getName().contains(".") ? file.getName().split("\\.")[0] : file.getName();
					ArrayList<Point> points  = new ArrayList<Point>();


					//INPUT FILE HANDLING
					if(key.equals(INPUT_KML.getOpt())) {
						try {
							points = KML.loadKMLPoints(file.getAbsolutePath(),false);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else if(key.equals(INPUT_ASCII_UTM.getOpt())) {
						points = UTMASCII.loadUTMPoints(file.getAbsolutePath(),utmZoneNumber,utmZoneID,colid,colx,coly,colz,headerLines,false);
					} else if(key.equals(INPUT_ASCII_LATLON.getOpt())) {
						points = LatLonASCII.loadLatLonPoints(file.getAbsolutePath(),colid,colx,coly,colz,headerLines,false);
					} else if(key.equals(INPUT_GPX.getOpt())) {
						try {
							points = GPXFormat.read(file.getAbsolutePath(),false);
						} catch (Exception e) {
							e.printStackTrace();
						} break;
					} else if(key.equals(INPUT_COR.getOpt())) {
						points = CORGPR.read(file.getAbsolutePath(),false);
					} else if(key.equals(INPUT_CUSTOM_WTK.getOpt())) {
						points = CRSConverter.loadPoints(file.getAbsolutePath(),wtkin.getAbsolutePath(),colid,colx,coly,colz,headerLines,false);
					} else if(key.equals(INPUT_SGY.getOpt())) {
						points = SEGY.read(file,utmZoneNumber,utmZoneID,false);
					}

					run(name, points, outputs, modifiers);



				}
			}
		}
	}

	public static ArrayList<Point> run(String name, ArrayList<Point> points, LinkedHashMap<String, Option> outputs,LinkedHashMap<String, Option> modifiers) {
		int utmZoneNumber = modifiers.containsKey(MODIFIER_UTM.getOpt()) ? Integer.valueOf(modifiers.get(MODIFIER_UTM.getOpt()).getValuesList().get(0)) : -1;
		char utmZoneID = modifiers.containsKey(MODIFIER_UTM.getOpt()) ? (modifiers.get(MODIFIER_UTM.getOpt()).getValuesList().get(1)).charAt(0) : Character.MAX_VALUE;
		//RUN MODIFIERS
		ArrayList<Point> modifiedPoints = new ArrayList<Point>(new ArrayList<>(points));
		logger.info("Running Modifiers");
		for(String k : modifiers.keySet()) {
			Option o = modifiers.get(k);
			logger.debug("..." + o.getOpt() + "\t(" + o.getDescription() + ")");

			if (o.getOpt().equals(MODIFIER_PTI 		.getOpt())) {
				double dx = Double.valueOf(o.getValue());
				logger.info("Interpolating with cubic spline at " + dx + "m increments");
				modifiedPoints = UTMASCII.interpolate(modifiedPoints,dx, utmZoneNumber,utmZoneID, false);
			}
			else if (o.getOpt().equals(MODIFIER_PTI_LINEAR 	.getOpt())) {

				double dx = Double.valueOf(o.getValue());
				logger.info("Interpolating Linearly at " + dx + "m increments");
				modifiedPoints = UTMASCII.interpolate(modifiedPoints,dx, utmZoneNumber,utmZoneID, true);
			}
			else if (o.getOpt().equals(MODIFIER_LINENUMBER 	.getOpt())) {
				logger.info("C");
			}
			else if (o.getOpt().equals(MODIFIER_CUT_MIN 	.getOpt())) {
				logger.info("Cutting Minimum IDs");
				ArrayList<Point> ptsCut = new ArrayList<Point>();
				for(Point p : modifiedPoints) {
					int minNumber = Integer.valueOf(o.getValue());
					if(Integer.valueOf(p.name) >= minNumber) {
						ptsCut.add(p);
					}
				}
				modifiedPoints = ptsCut;
			}
			else if (o.getOpt().equals(MODIFIER_CUT_MAX 	.getOpt())) {
				logger.info("Cutting Maximum IDs");
				ArrayList<Point> ptsCut = new ArrayList<Point>();
				for(Point p : modifiedPoints) {
					int maxNumber = Integer.valueOf(o.getValue());
					if(Integer.valueOf(p.name) <= maxNumber) {
						ptsCut.add(p);
					}
				}
				modifiedPoints = ptsCut;
			}
			else if (o.getOpt().equals(MODIFIER_COLUMN_ID 	.getOpt())) {}
			else if (o.getOpt().equals(MODIFIER_COLUMN_X 	.getOpt())) {}
			else if (o.getOpt().equals(MODIFIER_COLUMN_Y 	.getOpt())) {}
			else if (o.getOpt().equals(MODIFIER_COLUMN_Z 	.getOpt())) {}
			else if (o.getOpt().equals(MODIFIER_FORCE_UPDATE_NAMING.getOpt())) {
				logger.info("Updating Naming");

				//assign line numbers
				String lineNumber = modifiers.containsKey(MODIFIER_LINENUMBER.getOpt()) ? modifiers.get(MODIFIER_LINENUMBER.getOpt()).getValue() : "1";
				int number = 1;
				int NPoints = modifiedPoints.size();
				NumberFormat nf = NPoints < 10 ? nf1 : (NPoints < 100 ? nf2 : (NPoints < 1000 ? nf3 :  (NPoints < 10000 ? nf4 :  (NPoints < 100000 ? nf5 :  (NPoints < 1000000 ? nf6 : nf7)))));
				for(Point pt: modifiedPoints) {
					pt.name = lineNumber + "" + nf.format(number);
					number++;
				}
			}
			else if (o.getOpt().equals(MODIFIER_PRINT 		.getOpt())) {
				System.out.println("ID\tEasting\tNorthing\tZ\tZoneNumber\tZoneID\tLat\tLon");
				for(Point p: modifiedPoints) {
					LatLng pl = new LatLng(p.lat, p.lon);
					UTMRef r = pl.toUTMRef();
					System.out.println(p.name + "\t" + r.getEasting() + "\t" + r.getNorthing()+ "\t" + p.z +"\t" + r.getLngZone() +"\t" + r.getLatZone() +"\t" + p.lat + "\t" + p.lon);
				}
			}
			else if (o.getOpt().equals(MODIFIER_SKIP_NTH 	.getOpt())) {
				logger.info("Skipping Nth");
				ArrayList<Point> ptsMod = new ArrayList<Point>();
				int skip = Integer.valueOf(o.getValue());
				for (int i = 0 ; i < modifiedPoints.size() ; i+=skip) {
					ptsMod.add(modifiedPoints.get(i));
				}
				modifiedPoints = ptsMod;
			}
			else if (o.getOpt().equals(MODIFIER_REVERSE 	.getOpt())) {
				logger.info("Reversing");
				ArrayList<Point> ptsReversed = new ArrayList<Point>();
				for (int i = modifiedPoints.size()-1 ; i >= 0  ; i--) {
					ptsReversed.add(modifiedPoints.get(i));
				}
				modifiedPoints = ptsReversed;
			}
			else if(o.getOpt().equals(MODIFIER_NEGATE_Z.getOpt())) {
				logger.info("Negating Z");
				for(Point p: modifiedPoints) {
					p.z = -p.z;
				}
			}
		}
		//clipping
		if (modifiers.containsKey(MODIFIER_LAT_MIN.getOpt()) ||
				modifiers.containsKey(MODIFIER_LON_MIN.getOpt()) ||
				modifiers.containsKey(MODIFIER_LAT_MAX.getOpt()) ||
				modifiers.containsKey(MODIFIER_LON_MAX.getOpt())) {
			double valLatMin = modifiers.containsKey(MODIFIER_LAT_MIN.getOpt()) ? Double.valueOf(modifiers.get(MODIFIER_LAT_MIN.getOpt()).getValue()) : Double.NEGATIVE_INFINITY;
			double valLonMin = modifiers.containsKey(MODIFIER_LON_MIN.getOpt()) ? Double.valueOf(modifiers.get(MODIFIER_LON_MIN.getOpt()).getValue()) : Double.NEGATIVE_INFINITY;
			double valLatMax = modifiers.containsKey(MODIFIER_LAT_MAX.getOpt()) ? Double.valueOf(modifiers.get(MODIFIER_LAT_MAX.getOpt()).getValue()) : Double.POSITIVE_INFINITY;
			double valLonMax = modifiers.containsKey(MODIFIER_LON_MAX.getOpt()) ? Double.valueOf(modifiers.get(MODIFIER_LON_MAX.getOpt()).getValue()) : Double.POSITIVE_INFINITY;
			modifiedPoints = LatLonASCII.clipLatLon(modifiedPoints,valLatMin, valLatMax, valLonMin, valLonMax);
		}

		if(modifiers.containsKey(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR.getOpt())) {
			File srtmfile = new File(modifiers.get(MODIFIER_MAP_ELEVATIONS_DEM_LIDAR.getOpt()).getValue());
			if(srtmfile.exists()) {
				try {
					logger.info("Remapping Elevations");
					ARCGISASCII elevations = ARCGISASCII.read(srtmfile);
					if(elevations == null) {
						System.err.println("Elevations is null");
					}
					for(Point p : modifiedPoints) {
						double easting = p.getUTM().getEasting();
						double northing = p.getUTM().getNorthing();
						double z = elevations.getClosestElevation(easting, northing);
						p.z = z;
					}
					logger.info("...Done!");
				} catch(Exception e) {
					logger.error(e);
					e.printStackTrace();
				}
			} else {
				logger.error("Cannot find DEM Lidar File. Ignoring remapping");
			}
		}
		if(modifiers.containsKey(MODIFIER_MAP_ELEVATIONS_SRTM.getOpt())) {
			logger.info("Remapping Elevations using SRTM-3");
//			ArrayList<Point> ptsMod = new ArrayList<Point>();

			for (int i = 0 ; i < modifiedPoints.size() ; i++) {
				Point p = modifiedPoints.get(i);
				p.z = SRTM.getSRTMElevation(p.lat, p.lon);

//				ptsMod.add(p);
			}

//			modifiedPoints = ptsMod;
		}



		ArrayList<Point> ptsOut = new ArrayList<>(modifiedPoints);


		if(modifiers.containsKey(MODIFIER_PRINT.getOpt())) {

			try {
				UTMASCII.printPointsUTMLatLon(ptsOut, new File("temp.txt"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

//		//EXPORT
		for(String outkey : outputs.keySet()) {
			System.err.println("SETTING UP MODIFIERS");
			Option o = outputs.get(outkey);

			boolean isDir = new File(o.getValue()).exists() && new File(o.getValue()).isDirectory();

			ArrayList<ArrayList<Point>> outputPoints = new ArrayList<ArrayList<Point>>();
			ArrayList<String> outputFilenames = new ArrayList<String>();
			if(modifiers.containsKey(MODIFIER_SKIP_NTH.getOpt())) {
				logger.info("Skipping every nth");
				int N = Integer.valueOf(modifiers.get(MODIFIER_SPLIT_OUTPUT.getOpt()).getValue());
				int dn = modifiedPoints.size()/N;
				ArrayList<Point> splitPoints = new ArrayList<Point>();
				int ith = 0;
				int nth = 1;
				String nameID = (N < 10 ? nf1 : (N < 100 ? nf2 : (N < 1000 ? nf3 :  (N < 10000 ? nf4 :  (N < 100000 ? nf5 :  (N < 1000000 ? nf6 : nf7)))))).format(nth);

				while(modifiedPoints.size() > 0) {
					ith++;
					splitPoints.add(modifiedPoints.remove(0));
					if(ith == dn) {
						String outputFileName = isDir ? (o.getValue() + "/" + name + "_" + nameID + "_out_" + o.getOpt() + "." + outputExtensions.get(o.getOpt())) : (o.getValue());
						outputFilenames.add(outputFileName);
						ith = 0;
						outputPoints.add(new ArrayList<>(splitPoints));
						nth++;
						splitPoints = new ArrayList<Point>();

					}
				}
			} else {

				String outputFileName = isDir ? (o.getValue() + "/" + name + "_out_" + o.getOpt() + "." + outputExtensions.get(o.getOpt())) : (o.getValue());
				outputFilenames.add(outputFileName);

				outputPoints.add(modifiedPoints);


			}


			for(Point p : ptsOut) {

				System.out.println(p.lat + "\t" + p.lon + "\t" + p.z);
			}




			for(int i = 0 ; i < outputPoints.size() ; i++) {

				ArrayList<Point> pts = outputPoints.get(i);
				File fout = new File(outputFilenames.get(i));

				try {
					 if(o.getOpt().equals(OUTPUT_ASCII_LATLON.getOpt())) {
						LatLonASCII.printPointsLatLon(pts, fout);
					}
					else if(o.getOpt().equals(OUTPUT_ASCII_LATLON_COUNT.getOpt())) {
						double dlat = Double.valueOf(modifiers.get(MODIFIER_DLAT.getOpt()).getValue());
						double dlon = Double.valueOf(modifiers.get(MODIFIER_DLON.getOpt()).getValue());
						LatLonASCII.createGrid(pts, fout, dlat, dlon);
					}
					else if(o.getOpt().equals(OUTPUT_ASCII_UTM.getOpt())) {
						UTMASCII.printPointsUTM(pts, fout,utmZoneNumber,utmZoneID);
					}
					else if(o.getOpt().equals(OUTPUT_COR.getOpt())) {
						CORGPR.write(pts, fout.getAbsolutePath());
					}
					else if(o.getOpt().equals(OUTPUT_COR_METRE.getOpt())) {
						CORGPR.writeMetre(pts, fout.getAbsolutePath());
					}
					else if(o.getOpt().equals(OUTPUT_GPX_TRACK.getOpt())) {
						GPXFormat.writeTrack(pts, fout.getAbsolutePath());
					}
					else if(o.getOpt().equals(OUTPUT_GPX_WAYPOINT.getOpt())) {
						GPXFormat.writeWaypoints(pts, fout.getAbsolutePath());
					}
					else if(o.getOpt().equals(OUTPUT_KML.getOpt())) {
//						KML.exportZScaledWaypoints(pts, fout, true);
						KML.exportWaypoints(pts, fout, true);
					}
					else if(o.getOpt().equals(OUTPUT_KML_SCALED.getOpt())) {
						KML.exportZScaledWaypoints(pts, fout, true);
//						KML.exportWaypoints(pts, fout, true);
					}
					else if(o.getOpt().equals(OUTPUT_KML_TRACK.getOpt())) {
						KML.exportWaypoints(pts, fout, false);
					}
					else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_LATLON.getOpt())) {
						PetrelASCII.exportFiles(fout, pts, false);
					}
					else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_UTM.getOpt())) {
						PetrelASCII.exportFiles(fout, pts, true);
					}
					else if(o.getOpt().equals(OUTPUT_CUSTOM_WTK.getOpt())) {
						CRSConverter.printPoints(pts, fout, wtkout);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}

		}
		return ptsOut;
	}

	public static String getOutputExtension(Option o) {
		try {
			if(o.getOpt().equals(OUTPUT_ASCII.getOpt())) {
				return "_out.txt";
			} else if(o.getOpt().equals(OUTPUT_ASCII_LATLON.getOpt())) {
				return "_LatLng.txt";
			}
			else if(o.getOpt().equals(OUTPUT_ASCII_LATLON_COUNT.getOpt())) {
				return "_LatLngCount.txt";
			}
			else if(o.getOpt().equals(OUTPUT_ASCII_UTM.getOpt())) {
				return "_UTM.txt";
			}
			else if(o.getOpt().equals(OUTPUT_COR.getOpt())) {
				return "_out.cor";
			}
			else if(o.getOpt().equals(OUTPUT_COR_METRE.getOpt())) {
				return "_out.txt";
			}
			else if(o.getOpt().equals(OUTPUT_GPX_TRACK.getOpt())) {
				return "_track.gpx";
			}
			else if(o.getOpt().equals(OUTPUT_GPX_WAYPOINT.getOpt())) {
				return "_waypoints.gpx";
			}
			else if(o.getOpt().equals(OUTPUT_KML.getOpt())) {
				return "_pts.kml";
			}
			else if(o.getOpt().equals(OUTPUT_KML_SCALED.getOpt())) {
				return "_pts_scaled.kml";
			}
			else if(o.getOpt().equals(OUTPUT_KML_TRACK.getOpt())) {
				return "_track.kml";
			}
			else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_LATLON.getOpt())) {
				return "_ptslatlng.ASCII";
			}
			else if(o.getOpt().equals(OUTPUT_PETREL_POINTS_UTM.getOpt())) {
				return "_ptsutm.ASCII";
			}
			else if(o.getOpt().equals(OUTPUT_CUSTOM_WTK.getOpt())) {
				return null;
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
